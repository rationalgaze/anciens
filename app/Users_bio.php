<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_bio extends Model
{

	protected $table = 'usersbio_usr';
	protected $primaryKey = 'user_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
