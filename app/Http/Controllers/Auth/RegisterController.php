<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Users_bio;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\Welcome;
use App\Mail\NewUser;

class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	protected $redirectTo = '/profile';

	public function __construct()
	{
		$this->middleware('guest');
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users|unique:usersbio_usr,usr_email',
			'password' => 'required|string|min:6|confirmed',
		]);
	}

	protected function create(array $data)
	{

		$usr_bio = new Users_bio;
		$usr_bio->usr_email = $data['email'];
		$usr_bio->save();

		$user = User::create([
			'id' => $usr_bio->user_id,
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => bcrypt($data['password']),
		]);

		auth()->login($user);
		\Mail::to('rationalgathe@mail.ru')->send(new NewUser($user));

		return $user;
	}
}
