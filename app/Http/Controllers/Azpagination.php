<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Users_bio;
use App\User;
use App\Admin;

use Illuminate\Support\Facades\Input;

class Azpagination extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function a()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'a%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function b()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'B%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function c()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'c%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function d()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'd%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function e()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'e%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function f()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'f%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function g()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'g%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function h()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'h%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function i()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'i%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function j()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'j%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function k()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'k%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function l()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'l%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function m()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'm%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function n()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'n%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function o()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'o%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function p()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'p%')->orderBy('usr_nom', 'asc')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function q()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'q%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function r()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'r%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function s()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 's%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function t()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 't%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function u()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'u%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function v()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'v%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function w()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'w%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function x()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'x%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function y()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'y%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function z()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'z%')->orderBy('usr_nom', 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function all()
	{
		$users_bio = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function new()
	{
		$users_bio = Users_bio::orderBy('created_at','desc')->paginate(10);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function par_annee()
	{
		$users_bio = Users_bio::orderBy(DB::raw('ISNULL(`usr_annee_sortie`), `usr_annee_sortie`'), 'desc')->paginate(25);
		$users_all = Users_bio::orderBy('usr_nom', 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}
}
