<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Users_bio;
use App\User;
use App\Admin;
use Image;
use Excel;
use PHPExcel;
use PHPExcel_RichText;
use PHPExcel_Style_Color;
use File;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$users_bio = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->paginate(25);
		$users_all = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();
		$admins = Admin::all();

		return view('admin', compact(['users_bio','admins','users_all']));
	}

	public function par_secteur()
	{
		$admins = Admin::all();

		$bio_par_secteur = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('admin.par_secteur', compact(['admins', 'bio_par_secteur' ]));
	}

	public function par_region()
	{
		$admins = Admin::all();

		$bio_par_region = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('admin.par_region', compact(['admins', 'bio_par_region' ]));
	}

	public function par_parcours()
	{
		$admins = Admin::all();

		$bio_par_parcours = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('admin.par_parcours', compact(['admins', 'bio_par_parcours' ]));
	}

	public function show(Users_bio $usr)
	{
		return view('admin.show', compact('usr'));
	}

	public function edit(Users_bio $usr)
	{
		return view('admin.edit', compact('usr'));
	}

	public function add_usr(Request $request)
	{
		$this->validate(request(), [
			'tel_mobile' => 'digits_between:6,14|nullable',
			'tel_fixe' => 'digits_between:6,14|nullable',
			'date_naiss' => 'date_format:Y-m-d|nullable',
			'email' => 'required|string|email|max:255|unique:users|unique:usersbio_usr,usr_email',
			'telephone_fixe_pro' => 'digits_between:6,14|nullable',
			'password' => 'required|string|min:6',
			'photo' => 'image|mimes:jpg,jpeg,png,gif|max:5120|nullable',
			'cv' => 'max:10000|mimes:pdf,odt,doc,docx,rtf|nullable',
			'ok' => 'required',
		]);

		$user_bio = new Users_bio;
		$user = new User;

		$photo = $request->file('photo');
		$file = $request->file('cv');
		$name_img = '';
		$name_cv = '';

		if($photo){
			$ext = $photo->guessClientExtension();
			$name_img = 'Avatar_'.$user_bio->user_id.'_'.$user_bio->usr_nom.'_'.$user_bio->usr_prenom.'_'.uniqid().".{$ext}";
			$photo = Image::make($photo->getRealPath());
			$path = "storage/avatars/{$name_img}";
			$photo->fit(240, 240, function ($constraint) {
				$constraint->aspectRatio();
			})->save(public_path($path));

			$old_name = $user_bio->usr_img;
			File::delete("storage/avatars/{$old_name}");
			$user_bio->usr_img = $name_img;
		}

		if($file){
			$ext = $file->guessClientExtension();
			$name_cv = 'CV_'.$user_bio->user_id.'_'.$user_bio->usr_nom.'_'.$user_bio->usr_prenom.'_'.uniqid().".{$ext}";
			$file->storeAs('public/cv_storage', $name_cv);

			File::delete("storage/cv_storage/{$user_bio->usr_cv}");
			$user_bio->usr_cv = $name_cv;
		}

		$user_bio->usr_email = $request->email;

		$user_bio->usr_sex = $request->sex;
		$user_bio->usr_nom = $request->nom;
		$user_bio->usr_nom_naiss = $request->nom_naiss;
		$user_bio->usr_prenom = $request->prenom;
		$user_bio->usr_date_naiss = $request->date_naiss;
		$user_bio->usr_lieu_naiss = $request->lieu_naiss;
		$user_bio->usr_adresse_perso = $request->adresse_perso;
		$user_bio->usr_cp = $request->cp;
		$user_bio->usr_ville = $request->ville;
		$user_bio->usr_pays = $request->pays;
		$user_bio->usr_tel_mob = $request->tel_mobile;
		$user_bio->usr_tel_fixe = $request->tel_fixe;
		$user_bio->usr_annee_entree = $request->annee_entree;
		$user_bio->usr_annee_sortie = $request->annee_sortie;
		$user_bio->usr_diplome = $request->diplome;
		$user_bio->usr_fb = $request->fb;

		$user_bio->usr_activite = $request->activite;
		$user_bio->usr_poste = $request->profession;
		$user_bio->usr_contrat = $request->type_de_contrat;
		$user_bio->usr_annee_emb = $request->anne_embauche;
		$user_bio->usr_nom_entreprise = $request->entreprise;
		$user_bio->usr_adresse_entreprise = $request->adressepro;
		$user_bio->usr_cp_entreprise = $request->code_postalpro;
		$user_bio->usr_ville_entreprise = $request->ville_pro;
		$user_bio->usr_pays_travail = $request->payspro;
		$user_bio->usr_tel_pro = $request->telephone_fixe_pro;
		$user_bio->usr_email_pro = $request->courriel_professionnel;
		$user_bio->usr_secteur = $request->secteur;
		$user_bio->usr_site_web = $request->adresse_site_web;
		$user_bio->usr_linkedin = $request->linkedin;
		$user_bio->usr_viadeo = $request->viadeo;
		$user_bio->usr_donnees = $request->ok;

		$user_bio->save();

		$user->id = $user_bio->user_id;

		$user->email = $request->email;
		$user->name = $user_bio->usr_prenom;
		$user->password = bcrypt($request->password);

		$user->save();

		return redirect()->back();
	}

	public function update_personal(Users_bio $usr, Request $request)
	{
		$this->validate(request(), [
			'tel_mobile' => 'digits_between:6,14|nullable',
			'tel_fixe' => 'digits_between:6,14|nullable',
			'date_naiss' => 'date_format:Y-m-d|nullable',
			'email' => 'required|string|email|max:255',
			'photo' => 'image|mimes:jpg,jpeg,png,gif|max:5120|nullable',
		]);

		$usr->usr_email = $request->email;

		$photo = $request->file('photo');
		$name = '';

		if($photo){
			$ext = $photo->guessClientExtension();
			$name = 'Avatar_'.$usr->user_id.'_'.$usr->usr_nom.'_'.$usr->usr_prenom.'_'.uniqid().".{$ext}";
			$photo = Image::make($photo->getRealPath());
			$path = "storage/avatars/{$name}";
			$photo->fit(240, 240, function ($constraint) {
				$constraint->aspectRatio();
			})->save(public_path($path));

			$old_name = $usr->usr_img;
			File::delete("storage/avatars/{$old_name}");
			$usr->usr_img = $name;
		}

		$usr->usr_sex = $request->sex;
		$usr->usr_nom = $request->nom;
		$usr->usr_nom_naiss = $request->nom_naiss;
		$usr->usr_prenom = $request->prenom;
		$usr->usr_date_naiss = $request->date_naiss;
		$usr->usr_lieu_naiss = $request->lieu_naiss;
		$usr->usr_adresse_perso = $request->adresse_perso;
		$usr->usr_cp = $request->cp;
		$usr->usr_ville = $request->ville;
		$usr->usr_pays = $request->pays;
		$usr->usr_tel_mob = $request->tel_mobile;
		$usr->usr_tel_fixe = $request->tel_fixe;
		$usr->usr_annee_entree = $request->annee_entree;
		$usr->usr_annee_sortie = $request->annee_sortie;
		$usr->usr_diplome = $request->diplome;
		$usr->usr_fb = $request->fb;

		if($usr->user) {
			$usr->user->email = $request->email;
			$usr->user->save();
		}

		$usr->save();

		// return redirect()->route('admin.dashboard');
		return redirect()->back();
	}

	public function update_pro(Users_bio $usr, Request $request)
	{

		$this->validate(request(), [
			'telephone_fixe_pro' => 'digits_between:6,14|nullable',
			'cv' => 'max:10000|mimes:pdf,odt,doc,docx,rtf|nullable',
		]);

		$user_bio = $usr;

		$file = $request->file('cv');
		$name = '';

		if($file){
			$ext = $file->guessClientExtension();
			$name = 'CV_'.$usr->user_id.'_'.$usr->usr_nom.'_'.$usr->usr_prenom.'_'.uniqid().".{$ext}";
			$file->storeAs('public/cv_storage', $name);

			File::delete("storage/cv_storage/{$user_bio->usr_cv}");
		}

		$user_bio->	usr_activite = $request->activite;
		$user_bio->usr_poste = $request->profession;
		$user_bio->usr_contrat = $request->type_de_contrat;
		$user_bio->usr_annee_emb = $request->anne_embauche;
		$user_bio->usr_nom_entreprise = $request->entreprise;
		$user_bio->usr_adresse_entreprise = $request->adressepro;
		$user_bio->usr_cp_entreprise = $request->code_postalpro;
		$user_bio->usr_ville_entreprise = $request->ville_pro;
		$user_bio->usr_pays_travail = $request->payspro;
		$user_bio->usr_tel_pro = $request->telephone_fixe_pro;
		$user_bio->usr_email_pro = $request->courriel_professionnel;
		$user_bio->usr_secteur = $request->secteur;
		$user_bio->usr_site_web = $request->adresse_site_web;
		$user_bio->usr_donnees = $request->ok;
		$user_bio->usr_linkedin = $request->linkedin;
		$user_bio->usr_viadeo = $request->viadeo;
		$usr->usr_cv = $name;

		$user_bio->save();

		return redirect()->back();
	}

	public function change_pass(Users_bio $usr, Request $request)
	{
		$this->validate(request(), [
			'password' => 'required|string|min:6',
		]);

		if($usr->user) {
			$usr->user->password = bcrypt($request->password);
			$usr->user->save();
		}

		return redirect()->back()->with('message', 'Vous avez bien changé le mot de passe de '.$usr->usr_prenom);
	}

	public function delete(Users_bio $usr) 
	{
		if($usr){
			$email = $usr->usr_email;
			User::where('email',$email)->delete();
			Users_bio::where('usr_email',$email)->delete();
			(!$usr->usr_cv) ? : File::delete("storage/cv_storage/{$usr->usr_cv}");
			(!$usr->usr_img) ? : File::delete("storage/avatars/{$usr->usr_img}");
		}

		return redirect()->route('admin.dashboard');
	}

	public function edit_admin(Admin $admin)
	{
		return view('admin.edit_admin', compact('admin'));
	}

	public function add_admin(Request $request)
	{
		$this->validate(request(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:admins',
			'password' => 'required|string|min:6|confirmed',
		]);

		$admin = new Admin;
		$admin->name = $request->name;
		$admin->email = $request->email;
		$admin->password = bcrypt($request->password);

		$admin->save();

		return redirect()->route('admin.dashboard');
	}

	public function update_admin(Admin $admin, Request $request)
	{
		$this->validate(request(), [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255',
			'mdp_new' => 'required|string|min:6|confirmed',
		]);

		$admin->name = $request->name;
		$admin->email = $request->email;

		if(\Hash::check($request->mdp_act, $admin->password)){

			$admin->password = bcrypt($request->mdp_new);
			$admin->save();

			return redirect()->route('admin.dashboard')->with('message', 'Vous avez bien changé votre mot de passe'); 
		}

		$admin->save();

		return redirect()->route('admin.dashboard')->with('message', 'Vous avez bien changé vos données');
	}

	public function delete_admin(Admin $admin) 
	{
		if($admin && $admin->id != 1){
			$admin->delete();
		}

		return redirect()->back();
	}

	/*
	* Data export to MS Word
	*/
	public function to_word() 
	{
		ob_end_clean(); ob_start();
		$phpWord = new \PhpOffice\PhpWord\PhpWord();

		$bios = Users_bio::where([
			['usr_donnees', '!=', 'non'],
			['usr_annee_sortie', '>', 2012],
		])->orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		// Margins on the page.
		$sectionStyle = array(
			'orientation' => 'portrait',
			'marginTop' => 850.393700787,	//1.5 cm
			'marginLeft' => 850.393700787, 	//1.5 cm
			'marginRight' => 850.393700787,	//1.5 cm
			'marginBottom' => 722.834645669,//1.275 cm
		);

		$section = $phpWord->addSection($sectionStyle);

		// Styles
		$bold = array('bold'=>true, 'size'=>14, 'name'=>'Liberation Serif');
		$regular = array('bold'=>false, 'size'=>14, 'name'=>'Liberation Serif');
		$sm_regular = array('bold'=>false, 'size'=>12, 'name'=>'Liberation Serif');
		$it_regular = array('bold'=>false, 'size'=>14, 'name'=>'Liberation Serif', 'italic' => true);
		$reg = array('bold'=>true, 'size' => 16, 'name'=>'Liberation Serif');
		$bg_region = array('shading' => array('fill' => 'dddddd'));
		$bg_sousregion = array('borderBottomSize' => 1);

		$tableStyle = array('cellMargin'=> 50, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER);
		$tableCellStyle = array('valign' => 'top');
		$rightCellStyle = array('align' => 'right');

		$villes = array('paris', 'moulins', 'dijon', 'bourg-en-bresse', 'annecy', 'chambéry', 'lyon', 'privas', 'aurillac', 'valence','grenoble','saint-étienne', 'le puy-en-velay','clermont-ferrand', 'brest', 'rennes','marseille');

		/*
		* Tri Par Parcours 
		*/
		$section->addTextBreak(25);
		$section->addText(
			'Les Anciens par Parcours', 
			array('bold'=>true, 'size'=>48, 'name'=>'Liberation Serif'), 
			array('align' => 'center')
		);
		$section->addPageBreak();

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé de l\'Université de Brest" (1975-1992)'){
				$section->addTextBreak(1);
				$section->addText('Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé de l\'Université de Brest" (1975-1992)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_1 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé de l\'Université de Brest" (1975-1992)'){
				$tab_promo_1->addRow();
				$tab_promo_1->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_1->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_1->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur Microbiologiste, diplômé de l\'Université de Brest (1985-1993)'){
				$section->addTextBreak(1);
				$section->addText('Ingénieur Microbiologiste, diplômé de l\'Université de Brest (1985-1993)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_2 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur Microbiologiste, diplômé de l\'Université de Brest (1985-1993)'){
				$tab_promo_2->addRow();
				$tab_promo_2->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_2->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_2->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)'){
				$section->addTextBreak(1);
				$section->addText('Ingénieur diplômé de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_3 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)'){
				$tab_promo_3->addRow();
				$tab_promo_3->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_3->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_3->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)'){
				$section->addTextBreak(1);
				$section->addText('Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_4 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)'){
				$tab_promo_4->addRow();
				$tab_promo_4->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_4->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_4->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne(2002-2012)'){
				$section->addTextBreak(1);
				$section->addText('Ingénieur diplômé de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne(2002-2012)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_5 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne(2002-2012)'){
				$tab_promo_5->addRow();
				$tab_promo_5->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_5->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_5->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)'){
				$section->addTextBreak(1);
				$section->addText('Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_6 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Ingénieur diplômé de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)'){
				$tab_promo_6->addRow();
				$tab_promo_6->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_6->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_6->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Master professionnel diplômé de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)'){
				$section->addTextBreak(1);
				$section->addText('Master professionnel diplômé de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_promo_7 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_diplome == 'Master professionnel diplômé de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)'){
				$tab_promo_7->addRow();
				$tab_promo_7->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_promo_7->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_promo_7->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addPageBreak();

		/*
		* Tri Par Secteur d'activite 
		*/
		$section->addTextBreak(25);
		$section->addText(
			'Les Anciens par Secteur d\'activite', 
			array('bold'=>true, 'size'=>48, 'name'=>'Liberation Serif'), 
			array('align' => 'center')
		);
		$section->addPageBreak();

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie de la viande'){
				$section->addTextBreak(1);
				$section->addText('Industrie de la viande ', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_1 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie de la viande'){
				$tab_sect_1->addRow();
				$tab_sect_1->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_1->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_1->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie laitière'){
				$section->addTextBreak(1);
				$section->addText('Industrie laitière', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_2 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie laitière'){
				$tab_sect_2->addRow();
				$tab_sect_2->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_2->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_2->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie sucrière'){
				$section->addTextBreak(1);
				$section->addText('Industrie sucrière', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_3 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie sucrière'){
				$tab_sect_3->addRow();
				$tab_sect_3->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_3->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_3->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des conserves, produits surgelés, fruits et légumes'){
				$section->addTextBreak(1);
				$section->addText('Industrie des conserves, produits surgelés, fruits et légumes', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_4 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des conserves, produits surgelés, fruits et légumes'){
				$tab_sect_4->addRow();
				$tab_sect_4->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_4->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_4->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des produits à base de céréales'){
				$section->addTextBreak(1);
				$section->addText('Industrie des produits à base de céréales', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_5 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des produits à base de céréales'){
				$tab_sect_5->addRow();
				$tab_sect_5->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_5->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_5->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des produits alimentaires divers (confiseries, condiments...)'){
				$section->addTextBreak(1);
				$section->addText('Industrie des produits alimentaires divers (confiseries, condiments...)', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_6 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des produits alimentaires divers (confiseries, condiments...)'){
				$tab_sect_6->addRow();
				$tab_sect_6->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_6->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_6->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des boissons, alcools et agro-carburants'){
				$section->addTextBreak(1);
				$section->addText('Industrie des boissons, alcools et agro-carburants', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_7 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Industrie des boissons, alcools et agro-carburants'){
				$tab_sect_7->addRow();
				$tab_sect_7->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_7->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_7->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Cosmétique'){
				$section->addTextBreak(1);
				$section->addText('Cosmétique', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_8 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Cosmétique'){
				$tab_sect_8->addRow();
				$tab_sect_8->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_8->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_8->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Restauration et hôtellerie'){
				$section->addTextBreak(1);
				$section->addText('Restauration et hôtellerie', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_9 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Restauration et hôtellerie'){
				$tab_sect_9->addRow();
				$tab_sect_9->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_9->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_9->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Etablissements de santé'){
				$section->addTextBreak(1);
				$section->addText('Etablissements de santé', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_10 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Etablissements de santé'){
				$tab_sect_10->addRow();
				$tab_sect_10->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_10->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_10->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Formation et consulting'){
				$section->addTextBreak(1);
				$section->addText('Formation et consulting', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_11 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Formation et consulting'){
				$tab_sect_11->addRow();
				$tab_sect_11->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_11->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_11->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Enseignement'){
				$section->addTextBreak(1);
				$section->addText('Enseignement', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_12 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Enseignement'){
				$tab_sect_12->addRow();
				$tab_sect_12->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_12->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_12->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Recherche'){
				$section->addTextBreak(1);
				$section->addText('Recherche', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_13 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Recherche'){
				$tab_sect_13->addRow();
				$tab_sect_13->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_13->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_13->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addTextBreak(1);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Autres secteurs'){
				$section->addTextBreak(1);
				$section->addText('Autres secteurs', $reg, $bg_region);
				break;
			}
		}
		$section->addTextBreak(1);
		$tab_sect_14 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_secteur == 'Autres secteurs'){
				$tab_sect_14->addRow();
				$tab_sect_14->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$tab_sect_14->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$tab_sect_14->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addPageBreak();

		/*
		* Tri Par Region de travaille 
		*/
		$section->addTextBreak(25);
		$section->addText(
			'Par Region', 
			array('bold'=>true, 'size'=>48, 'name'=>'Liberation Serif'), 
			array('align' => 'center')
		);

		$section->addPageBreak();

		/****** Auvergne-Rhône-Alpes ******/

		$section->addText('Auvergne-Rhône-Alpes', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '01' || strtolower($ville) == 'bourg-en-bresse')){
				$section->addTextBreak(1);
				$section->addText('Ain (01)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '01' || strtolower($ville) == 'bourg-en-bresse')){
				$table->addRow();
				$table->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '03' || strtolower($ville) == 'moulins')){
				$section->addTextBreak(1);
				$section->addText('Allier (03)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table2 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '03' || strtolower($ville) == 'moulins')){
				$table2->addRow();
				$table2->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table2->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table2->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '07' || strtolower($ville) == 'privas')){
				$section->addTextBreak(1);
				$section->addText('Ardèche (07)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table3 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '07' || strtolower($ville) == 'privas')){
				$table3->addRow();
				$table3->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table3->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table3->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '15' || strtolower($ville) == 'aurillac')){
				$section->addTextBreak(1);
				$section->addText('Cantal (15)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table4 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '15' || strtolower($ville) == 'aurillac')){
				$table4->addRow();
				$table4->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table4->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table4->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '26' || strtolower($ville) == 'valence')){
				$section->addTextBreak(1);
				$section->addText('Drôme (26)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table5 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '26' || strtolower($ville) == 'valence')){
				$table5->addRow();
				$table5->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table5->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table5->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '38' || strtolower($ville) == 'grenoble')){
				$section->addTextBreak(1);
				$section->addText('Isère (38)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table6 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '38' || strtolower($ville) == 'grenoble')){
				$table6->addRow();
				$table6->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table6->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table6->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '42' || strtolower($ville) == 'saint-étienne')){
				$section->addTextBreak(1);
				$section->addText('Loire (42)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table7 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '42' || strtolower($ville) == 'saint-étienne')){
				$table7->addRow();
				$table7->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table7->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table7->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '43' || strtolower($ville) == 'le puy-en-velay')){
				$section->addTextBreak(1);
				$section->addText('Haute-Loire (43)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table8 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '43' || strtolower($ville) == 'le puy-en-velay')){
				$table8->addRow();
				$table8->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table8->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table8->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '63' || strtolower($ville) == 'clermont-ferrand')){
				$section->addTextBreak(1);
				$section->addText('Puy-de-Dôme (63)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table9 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '63' || strtolower($ville) == 'clermont-ferrand')){
				$table9->addRow();
				$table9->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table9->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table9->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '69' || strtolower($ville) == 'lyon')){
				$section->addTextBreak(1);
				$section->addText('Rhône et Métropole de Lyon (69)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table10 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '69' || strtolower($ville) == 'lyon')){
				$table10->addRow();
				$table10->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table10->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table10->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '73' || strtolower($ville) == 'chambéry')){
				$section->addTextBreak(1);
				$section->addText('Savoie (73)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table11 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '73' || strtolower($ville) == 'chambéry')){
				$table11->addRow();
				$table11->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table11->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table11->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '74' || strtolower($ville) == 'annecy')){
				$section->addTextBreak(1);
				$section->addText('Haute-Savoie (74)', $it_regular, $bg_sousregion);				
				break;
			}
		}

		$table12 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '74' || strtolower($ville) == 'annecy')){
				$table12->addRow();
				$table12->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table12->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table12->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Bourgogne-Franche-Comté ******/

		$section->addTextBreak(1);
		$section->addText('Bourgogne-Franche-Comté', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '21' || strtolower($ville) == 'dijon')){

				$section->addTextBreak(1);
				$section->addText('Côte-d\'Or (21)', $it_regular, $bg_sousregion);
				
				break;
			}
		}

		$table_1 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '21' || strtolower($ville) == 'dijon')){
				$table_1->addRow();
				$table_1->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_1->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_1->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '25' || strtolower($ville) == 'besançon')){

				$section->addTextBreak(1);
				$section->addText('Doubs (25)', $it_regular, $bg_sousregion);
				
				break;
			}
		}

		$table_2 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '25' || strtolower($ville) == 'besançon')){
				$table_2->addRow();
				$table_2->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_2->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_2->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '39'){

				$section->addTextBreak(1);
				$section->addText('Jura (39)', $it_regular, $bg_sousregion);
				
				break;
			}
		}

		$table_3 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '39'){
				$table_3->addRow();
				$table_3->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_3->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_3->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '58'){
				$section->addTextBreak(1);
				$section->addText('Nièvre (58)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_4 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '58'){
				$table_4->addRow();
				$table_4->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_4->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_4->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '70'){
				$section->addTextBreak(1);
				$section->addText('Haute-Saône (70)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_5 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '70'){
				$table_5->addRow();
				$table_5->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_5->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_5->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '71'){
				$section->addTextBreak(1);
				$section->addText('Saône-et-Loire (71)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_6 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '71'){
				$table_6->addRow();
				$table_6->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_6->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_6->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '89'){
				$section->addTextBreak(1);
				$section->addText('Yonne (89)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_7 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '89'){
				$table_7->addRow();
				$table_7->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_7->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_7->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '90'){
				$section->addTextBreak(1);
				$section->addText('Territoire de Belfort (90)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_8 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '90'){
				$table_8->addRow();
				$table_8->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_8->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_8->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Bretagne ******/

		$section->addTextBreak(1);
		$section->addText('Bretagne', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '22' || strtolower($ville) == 'rennes')){
				$section->addTextBreak(1);
				$section->addText('Côtes-d\'Armor (22)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_22 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '22' || strtolower($ville) == 'rennes')){
				$table_22->addRow();
				$table_22->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_22->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_22->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '29' || strtolower($ville) == 'brest')){

				$section->addTextBreak(1);
				$section->addText('Finistère (29)', $it_regular, $bg_sousregion);
				
				break;
			}
		}

		$table_29 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '29' || strtolower($ville) == 'brest')){
				$table_29->addRow();
				$table_29->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_29->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_29->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '35'){

				$section->addTextBreak(1);
				$section->addText('Ille-et-Vilaine (35)', $it_regular, $bg_sousregion);
				
				break;
			}
		}

		$table_35 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '35'){
				$table_35->addRow();
				$table_35->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_35->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_35->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '56'){
				$section->addTextBreak(1);
				$section->addText('Morbihan (56)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_56 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '56'){
				$table_56->addRow();
				$table_56->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_56->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_56->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Centre-Val de Loire ******/

		$section->addTextBreak(1);
		$section->addText('Centre-Val de Loire', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '18'){
				$section->addTextBreak(1);
				$section->addText('Cher (18)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_18 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '18'){
				$table_18->addRow();
				$table_18->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_18->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_18->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '28'){
				$section->addTextBreak(1);
				$section->addText('Eure-et-Loir (28)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_28 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '28'){
				$table_28->addRow();
				$table_28->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_28->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_28->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '36'){
				$section->addTextBreak(1);
				$section->addText('Indre (36)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_36 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '36'){
				$table_36->addRow();
				$table_36->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_36->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_36->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '37'){
				$section->addTextBreak(1);
				$section->addText('Indre-et-Loire (37)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_37 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '37'){
				$table_37->addRow();
				$table_37->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_37->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_37->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '41'){
				$section->addTextBreak(1);
				$section->addText('Loir-et-Cher (41)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_41 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '41'){
				$table_41->addRow();
				$table_41->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_41->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_41->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '45'){
				$section->addTextBreak(1);
				$section->addText('Loiret (45)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_45 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '45'){
				$table_45->addRow();
				$table_45->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_45->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_45->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Corse ******/

		$section->addTextBreak(1);
		$section->addText('Corse', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '20'){
				$section->addTextBreak(1);
				$section->addText('Corse (20)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_20 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '20'){
				$table_20->addRow();
				$table_20->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_20->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_20->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Grand Est ******/

		$section->addTextBreak(1);
		$section->addText('Grand Est', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '08'){
				$section->addTextBreak(1);
				$section->addText('Ardennes (08)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_08 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '08'){
				$table_08->addRow();
				$table_08->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_08->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_08->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '10'){
				$section->addTextBreak(1);
				$section->addText('Aube (10)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_10 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '10'){
				$table_10->addRow();
				$table_10->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_10->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_10->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '51'){
				$section->addTextBreak(1);
				$section->addText('Marne (51)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_51 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '51'){
				$table_51->addRow();
				$table_51->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_51->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_51->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '52'){
				$section->addTextBreak(1);
				$section->addText('Haute-Marne (52)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_52 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '52'){
				$table_52->addRow();
				$table_52->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_52->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_52->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '54'){
				$section->addTextBreak(1);
				$section->addText('Meurthe-et-Moselle (54)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_54 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '54'){
				$table_54->addRow();
				$table_54->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_54->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_54->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '55'){
				$section->addTextBreak(1);
				$section->addText('Meuse (55)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_55 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '55'){
				$table_55->addRow();
				$table_55->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_55->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_55->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '57'){
				$section->addTextBreak(1);
				$section->addText('Moselle (57)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_57 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '57'){
				$table_55->addRow();
				$table_55->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_55->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_55->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '67'){
				$section->addTextBreak(1);
				$section->addText('Bas-Rhin (67)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_67 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '67'){
				$table_67->addRow();
				$table_67->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_67->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_67->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '68'){
				$section->addTextBreak(1);
				$section->addText('Haut-Rhin (68)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_68 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '68'){
				$table_68->addRow();
				$table_68->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_68->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_68->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '88'){
				$section->addTextBreak(1);
				$section->addText('Vosges (88)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_88 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '88'){
				$table_88->addRow();
				$table_88->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_88->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_88->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Hauts-de-France ******/

		$section->addTextBreak(1);
		$section->addText('Hauts-de-France', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '02'){
				$section->addTextBreak(1);
				$section->addText('Aisne (02)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_02 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '02'){
				$table_02->addRow();
				$table_02->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_02->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_02->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '59'){
				$section->addTextBreak(1);
				$section->addText('Nord (59)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_59 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '59'){
				$table_59->addRow();
				$table_59->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_59->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_59->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '60'){
				$section->addTextBreak(1);
				$section->addText('Oise (60)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_60 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '60'){
				$table_51->addRow();
				$table_51->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_51->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_51->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '62'){
				$section->addTextBreak(1);
				$section->addText('Pas-de-Calais (62)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_62 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '62'){
				$table_62->addRow();
				$table_62->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_62->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_62->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '80'){
				$section->addTextBreak(1);
				$section->addText('Somme (80)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_80 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '80'){
				$table_80->addRow();
				$table_80->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_80->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_80->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Île-de-France ******/

		$section->addTextBreak(1);
		$section->addText('Île-de-France', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && (substr($bio->usr_cp_entreprise,0,2) == '75' || strtolower($ville) == 'paris')){
				$section->addTextBreak(1);
				$section->addText('Paris (75)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_75 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && (substr($bio->usr_cp_entreprise,0,2) == '75' || strtolower($ville) == 'paris')){
				$table_75->addRow();
				$table_75->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_75->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_75->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '77'){
				$section->addTextBreak(1);
				$section->addText('Seine-et-Marne (77)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_77 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '77'){
				$table_77->addRow();
				$table_77->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_77->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_77->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '78'){
				$section->addTextBreak(1);
				$section->addText('Yvelines (78)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_78 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '78'){
				$table_78->addRow();
				$table_78->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_78->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_78->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '91'){
				$section->addTextBreak(1);
				$section->addText('Essonne (91)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_91 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '91'){
				$table_91->addRow();
				$table_91->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_91->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_91->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '92'){
				$section->addTextBreak(1);
				$section->addText('Hauts-de-Seine (92)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_92 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '92'){
				$table_92->addRow();
				$table_92->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_92->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_92->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '93'){
				$section->addTextBreak(1);
				$section->addText('Seine-Saint-Denis (93)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_93 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '93'){
				$table_93->addRow();
				$table_93->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_93->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_93->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '94'){
				$section->addTextBreak(1);
				$section->addText('Val-de-Marne (94)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_94 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '94'){
				$table_94->addRow();
				$table_94->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_94->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_94->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '95'){
				$section->addTextBreak(1);
				$section->addText('Val-d\'Oise (95)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_95 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '95'){
				$table_95->addRow();
				$table_95->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_95->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_95->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Normandie ******/

		$section->addTextBreak(1);
		$section->addText('Normandie', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '14'){
				$section->addTextBreak(1);
				$section->addText('Calvados (14)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_14 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '14'){
				$table_14->addRow();
				$table_14->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_14->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_14->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '27'){
				$section->addTextBreak(1);
				$section->addText('Eure (27)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_27 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '27'){
				$table_27->addRow();
				$table_27->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_27->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_27->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '50'){
				$section->addTextBreak(1);
				$section->addText('Manche (50)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_50 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '50'){
				$table_50->addRow();
				$table_50->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_50->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_50->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '61'){
				$section->addTextBreak(1);
				$section->addText('Orne (61)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_61 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '61'){
				$table_61->addRow();
				$table_61->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_61->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_61->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '76'){
				$section->addTextBreak(1);
				$section->addText('Seine-Maritime (76)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_76 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '76'){
				$table_76->addRow();
				$table_76->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_76->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_76->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Nouvelle-Aquitaine ******/

		$section->addTextBreak(1);
		$section->addText('Nouvelle-Aquitaine', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '16'){
				$section->addTextBreak(1);
				$section->addText('Charente (16)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_16 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '16'){
				$table_16->addRow();
				$table_16->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_16->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_16->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '17'){
				$section->addTextBreak(1);
				$section->addText('Charente-Maritime (17)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_17 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '17'){
				$table_17->addRow();
				$table_17->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_17->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_17->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '19'){
				$section->addTextBreak(1);
				$section->addText('Corrèze (19)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_19 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '19'){
				$table_19->addRow();
				$table_19->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_19->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_19->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '23'){
				$section->addTextBreak(1);
				$section->addText('Creuse (23)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_23 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '23'){
				$table_23->addRow();
				$table_23->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_23->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_23->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '24'){
				$section->addTextBreak(1);
				$section->addText('Dordogne (24)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_24 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '24'){
				$table_24->addRow();
				$table_24->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_24->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_24->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '33'){
				$section->addTextBreak(1);
				$section->addText('Gironde (33)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_33 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '33'){
				$table_33->addRow();
				$table_33->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_33->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_33->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '40'){
				$section->addTextBreak(1);
				$section->addText('Landes (40)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_40 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '40'){
				$table_40->addRow();
				$table_40->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_40->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_40->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '47'){
				$section->addTextBreak(1);
				$section->addText('Lot-et-Garonne (47)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_47 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '47'){
				$table_47->addRow();
				$table_47->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_47->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_47->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '64'){
				$section->addTextBreak(1);
				$section->addText('Pyrénées-Atlantiques (64)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_64 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '64'){
				$table_64->addRow();
				$table_64->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_64->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_64->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '79'){
				$section->addTextBreak(1);
				$section->addText('Deux-Sèvres (79)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_79 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '79'){
				$table_79->addRow();
				$table_79->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_79->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_79->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '86'){
				$section->addTextBreak(1);
				$section->addText('Vienne (86)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_86 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '86'){
				$table_86->addRow();
				$table_86->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_86->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_86->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '87'){
				$section->addTextBreak(1);
				$section->addText('Haute-Vienne (87)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_87 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '87'){
				$table_87->addRow();
				$table_87->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_87->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_87->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Occitanie ******/

		$section->addTextBreak(1);
		$section->addText('Occitanie', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '09'){
				$section->addTextBreak(1);
				$section->addText('Ariège (09)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_09 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '09'){
				$table_09->addRow();
				$table_09->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_09->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_09->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '11'){
				$section->addTextBreak(1);
				$section->addText('Aude (11)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_11 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '11'){
				$table_11->addRow();
				$table_11->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_11->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_11->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '12'){
				$section->addTextBreak(1);
				$section->addText('Aveyron (12)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_12 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '12'){
				$table_12->addRow();
				$table_12->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_12->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_12->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '30'){
				$section->addTextBreak(1);
				$section->addText('Gard (30)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_30 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '30'){
				$table_30->addRow();
				$table_30->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_30->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_30->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '31'){
				$section->addTextBreak(1);
				$section->addText('Haute-Garonne (31)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_31 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '31'){
				$table_31->addRow();
				$table_31->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_31->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_31->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '32'){
				$section->addTextBreak(1);
				$section->addText('Gers (32)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_32 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '32'){
				$table_32->addRow();
				$table_32->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_32->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_32->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '34'){
				$section->addTextBreak(1);
				$section->addText('Hérault (34)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_34 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '34'){
				$table_34->addRow();
				$table_34->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_34->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_34->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '46'){
				$section->addTextBreak(1);
				$section->addText('Lot (46)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_46 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '46'){
				$table_46->addRow();
				$table_46->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_46->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_46->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '48'){
				$section->addTextBreak(1);
				$section->addText('Lozère (48)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_48 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '48'){
				$table_48->addRow();
				$table_48->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_48->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_48->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '65'){
				$section->addTextBreak(1);
				$section->addText('Hautes-Pyrénées (65)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_65 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '65'){
				$table_65->addRow();
				$table_65->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_65->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_65->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '66'){
				$section->addTextBreak(1);
				$section->addText('Pyrénées-Orientales (66)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_66 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '66'){
				$table_66->addRow();
				$table_66->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_66->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_66->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '81'){
				$section->addTextBreak(1);
				$section->addText('Tarn (81)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_81 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '81'){
				$table_81->addRow();
				$table_81->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_81->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_81->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '82'){
				$section->addTextBreak(1);
				$section->addText('Tarn-et-Garonne (82)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_82 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '82'){
				$table_82->addRow();
				$table_82->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_82->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_82->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Pays de la Loire ******/

		$section->addTextBreak(1);
		$section->addText('Pays de la Loire', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '44'){
				$section->addTextBreak(1);
				$section->addText('Loire-Atlantique (44)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_44 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '44'){
				$table_44->addRow();
				$table_44->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_44->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_44->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '49'){
				$section->addTextBreak(1);
				$section->addText('Maine-et-Loire (49)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_49 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '49'){
				$table_49->addRow();
				$table_49->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_49->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_49->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '53'){
				$section->addTextBreak(1);
				$section->addText('Mayenne (53)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_53 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '53'){
				$table_53->addRow();
				$table_53->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_53->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_53->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '72'){
				$section->addTextBreak(1);
				$section->addText('Sarthe (72)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_72 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '72'){
				$table_72->addRow();
				$table_72->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_72->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_72->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '85'){
				$section->addTextBreak(1);
				$section->addText('Vendée (85)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_85 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '85'){
				$table_85->addRow();
				$table_85->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_85->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_85->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Provence-Alpes-Côte d'Azur ******/

		$section->addTextBreak(1);
		$section->addText('Provence-Alpes-Côte d\'Azur', $reg, $bg_region);

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '04'){
				$section->addTextBreak(1);
				$section->addText('Alpes-de-Haute-Provence (04)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_04 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '04'){
				$table_04->addRow();
				$table_04->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_04->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_04->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '05'){
				$section->addTextBreak(1);
				$section->addText('Hautes-Alpes (05)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_05 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '05'){
				$table_05->addRow();
				$table_05->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_05->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_05->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '06'){
				$section->addTextBreak(1);
				$section->addText('Alpes-Maritimes (06)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_06 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '06'){
				$table_06->addRow();
				$table_06->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_06->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_06->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '13' || strtolower($ville) == 'marseille')){
				$section->addTextBreak(1);
				$section->addText('Bouches-du-Rhône (13)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_13 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '13' || strtolower($ville) == 'marseille')){
				$table_13->addRow();
				$table_13->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_13->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_13->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '83'){
				$section->addTextBreak(1);
				$section->addText('Var (83)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_83 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '83'){
				$table_83->addRow();
				$table_83->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_83->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_83->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '84'){
				$section->addTextBreak(1);
				$section->addText('Vaucluse (84)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_84 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '84'){
				$table_84->addRow();
				$table_84->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_84->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_84->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** France métropolitaine (Dom-Tom) ******/

		$section->addTextBreak(1);
		$section->addText('France métropolitaine (Dom-Tom)', $reg, $bg_region);

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '971'){
				$section->addTextBreak(1);
				$section->addText('Guadeloupe (971)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_971 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '971'){
				$table_971->addRow();
				$table_971->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_971->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_971->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,3) == '972'){
				$section->addTextBreak(1);
				$section->addText('Martinique (972)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_972 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '972'){
				$table_972->addRow();
				$table_972->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_972->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_972->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '973'){
				$section->addTextBreak(1);
				$section->addText('Guyane (973)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_973 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '973'){
				$table_973->addRow();
				$table_973->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_973->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_973->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && $region == '988'){
				$section->addTextBreak(1);
				$section->addText('La Réunion (974)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_974 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '988'){
				$table_974->addRow();
				$table_974->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_974->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_974->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '975'){
				$section->addTextBreak(1);
				$section->addText('St Pierre et Miquelon (975)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_975 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '975'){
				$table_975->addRow();
				$table_975->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_975->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_975->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '976'){
				$section->addTextBreak(1);
				$section->addText('Mayotte (976)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_976 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '976'){
				$table_976->addRow();
				$table_976->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_976->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_976->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '987'){
				$section->addTextBreak(1);
				$section->addText('Polynésie Française (987)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_987 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '987'){
				$table_987->addRow();
				$table_987->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_987->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_987->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '988'){
				$section->addTextBreak(1);
				$section->addText('Nouvelle Calédonie (988)', $it_regular, $bg_sousregion);
				break;
			}
		}

		$table_988 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == 'France' && substr($bio->usr_cp_entreprise,0,3) == '988'){
				$table_988->addRow();
				$table_988->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_988->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_988->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Etrangere ******/

		$section->addTextBreak(1);
		$section->addText('Etrangere', $reg, $bg_region);

		$table_99 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail != 'France' && $bio->usr_pays_travail != ''){
				$table_99->addRow();
				$table_99->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_99->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_99->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}

		/****** Sans Region ******/

		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == '' && $bio->usr_cp_entreprise == '' && !in_array(strtolower($ville), $villes)){
				$section->addTextBreak(1);
				$section->addText('Sans Region', $reg, $bg_region);
				break;
			}
		}

		$table_000 = $section->addTable($tableStyle);
		foreach ($bios as $bio){
			$ville = $bio->usr_ville_entreprise;
			if ($bio->usr_pays_travail == '' && $bio->usr_cp_entreprise == '' && !in_array(strtolower($ville), $villes)){
				$table_000->addRow();
				$table_000->addCell(3000, $tableCellStyle)->addText(strtoupper($bio->usr_nom).' '.$bio->usr_prenom, $regular);
				$table_000->addCell(5150, $tableCellStyle)->addText($bio->usr_nom_entreprise, $regular);
				$table_000->addCell(2000, $tableCellStyle)->addText($bio->usr_ville_entreprise, $regular, $rightCellStyle);
			}
		}
		$section->addPageBreak();

		$section->addTextBreak(25);
		$section->addText(
			'Les Anciens de A à Z', 
			array('bold'=>true, 'size'=>48, 'name'=>'Liberation Serif'), 
			array('align' => 'center')
		);
		$section->addPageBreak();

		/*
		* Tri Par A - Z 
		*/
		foreach ($bios as $bio) {
			$textrun = $section->createTextRun();

			
			$m_mme = ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null;
			$textrun->addText($m_mme.' '.strtoupper($bio->usr_nom).' '.$bio->usr_prenom.' ', $bold);
			$textrun->addText('- '.$bio->usr_annee_sortie.' - '.$bio->usr_diplome, $regular);
			
			if($bio->usr_donnees == 'oui'){
				if($bio->usr_adresse_perso && $bio->usr_cp) {
					$section->addText(
						$bio->usr_adresse_perso.' ~ '.
						$bio->usr_cp.' '. 
						$bio->usr_ville.' - '. 
						$bio->usr_tel_mob,
						$regular
					);
				} else if($bio->usr_ville && $bio->usr_tel_mob){
					$section->addText(
						$bio->usr_adresse_perso.' '.
						$bio->usr_cp.' '. 
						$bio->usr_ville.' - '. 
						$bio->usr_tel_mob,
						$regular
					);
				} else {
					$section->addText(
						$bio->usr_adresse_perso.' '.
						$bio->usr_cp.' '. 
						$bio->usr_ville.' '. 
						$bio->usr_tel_mob,
						$regular
					);
				}

				$section->addText($bio->usr_email, $regular);
			}


			if($bio->usr_poste && $bio->usr_nom_entreprise) {
				$section->addText(
					$bio->usr_poste.' - '.
					$bio->usr_nom_entreprise.' ',
					$it_regular,
					array('indentation' => array('left' => 480,))
				);
			} else {
				$section->addText(
					$bio->usr_poste.' '.
					$bio->usr_nom_entreprise.' ',
					$it_regular,
					array('indentation' => array('left' => 480,))
				);
			}

			if($bio->usr_adresse_entreprise && $bio->usr_cp_entreprise) {
				$section->addText(
					$bio->usr_adresse_entreprise.' ~ '.
					$bio->usr_cp_entreprise.' '.
					$bio->usr_ville_entreprise.' - '.
					$bio->usr_tel_pro,
					$it_regular,
					array('indentation' => array('left' => 480,))
				);
			} elseif ($bio->usr_ville_entreprise && $bio->usr_tel_pro) {
				$section->addText(
					$bio->usr_adresse_entreprise.' '.
					$bio->usr_cp_entreprise.' '.
					$bio->usr_ville_entreprise.' - '.
					$bio->usr_tel_pro,
					$it_regular,
					array('indentation' => array('left' => 480,))
				);
			} else {
				$section->addText(
					$bio->usr_adresse_entreprise.' '.
					$bio->usr_cp_entreprise.' '.
					$bio->usr_ville_entreprise.' '.
					$bio->usr_tel_pro,
					$it_regular,
					array('indentation' => array('left' => 480,))
				);
			}

			$section->addText(
				$bio->usr_email_pro,
				$it_regular,
				array('indentation' => array('left' => 480,))
			);

			$section->addTextBreak(1);
		}


		$file = 'Annuaire.docx';
		header("Content-Description: File Transfer");
		header('Content-Disposition: attachment; filename="' . $file . '"');
		header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
		header('Content-Transfer-Encoding: binary');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Expires: 0');
		$xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$xmlWriter->save("php://output");
	}

	/*
	* Data export to MS Excel
	*/
	public function to_excel() 
	{
		ob_end_clean(); ob_start();
		Excel::create('Annuaire des diplômés', function($excel) {

			/*
			* Tout les données
			*/
			$excel->sheet('Toutes les données', function($sheet) {
				$sheet->setOrientation('portrait');
				$sheet->setPageMargin(array(
					0.25, 0.30, 0.25, 0.30
				));

				$sheet->setStyle(array(
					'font' => array(
						'name' => 'Calibri',
						'size' => 15
					)
				));

				$k = Users_bio::count()+1;
				$sheet->fromArray(Users_bio::all());
				while($k > 0){
					$sheet->row($k)->setHeight($k, 25);
					$k--;
				}
			});

		})->download('xls');
	}

	/*
	* Data import from Excel, Libre Office Calc etc.
	*/
	public function importExcel(Request $request)
	{
		$this->validate(request(), [
			'import_file' => 'file|max:5120|nullable',
		]);

		$file = $request->file('import_file');

		if($file){
			$data = Excel::load($file, function($reader) {})->get();
			if(!empty($data) && $data->count()){
				foreach ($data as $list) {
					foreach ($list as $key => $value) {
						$prenom = '';
						$p = '';
						$mail = strtolower(substr($value->prenom, 0, 1).$value->nom)."_";

						($value->telephone) 
							? $mail = $mail.preg_replace("/\D|\./", "", $value->telephone)
							: $mail = $mail.preg_replace("/-|\s|[00:00:00]/i", "", $value->date_de_naissance);

						$mail = $mail.'@temp.fr';

						$prenom_parts = preg_split('/-/', strtolower($value->prenom));

						for($i=0;$i<count($prenom_parts);$i++){
							$p = $p.ucfirst($prenom_parts[$i]).' ';
						}

						(count($prenom_parts) > 1) ? $p = preg_replace("/\s/i", "-",trim($p)): $p = trim($p);

						$p_parts = preg_split('/\s/', $p);

						for($i=0;$i<count($p_parts);$i++){
							$prenom = $prenom.ucfirst($p_parts[$i]).' ';
						}
						$prenom = trim($prenom);

						if ($value->mail) {
							$mail = $value->mail;
						}
						$insert[] = [
							'usr_nom' => $value->nom,
							'usr_prenom' => $prenom,
							'usr_email' => $mail,
							'usr_date_naiss' => $value->date_de_naissance,
							'usr_adresse_perso' => $value->adresse_1." ".$value->adresse_2." ".$value->adresse_3,
							'usr_cp' => $value->cp,
							'usr_ville' => $value->adresse_commune,
							'usr_tel_mob' => preg_replace("/\D|\./i", "", $value->telephone),
							'usr_pays' => $value->adresse_etranger,
							'usr_annee_sortie' => 1 + $value->annee_de_inscription,
							'usr_diplome' => $value->diplome,
						];

						$insert_user[] = [
							'name' => $prenom,
							'email' => $mail,
							'password' => bcrypt(str_random(12)),
						];
					}
				}

				if(!empty($insert)){
					try{
						foreach ($insert as $i) {
							Users_bio::updateOrCreate($i);
						}

						for($i=0;$i<count($insert_user); $i++) {
							$id = Users_bio::where('usr_email', $insert_user[$i]['email'])->get();

							$insert_user[$i]['id'] = $id[0]->user_id;
						}

						foreach ($insert_user as $iu) {
							User::updateOrCreate($iu);
						}
					} catch(\Exception $e) {
						return redirect()->back()->with('error', $e->errorInfo[2]);
					}
					return redirect()->back()->with('message', 'Vous avez bien importé des données');
				}
			} else {
				return redirect()->back()->with('error', 'Insertions n\'a pas reussie');
			}
		}	
		return back();
	}
}