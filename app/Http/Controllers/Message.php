<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\MessageToAdmin;

class Message extends Controller
{

	public function msg_to_admin(Request $request)
	{
		$this->validate(request(), [
			'nom_msg' => 'string|max:100',
			'usr_email' => 'required|string|email|max:255',
			'texte' => 'string|max:1000',
		]);

		\Mail::to('admin-anciens.esiab@univ-brest.fr')
					->cc('diplom.esiab@gmail.com')
					->send(new MessageToAdmin($request));

		return back()->with('message', 'Votre message a bien été envoyé');
	}
}
