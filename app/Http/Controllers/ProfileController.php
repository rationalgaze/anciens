<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Users_bio;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;
use File;
use Hash;

class ProfileController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$users_bio = Users_bio::where('usr_donnees', '!=', 'non')->orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->paginate(25);
		return view('profile.index', compact(['users_bio']));
	}

	public function par_secteur()
	{
		$bio_par_secteur = Users_bio::where('usr_donnees', '!=', 'non')->orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('profile.par_secteur', compact(['bio_par_secteur']));
	}

	public function par_region()
	{
		$bio_par_region = Users_bio::where('usr_donnees', '!=', 'non')->orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('profile.par_region', compact(['bio_par_region']));
	}

	public function par_parcours()
	{
		$bio_par_parcours = Users_bio::where('usr_donnees', '!=', 'non')->orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->get();

		return view('profile.par_parcours', compact(['bio_par_parcours']));
	}

	public function show(Users_bio $usr) {
		if($usr && $usr->usr_donnees != 'non'){
			return view('profile.show', compact('usr'));
		} else {
			return view('/errors/404');
		}
	}

	public function update(Request $request)
	{
		$this->validate(request(), [
			'tel_mobile' => 'digits_between:6,14|nullable',
			'tel_fixe' => 'digits_between:6,14|nullable',
			'date_naiss' => 'date_format:Y-m-d|nullable',
		]);

		$user_bio = Auth::user()->bio;

		$user_bio->usr_sex = $request->sex;
		$user_bio->usr_nom = $request->nom;
		$user_bio->usr_nom_naiss = $request->nom_naiss;
		$user_bio->usr_prenom = $request->prenom;
		$user_bio->usr_date_naiss = $request->date_naiss;
		$user_bio->usr_lieu_naiss = $request->lieu_naiss;
		$user_bio->usr_adresse_perso = $request->adresse_perso;
		$user_bio->usr_cp = $request->cp;
		$user_bio->usr_ville = $request->ville;
		$user_bio->usr_pays = $request->pays;
		$user_bio->usr_tel_mob = $request->tel_mobile;
		$user_bio->usr_tel_fixe = $request->tel_fixe;
		$user_bio->usr_annee_entree = $request->annee_entree;
		$user_bio->usr_annee_sortie = $request->annee_sortie;
		$user_bio->usr_diplome = $request->diplome;
		$user_bio->usr_fb = $request->fb;

		$user_bio->save();

		return redirect()->route('profile'); 
	}

	public function change_email(Request $request)
	{
		$this->validate(request(), [
			'email' => 'required|string|email|max:255|unique:users|unique:usersbio_usr,usr_email',
		]);

		$user_bio = Auth::user()->bio;
		$user = Auth::user();

		$user->email = $request->email;
		$user_bio->usr_email = $request->email;

		$user_bio->save();
		$user->save();

		return redirect()->route('profile'); 
	}

	public function change_pass(Request $request)
	{
		$this->validate(request(), [
			'mdp_act' => 'required|string|min:6',
			'mdp_new' => 'required|string|min:6|confirmed',
		]);

		$user = Auth::user();

		if(Hash::check($request->mdp_act, $user->password)){

			$user->password = bcrypt($request->mdp_new);
			$user->save();

			return redirect()->back()->with('message', 'Vous avez bien changé votre mot de passe'); 
		}
		else
		{
			return redirect()->back()->withErrors('Votre ancien mot de passe n\'est pas correct' );
		}
	}

	public function change_avatar(Request $request)
	{
		$this->validate(request(), [
			'photo' => 'image|mimes:jpg,jpeg,png,gif|max:5120|nullable',
		]);

		$user_bio = Auth::user()->bio;

		$photo = $request->file('photo');
		$name = '';

		if($photo){
			$ext = $photo->guessClientExtension();
			$name = 'Avatar_'.$user_bio->user_id.'_'.$user_bio->usr_nom.'_'.$user_bio->usr_prenom.'_'.uniqid().".{$ext}";
			$photo = Image::make($photo->getRealPath());
			$path = "storage/avatars/{$name}";
			$photo->fit(240, 240, function ($constraint) {
				$constraint->aspectRatio();
			})->save(public_path($path));

			$old_name = $user_bio->usr_img;
			File::delete("storage/avatars/{$old_name}");
			$user_bio->usr_img = $name;
		}


		$user_bio->save();

		return redirect()->back(); 
	}

	public function update_pro(Request $request)
	{
		$this->validate(request(), [
			'telephone_fixe_pro' => 'digits_between:6,14|nullable',
			'cv' => 'max:10000|mimes:pdf,odt,doc,docx,rtf|nullable',
			'courriel_professionnel' => 'string|email|max:255|nullable',
		]);

		$user_bio = Auth::user()->bio;

		$file = $request->file('cv');
		$name = '';

		if($file){
			$ext = $file->guessClientExtension();
			$name = 'CV_'.$user_bio->user_id.'_'.$user_bio->usr_nom.'_'.$user_bio->usr_prenom.'_'.uniqid().".{$ext}";
			$file->storeAs('public/cv_storage', $name);

			File::delete("storage/cv_storage/{$user_bio->usr_cv}");
		}

		$user_bio->usr_activite = $request->activite;
		$user_bio->usr_poste = $request->profession;
		$user_bio->usr_contrat = $request->type_de_contrat;
		$user_bio->usr_annee_emb = $request->anne_embauche;
		$user_bio->usr_nom_entreprise = $request->entreprise;
		$user_bio->usr_adresse_entreprise = $request->adressepro;
		$user_bio->usr_cp_entreprise = $request->code_postalpro;
		$user_bio->usr_ville_entreprise = $request->ville_pro;
		$user_bio->usr_pays_travail = $request->payspro;
		$user_bio->usr_tel_pro = $request->telephone_fixe_pro;
		$user_bio->usr_email_pro = $request->courriel_professionnel;
		$user_bio->usr_secteur = $request->secteur;
		$user_bio->usr_site_web = $request->adresse_site_web;
		$user_bio->usr_linkedin = $request->linkedin;
		$user_bio->usr_viadeo = $request->viadeo;
		$user_bio->usr_cv = $name;
		$user_bio->usr_donnees = $request->ok;

		$user_bio->save();

		return redirect()->route('profile'); 
	}

	public function a()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'a%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function b()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'B%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function c()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'c%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function d()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'd%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function e()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'e%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function f()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'f%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function g()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'g%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function h()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'h%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function i()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'i%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function j()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'j%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function k()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'k%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function l()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'l%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function m()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'm%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function n()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'n%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function o()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'o%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function p()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'p%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function q()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'q%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function r()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'r%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function s()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 's%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function t()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 't%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function u()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'u%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function v()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'v%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function w()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'w%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function x()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'x%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function y()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'y%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function z()
	{
		$users_bio = Users_bio::where('usr_nom', 'like', 'z%')->orderBy('usr_nom', 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function all()
	{
		$users_bio = Users_bio::orderBy(DB::raw('ISNULL(`usr_nom`), `usr_nom`'), 'asc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}

	public function par_annee()
	{
		$users_bio = Users_bio::orderBy(DB::raw('ISNULL(`usr_annee_sortie`), `usr_annee_sortie`'), 'desc')->paginate(25);

		return view('profile.index', compact('users_bio'));
	}
}
