
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
		el: '#app'
});

$(function () {
	$('[data-tooltip="true"]').tooltip();
});

$('#delete').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('delete'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('form').attr('action', '/admin/delete/' + recipient);
});

$('#delete_adm').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var recipient = button.data('delete_adm'); // Extract info from data-* attributes

	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this);
	modal.find('form').attr('action', '/admin/delete_admin/' + recipient);
});

$(function () {
	new Clipboard('.cpy_cbd');
});

//generation of temporary password
$('.gen').click(function(e) {
	e.preventDefault();
	$('#pass').val(generatePassword());
});

function generatePassword() {
		var length = 12,
				charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
				retVal = "";
		for (var i = 0, n = charset.length; i < length; ++i) {
				retVal += charset.charAt(Math.floor(Math.random() * n));
		}
		return retVal;
}

//make active the tab with users if url have page=number_here or /a-z (http://127.0.0.1:8000/profile/g http://127.0.0.1:8000/profile/all?page=2)
$(document).ready(function(e) {

	var rx = /[=]([0-9])*$|[\/][a-z]$|([\/]par_annee$)/;
	var url = document.URL; 
	if(rx.test(url)){
		$('#profile').removeClass('active');
		$('a.profile').parent().removeClass('active');
		$('#etudiants').addClass('active');
		$('a.etudiants').parent().addClass('active');
	}
});

$('#btn_msg_toadm').click(function(e) {
	e.preventDefault();
	$('#msg_toadm').toggle(300);
});

if($(document).find('#error').length == 1){
	$('#msg_toadm').show();
} else {
	$('#msg_toadm').hide();
}