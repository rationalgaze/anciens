@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li class="active"><a href="#profile" role="tab" aria-controls="profile" data-toggle="tab" class="profile">Mon profil</a></li>
						<li><a href="#etudiants" role="tab" aria-controls="etudiants" data-toggle="tab" class="etudiants">Liste des Diplômés</a></li>
						<li class="pull-right"><a href="#" data-toggle="modal" data-target="#modPass" class="btn btn-danger btn-sm mdpchange">Changement du mot de passe</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			@if(Session::has('message'))
				<div class="alert alert-dismissible alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ Session::get('message') }}
				</div>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="profile">
					<div class="panel panel-default">

						@if(Auth::check())
						<div class="panel-heading">{{ Auth::user()->name }}</div>
							<div class="col-xs-12">
								<h1 class="personal">Données personnelles
									<a href="#" data-toggle="modal" data-target="#modPersonalData" data-placement="right" title="Modifier vos données personnelles" data-tooltip="true">
										<i class="material-icons edit">edit</i>
									</a>
									</a>
								</h1>
								<hr>
							</div>
						<div class="panel-body">
							<div class="panel-body">
								<section class="personal">
									<div class="row">
										<div class="col-xs-12 col-sm-5">
											<div class="avatar text-center">
												
												@if(Auth::user()->bio->usr_img)
													<div class="halo">
														<a href="#" data-toggle="modal" data-target="#modAvatar" data-placement="right" title="Modifier votre photo" data-tooltip="true">
															<img src="{{ asset('storage/avatars/'.Auth::user()->bio->usr_img) }}" class="img-circle">
															<span class="icn"><i class="material-icons">edit</i></span>
														</a>
													</div>
												@else
													<div class="halo">
														<a href="#" data-toggle="modal" data-target="#modAvatar" data-placement="right" title="Modifier votre photo" data-tooltip="true">
															<img src="http://dummyimage.com/240/434343.jpg/e7e7e7&text= Avatar" class="img-circle">
															<span class="icn"><i class="material-icons">edit</i></span>
														</a>
													</div>
												@endif
											</div>
										</div>
										<div class="col-xs-12 col-sm-7 info">
											@if(Auth::user()->bio->usr_prenom || Auth::user()->bio->usr_nom)
												<h2 class="ancien_title text-capitalize" style="color: #e7e7e7">
													<strong>
														{{ strtolower(Auth::user()->bio->usr_prenom).' '. strtolower(Auth::user()->bio->usr_nom) }}
														@if(Auth::user()->bio->usr_nom_naiss)
															{{ ' ('. strtolower(Auth::user()->bio->usr_nom_naiss).')' }}
														@endif
													</strong>
													<a href="#" data-toggle="modal" data-target="#modPersonalData" data-placement="right" title="Modifier vos données personnelles" data-tooltip="true">
														<i class="material-icons edit">edit</i>
													</a>
												</h2>
											@else
												<h2 class="ancien_title text-capitalize" style="color: #e7e7e7">
													<strong>
														Votre Nom et Prénom
													</strong>
													<a href="#" data-toggle="modal" data-target="#modPersonalData" data-placement="right" title="Modifier vos données personnelles" data-tooltip="true">
														<i class="material-icons edit">edit</i>
													</a>
												</h2>
											@endif

											@if(Auth::user()->bio->usr_adresse_perso || Auth::user()->bio->usr_cp || Auth::user()->bio->usr_ville || Auth::user()->bio->usr_pays)
												<p class="adress text-capitalize">
													<i class="material-icons"><small>location_on</small></i>
													<small>
														{{ 
															(Auth::user()->bio->usr_cp)?Auth::user()->bio->usr_adresse_perso.', '.Auth::user()->bio->usr_cp.' '.strtolower(Auth::user()->bio->usr_ville).', '.Auth::user()->bio->usr_pays :
															Auth::user()->bio->usr_adresse_perso.' '.Auth::user()->bio->usr_cp.' '.Auth::user()->bio->usr_ville.' '.Auth::user()->bio->usr_pays
														}}
													</small>
												</p>
											@endif

											@if(Auth::user()->bio->usr_date_naiss)
												<p><i class="material-icons">cake</i>{{ 
													(Auth::user()->bio->usr_lieu_naiss)?Carbon\Carbon::parse(Auth::user()->bio->usr_date_naiss)->format('d F Y').', '.Auth::user()->bio->usr_lieu_naiss :Carbon\Carbon::parse(Auth::user()->bio->usr_date_naiss)->format('d F Y') }}</p>
											@endif

											@if(Auth::user()->bio->usr_email)
												<p class="mail">
													<i class="material-icons">mail_outline</i>
													{{ Auth::user()->bio->usr_email }} 
													<a href="#" data-toggle="modal" data-target="#modEmail" data-placement="right" title="Changer" data-tooltip="true">
														<i class="material-icons edit">edit</i>
													</a>
												</p>
											@endif

											@if((Auth::user()->bio->usr_annee_entree || Auth::user()->bio->usr_annee_sortie || Auth::user()->bio->usr_diplome) && (Auth::user()->bio->usr_annee_entree != '?' || Auth::user()->bio->usr_annee_sortie != '?' || Auth::user()->bio->usr_diplome != ''))
												<p>
													<i class="material-icons">school</i>
													@if(Auth::user()->bio->usr_annee_entree && Auth::user()->bio->usr_annee_sortie && Auth::user()->bio->usr_diplome)
														{{ Auth::user()->bio->usr_annee_entree.' - '.Auth::user()->bio->usr_annee_sortie.' - '.Auth::user()->bio->usr_diplome }}
													@elseif(!Auth::user()->bio->usr_diplome || Auth::user()->bio->usr_diplome == '')
														{{ Auth::user()->bio->usr_annee_entree.' - '.Auth::user()->bio->usr_annee_sortie }}
													@else
														{{ Auth::user()->bio->usr_annee_entree.' '.Auth::user()->bio->usr_annee_sortie.' '.Auth::user()->bio->usr_diplome }}
													@endif
												</p>
											@endif

											@if(Auth::user()->bio->usr_tel_mob||Auth::user()->bio->usr_tel_fixe)
												<p>
													<i class="material-icons">stay_current_portrait</i>
													@if(Auth::user()->bio->usr_tel_fixe) 
														{{ chunk_split(Auth::user()->bio->usr_tel_mob, 2, " ").' ' }}<i class="material-icons" style="margin: 0 5px;">phone</i>{{ ' '.chunk_split(Auth::user()->bio->usr_tel_fixe, 2, " ") }}
													@else
														{{ chunk_split(Auth::user()->bio->usr_tel_mob,2, " ") }}
													@endif
												</p>
											@endif

											@if(Auth::user()->bio->usr_fb)
												<p class="socials mail">
													<i class="material-icons fa fa-facebook-square"></i>
													<a href="{{Auth::user()->bio->usr_fb}}" target="_blank">
														@if(Auth::user()->bio->usr_nom || Auth::user()->bio->usr_prenom)
															{{ Auth::user()->bio->usr_nom." ".Auth::user()->bio->usr_prenom }} 
														@else
															<span>facebook</span>
														@endif
													</a>
												</p>
											@endif
										</div>
									</div>
								</section>

								<section class="pro">
									<div class="row">
										<h1>Données professionnelles 
											<a href="#" data-toggle="modal" data-target="#modProData" data-placement="right" title="Modifier vos données professionnelles" data-tooltip="true">
												<i class="material-icons edit">edit</i>
											</a>
										</h1>
										<hr>

										@if(Auth::user()->bio->usr_activite == 'Sans emploi')
											<p>Sans emploi</p>
											@if(Auth::user()->bio->usr_cv)
												<p>
													<i class="material-icons">content_paste</i>
													<a href="{{ asset('storage/cv_storage/'.Auth::user()->bio->usr_cv) }}" alt="{{ Auth::user()->bio->usr_prenom.' '.Auth::user()->bio->usr_nom }}" target="_blank">voir CV</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_linkedin)
												<p class="socials mail">
													<i class="material-icons fa fa-linkedin-square"></i>
													<a href="{{ Auth::user()->bio->usr_linkedin }}" target="_blank">
														LinkedIn
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_viadeo)
												<p class="socials mail">
													<i class="material-icons fa fa-viadeo-square"></i>
													<a href="{{ Auth::user()->bio->usr_viadeo }}" target="_blank">
														Viadeo
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_donnees == 'oui')
												<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
											@elseif(Auth::user()->bio->usr_donnees == 'pro')
												<div>J'accepte l'utilisation que mes données professionnelles</div>
											@else
												<div>J'accepte pas l'utilisation de mes données</div>
											@endif
										@elseif(Auth::user()->bio->usr_activite == 'Retraite')
											<p>En retraite</p>
											@if(Auth::user()->bio->usr_cv)
												<p>
													<i class="material-icons">content_paste</i>
													<a href="{{ asset('storage/cv_storage/'.Auth::user()->bio->usr_cv) }}" alt="{{ Auth::user()->bio->usr_prenom.' '.Auth::user()->bio->usr_nom }}" target="_blank">voir CV</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_linkedin)
												<p class="socials mail">
													<i class="material-icons fa fa-linkedin-square"></i>
													<a href="{{ Auth::user()->bio->usr_linkedin }}" target="_blank">
														LinkedIn
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_viadeo)
												<p class="socials mail">
													<i class="material-icons fa fa-viadeo-square"></i>
													<a href="{{ Auth::user()->bio->usr_viadeo }}" target="_blank">
														Viadeo
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_donnees == 'oui')
												<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
											@elseif(Auth::user()->bio->usr_donnees == 'pro')
												<div>J'accepte l'utilisation que mes données professionnelles</div>
											@else
												<div>J'accepte pas l'utilisation de mes données</div>
											@endif
										@else
											@if(Auth::user()->bio->usr_poste && Auth::user()->bio->usr_nom_entreprise && Auth::user()->bio->usr_secteur && Auth::user()->bio->usr_contrat)
												<p>
													<i class="material-icons">work</i>
													{{ Auth::user()->bio->usr_poste.' - '.Auth::user()->bio->usr_nom_entreprise.' ( '.Auth::user()->bio->usr_secteur.' ) - '.Auth::user()->bio->usr_contrat }}
												</p>
											@elseif(Auth::user()->bio->usr_poste && Auth::user()->bio->usr_nom_entreprise && Auth::user()->bio->usr_secteur)
												<p>
													<i class="material-icons">work</i>
													{{ Auth::user()->bio->usr_poste.' - '.Auth::user()->bio->usr_nom_entreprise.' ( '.Auth::user()->bio->usr_secteur.' )' }}
												</p>
											@elseif(Auth::user()->bio->usr_poste && Auth::user()->bio->usr_nom_entreprise)
												<p>
													<i class="material-icons">work</i>
													{{ Auth::user()->bio->usr_poste.' - '.Auth::user()->bio->usr_nom_entreprise }}
												</p>
											@elseif(Auth::user()->bio->usr_poste && Auth::user()->bio->usr_nom_entreprise && Auth::user()->bio->usr_contrat)
												<p>
													<i class="material-icons">work</i>
													{{ Auth::user()->bio->usr_poste.' - '.Auth::user()->bio->usr_nom_entreprise .' - '.Auth::user()->bio->usr_contrat }}
												</p>
											@elseif(Auth::user()->bio->usr_poste||Auth::user()->bio->usr_nom_entreprise ||Auth::user()->bio->usr_secteur||Auth::user()->bio->usr_contrat)
												<p>
													<i class="material-icons">work</i>
													{{ Auth::user()->bio->usr_poste.' '.Auth::user()->bio->usr_nom_entreprise .' '.Auth::user()->bio->usr_secteur.' '.Auth::user()->bio->usr_contrat }}
												</p>
											@endif

											@if(Auth::user()->bio->usr_adresse_entreprise || Auth::user()->bio->usr_cp_entreprise || Auth::user()->bio->usr_ville_entreprise || Auth::user()->bio->usr_pays_travail)
												<p class="adress-pro">
													<i class="material-icons">location_on</i>
														{{ 
															(Auth::user()->bio->usr_cp_entreprise && Auth::user()->bio->usr_pays_travail)
																?
																	Auth::user()->bio->usr_adresse_entreprise.', '.Auth::user()->bio->usr_cp_entreprise.' '.Auth::user()->bio->usr_ville_entreprise.', '.Auth::user()->bio->usr_pays_travail
																:
																	Auth::user()->bio->usr_adresse_entreprise.' '.Auth::user()->bio->usr_cp_entreprise.' '.Auth::user()->bio->usr_ville_entreprise.' '.Auth::user()->bio->usr_pays_travail
														}}
												</p>
											@endif

											@if(Auth::user()->bio->usr_email_pro)
												<p class="mail">
													<a href="mailto:{{ Auth::user()->bio->usr_email_pro }}"><i class="material-icons">mail_outline</i>{{ Auth::user()->bio->usr_email_pro }}</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_tel_pro)
												<p>
													<i class="material-icons">phone</i>
													{{ chunk_split(Auth::user()->bio->usr_tel_pro, 2, " ") }}
												</p>
											@endif

											@if(Auth::user()->bio->usr_site_web)
												<p>
													<i class="material-icons">public</i>
													<a href="http://www.{{ Auth::user()->bio->usr_site_web }}/" target="_blank">{{ Auth::user()->bio->usr_site_web }}</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_cv)
												<p>
													<i class="material-icons">content_paste</i>
													<a href="{{ asset('storage/cv_storage/'.Auth::user()->bio->usr_cv) }}" alt="{{ Auth::user()->bio->usr_prenom.' '.Auth::user()->bio->usr_nom }}" target="_blank">voir CV</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_linkedin)
												<p class="socials mail">
													<i class="material-icons fa fa-linkedin-square"></i>
													<a href="{{ Auth::user()->bio->usr_linkedin }}" target="_blank">
														LinkedIn
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_viadeo)
												<p class="socials mail">
													<i class="material-icons fa fa-viadeo-square"></i>
													<a href="{{ Auth::user()->bio->usr_viadeo }}" target="_blank">
														Viadeo
													</a>
												</p>
											@endif

											@if(Auth::user()->bio->usr_donnees == 'oui')
												<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
											@elseif(Auth::user()->bio->usr_donnees == 'pro')
												<div>J'accepte l'utilisation que mes données professionnelles</div>
											@else
												<div>J'accepte pas l'utilisation de mes données</div>
											@endif
										@endif
									</div>
								</section>
							</div>
						</div>
						@endif
					</div>
				</div>

				<div class="tab-pane" id="etudiants">
					<div class="panel panel-default">
						<div class="panel-heading">Liste des Diplômés</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="#paraz" class="btn btn-default btn-xs active disabled">A - Z</a>
										<a href="{{ route('profile.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
										<a href="{{ route('profile.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
										<a href="{{ route('profile.par_parcours') }}" class="btn btn-default btn-xs">Tri par parcours</a>
										<a href="{{ route('profile.par_annee') }}" class="btn btn-default btn-xs">Tri par année</a>
									</div>
								</div>
								<hr>
								<div class="col-xs-12 text-center az_pages">
									<a class="az_btn" href="{{route('profile.a')}}">A</a>
									<a class="az_btn" href="{{route('profile.b')}}">B</a>
									<a class="az_btn" href="{{route('profile.c')}}">C</a>
									<a class="az_btn" href="{{route('profile.d')}}">D</a>
									<a class="az_btn" href="{{route('profile.e')}}">E</a>
									<a class="az_btn" href="{{route('profile.f')}}">F</a>
									<a class="az_btn" href="{{route('profile.g')}}">G</a>
									<a class="az_btn" href="{{route('profile.h')}}">H</a>
									<a class="az_btn" href="{{route('profile.i')}}">I</a>
									<a class="az_btn" href="{{route('profile.j')}}">J</a>
									<a class="az_btn" href="{{route('profile.k')}}">K</a>
									<a class="az_btn" href="{{route('profile.l')}}">L</a>
									<a class="az_btn" href="{{route('profile.m')}}">M</a>
									<a class="az_btn" href="{{route('profile.n')}}">N</a>
									<a class="az_btn" href="{{route('profile.o')}}">O</a>
									<a class="az_btn" href="{{route('profile.p')}}">P</a>
									<a class="az_btn" href="{{route('profile.q')}}">Q</a>
									<a class="az_btn" href="{{route('profile.r')}}">R</a>
									<a class="az_btn" href="{{route('profile.s')}}">S</a>
									<a class="az_btn" href="{{route('profile.t')}}">T</a>
									<a class="az_btn" href="{{route('profile.u')}}">U</a>
									<a class="az_btn" href="{{route('profile.v')}}">V</a>
									<a class="az_btn" href="{{route('profile.w')}}">W</a>
									<a class="az_btn" href="{{route('profile.x')}}">X</a>
									<a class="az_btn" href="{{route('profile.y')}}">Y</a>
									<a class="az_btn" href="{{route('profile.z')}}">Z</a>
									<a class="az_btn" href="{{route('profile.all')}}">Tous</a>
								</div>
							</div>
							<div role="tabpanel2" class="tab-pane active" id="paraz">
								@php $i=$users_bio->firstItem(); @endphp
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12 col-xs-12 text-center">
												{{ $users_bio->links() }}
											</div>
										</div>
										<div class="row">
											<div class="table-responsive">
												<table class="table table-striped task-table">
													<thead>
														<tr>
															<th width="20px">&#8470;</th>
															<th width="200">
																<div>Personne</div>
																<div>(Nom, Prénom, E-mail)</div>
															</th>
															<th>Année <br>de sortie</th>
															<th>
																<div>Poste,</div>
																<div>Entreprise</div>
															</th>
															<th>
																<div>Coordonnées de l'entreprise</div>
															</th>
															<th width="10">
																<div>CV</div>
															</th>
															<th width="90">
																<div>Réseaux Sociaux</div>
															</th>
														</tr>
													</thead>
													<tbody>
														@foreach($users_bio as $bio)
															@if ($users_bio && $bio->usr_donnees != 'non')
															<tr>
																<td class="table-text">
																	<div>{{ $i++ }}</div>
																</td>
																<td class="table-text">
																	<div>
																		@if($bio->usr_sex == 'H') M @endif
																		@if($bio->usr_sex == 'F') MME @endif
																		<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																			<strong style="color: #ffdc2b;">{{ $bio->usr_nom }}</strong> 
																			{{ $bio->usr_prenom }}
																		</a>
																	</div>
																	<div>
																		<small><a style="color: #999;" href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></small>
																	</div>
																</td>
																<td class="table-text">
																	<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																</td>
																<td class="table-text">
																	<div><em><small>{{ $bio->usr_poste }}</small></em></div>
																	<div>{{ $bio->usr_nom_entreprise }}</div>
																</td>
																<td>
																	<div>
																		{{ $bio->usr_adresse_entreprise }} 
																		{{ $bio->usr_cp_entreprise }} 
																		{{ $bio->usr_ville_entreprise }}
																	</div>
																	<div>{{ $bio->usr_pays_travail }}</div>
																</td>
																<td>
																	@if($bio->usr_cv)
																		<div class="socials_icn">
																			<a href="{{ asset('storage/cv_storage/'.$bio->usr_cv) }}" alt="{{ $bio->usr_prenom.' '.$bio->usr_nom }}" target="_blank"><i class="material-icons">content_paste</i></a>
																		</div>
																	@endif
																</td>
																<td>
																	<div class="socials_icn">
																		@if($bio->usr_fb)
																			<a href="{{$bio->usr_fb}}" target="_blank">
																				<i class="material-icons fa fa-facebook-square"></i>
																			</a>
																		@endif
																		@if($bio->usr_linkedin)
																			<a href="{{$bio->usr_linkedin}}" target="_blank">
																				<i class="material-icons fa fa-linkedin-square"></i>
																			</a>
																		@endif
																		@if($bio->usr_viadeo)
																			<a href="{{$bio->usr_viadeo}}" target="_blank">
																				<i class="material-icons fa fa-viadeo-square"></i>
																			</a>
																		@endif
																	</div>
																</td>
															</tr>
															@endif
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
										<div class="panel-footer">
											<div class="row">
												<div class="col-md-12 col-xs-12 text-center">
														{{ $users_bio->links() }}
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-12 text-center">
												<div class="btn-group btn-group-sm nav"> 
													<a href="#paraz" class="btn btn-default btn-xs active disabled">A - Z</a>
													<a href="{{ route('profile.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
													<a href="{{ route('profile.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
													<a href="{{ route('profile.par_parcours') }}" class="btn btn-default btn-xs">Tri par parcours</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Changing Image -->
	<div class="modal fade" id="modAvatar" tabindex="-1" role="dialog" aria-labelledby="Modify Avatar">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong>Changement d'Image</strong></h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('change_avatar') }}" enctype="multipart/form-data">
						{{ csrf_field() }}

						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
							<label for="photo" class="col-md-4 control-label">Votre photo</label>

							<div class="col-sm-6" data-placement="top" title="Formats possibles: jpg,jpeg,png,gif" data-tooltip="true">

								<input type="file" name="photo" id="photo_hidden" class="upl_hidden" onchange="document.getElementById('helpBlock_img').innerHTML=this.value.replace(/.*[\/\\]/, ''); $('#helpBlock_img').removeClass('hidden')">

								<button class="btn btn-success" onclick="event.preventDefault(); document.getElementById('photo_hidden').click();">Chercher sur le disque</button>
								<span id="helpBlock_img" class="help-block hidden" style="color: #434343; margin-bottom: 0;"></span>

								@if ($errors->has('photo'))
									<span class="help-block">
										<strong>{{ $errors->first('photo') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Enregistrer
								</button>
								<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
									Retour
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Changing E-mail -->
	<div class="modal fade" id="modEmail" tabindex="-1" role="dialog" aria-labelledby="Modify Email">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong>Changement d'E-mail</strong></h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('change_email') }}">
						{{ csrf_field() }}

						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">E-mail</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->bio->usr_email }}" required placeholder="E-mail">

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Enregistrer
								</button>
								<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
									Retour
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Changing Password -->
	<div class="modal fade" id="modPass" tabindex="-1" role="dialog" aria-labelledby="Modify Mdp">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h5 class="modal-title" id="myModalLabel"><strong>Changement du mot de passe</strong></h5>
				</div>
				<div class="modal-body">
					<form role="form" method="POST" action="{{ route('change_pass') }}">
						{{ csrf_field() }}

						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="form-group{{ $errors->has('mdp_act') ? ' has-error' : '' }}">
							<label for="mdp_act" class="control-label">Entrez votre mot de passe actuel</label>

							<input id="mdp_act" type="password" class="form-control" name="mdp_act" required>

							@if ($errors->has('mdp_act'))
								<span class="help-block">
									<strong>{{ $errors->first('mdp_act') }}</strong>
								</span>
							@endif
						</div>

						<div class="form-group{{ $errors->has('mdp_new') ? ' has-error' : '' }}">
							<label for="mdp_new	" class="control-label">Entrez votre nouveau mot de passe</label>

							<input id="mdp_new" type="password" class="form-control" name="mdp_new" required>

							@if ($errors->has('mdp_new'))
								<span class="help-block">
									<strong>{{ $errors->first('mdp_new') }}</strong>
								</span>
							@endif
						</div>

						<div class="form-group">
							<label for="mdp_new-confirm" class="control-label">Confirmez votre nouveau mot de passe</label>

							<input id="mdp_new-confirm" type="password" class="form-control" name="mdp_new_confirmation" required>
						</div>

						<div class="form-group">
								<button type="submit" class="btn btn-primary">
									Enregistrer
								</button>
								<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
									Retour
								</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Donnees Perso -->
	<div class="modal fade" id="modPersonalData" tabindex="-1" role="dialog" aria-labelledby="Modify Personal Data">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong>Formulaire de saisie personnel</strong></h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('update') }}">
						{{ csrf_field() }}

						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="form-group">
							<label for="name" class="col-md-4 control-label">Êtes-vous un/une</label>

							<div class="col-md-6">
								<label class="radio-inline">
									<input type="radio" name="sex" id="sex_h" value="H"
										@if ( Auth::user()->bio->usr_sex == "H")
											checked
										@endif
									>Homme
								</label>
								<label class="radio-inline">
									<input type="radio" name="sex" id="sex_f" value="F"
									@if ( Auth::user()->bio->usr_sex == "F")
										checked
									@endif
									>Femme
								</label>
							</div>
						</div>

						<div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
							<label for="nom" class="col-md-4 control-label">Nom</label>

							<div class="col-md-6">
								<input id="nom" type="text" class="form-control" name="nom" value="{{ Auth::user()->bio->usr_nom }}" placeholder="Nom" autofocus>

								@if ($errors->has('nom'))
									<span class="help-block">
										<strong>{{ $errors->first('nom') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('nom_naiss') ? ' has-error' : '' }}">
							<label for="nom_naiss" class="col-md-4 control-label">Nom de naissance</label>

							<div class="col-md-6">
								<input id="nom_naiss" type="text" class="form-control" name="nom_naiss" value="{{ Auth::user()->bio->usr_nom_naiss }}" placeholder="Nom de naissance">

								@if ($errors->has('nom_naiss'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_naiss') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }}">
							<label for="prenom" class="col-md-4 control-label">Prénom</label>

							<div class="col-md-6">
								<input id="prenom" type="text" class="form-control" name="prenom" value="{{ Auth::user()->bio->usr_prenom }}" placeholder="Prénom">

								@if ($errors->has('prenom'))
									<span class="help-block">
										<strong>{{ $errors->first('prenom') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('date_naiss') ? ' has-error' : '' }}">
							<label for="date_naiss" class="col-md-4 control-label">Date de naissance</label>

							<div class="col-md-6">
								<input id="date_naiss" type="date" class="form-control" name="date_naiss" value="{{ Auth::user()->bio->usr_date_naiss }}" placeholder="Format : aaaa-mm-jj">

								@if ($errors->has('date_naiss'))
									<span class="help-block">
										<strong>{{ $errors->first('date_naiss') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('lieu_naiss') ? ' has-error' : '' }}">
							<label for="lieu_naiss" class="col-md-4 control-label">Lieu de naissance</label>

							<div class="col-md-6">
								<input id="lieu_naiss" type="text" class="form-control" name="lieu_naiss" value="{{ Auth::user()->bio->usr_lieu_naiss }}" placeholder="Lieu de naissance">

								@if ($errors->has('lieu_naiss'))
									<span class="help-block">
										<strong>{{ $errors->first('lieu_naiss') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('adresse_perso') ? ' has-error' : '' }}">
							<label for="adresse_perso" class="col-md-4 control-label">Adresse personnelle</label>

							<div class="col-md-6">
								<input id="adresse_perso" type="text" class="form-control" name="adresse_perso" value="{{ Auth::user()->bio->usr_adresse_perso }}" placeholder="Ex.: 2 rue de Brest">

								@if ($errors->has('adresse_perso'))
									<span class="help-block">
										<strong>{{ $errors->first('adresse_perso') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('cp') ? ' has-error' : '' }}">
							<label for="cp" class="col-md-4 control-label">Code postal</label>

							<div class="col-md-6">
								<input id="cp" type="text" class="form-control" name="cp" value="{{ Auth::user()->bio->usr_cp }}" placeholder="Ex.: 29200">

								@if ($errors->has('cp'))
									<span class="help-block">
										<strong>{{ $errors->first('cp') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('ville') ? ' has-error' : '' }}">
							<label for="ville" class="col-md-4 control-label">Ville</label>

							<div class="col-md-6">
								<input id="ville" type="text" class="form-control" name="ville" value="{{ Auth::user()->bio->usr_ville }}" placeholder="Ex.: Brest">

								@if ($errors->has('ville'))
									<span class="help-block">
										<strong>{{ $errors->first('ville') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('pays') ? ' has-error' : '' }}">
							<label for="ville" class="col-md-4 control-label">Pays</label>

							<div class="col-md-6">
								{!! Form::select('pays',
									[
										'' =>'Sélectionnez un pays',
										'France' =>'France',

										'Afghanistan'=>'Afghanistan',
										'Afrique_Centrale'=>'Afrique_Centrale',
										'Afrique_du_sud'=>'Afrique_du_Sud',
										'Albanie'=>'Albanie',
										'Algerie'=>'Algerie',
										'Allemagne'=>'Allemagne',
										'Andorre'=>'Andorre',
										'Angola'=>'Angola',
										'Anguilla'=>'Anguilla',
										'Arabie_Saoudite'=>'Arabie_Saoudite',
										'Argentine'=>'Argentine',
										'Armenie'=>'Armenie',
										'Australie'=>'Australie',
										'Autriche'=>'Autriche',
										'Azerbaidjan'=>'Azerbaidjan',
										'Bahamas'=>'Bahamas',
										'Bangladesh'=>'Bangladesh',
										'Barbade'=>'Barbade',
										'Bahrein'=>'Bahrein',
										'Belgique'=>'Belgique',
										'Belize'=>'Belize',
										'Benin'=>'Benin',
										'Bermudes'=>'Bermudes',
										'Bielorussie'=>'Bielorussie',
										'Bolivie'=>'Bolivie',
										'Botswana'=>'Botswana',
										'Bhoutan'=>'Bhoutan',
										'Boznie_Herzegovine'=>'Boznie_Herzegovine',
										'Bresil'=>'Bresil',
										'Brunei'=>'Brunei',
										'Bulgarie'=>'Bulgarie',
										'Burkina_Faso'=>'Burkina_Faso',
										'Burundi'=>'Burundi',
										'Caiman'=>'Caiman',
										'Cambodge'=>'Cambodge',
										'Cameroun'=>'Cameroun',
										'Canada'=>'Canada',
										'Canaries'=>'Canaries',
										'Cap_vert'=>'Cap_Vert',
										'Chili'=>'Chili',
										'Chine'=>'Chine',
										'Chypre'=>'Chypre',
										'Colombie'=>'Colombie',
										'Comores'=>'Colombie',
										'Congo'=>'Congo',
										'Congo_democratique'=>'Congo_democratique',
										'Cook'=>'Cook',
										'Coree_du_Nord'=>'Coree_du_Nord',
										'Coree_du_Sud'=>'Coree_du_Sud',
										'Costa_Rica'=>'Costa_Rica',
										'Cote_d_Ivoire'=>'Côte_d_Ivoire',
										'Croatie'=>'Croatie',
										'Cuba'=>'Cuba',
										'Danemark'=>'Danemark',
										'Djibouti'=>'Djibouti',
										'Dominique'=>'Dominique',
										'Egypte'=>'Egypte',
										'Emirats_Arabes_Unis'=>'Emirats_Arabes_Unis',
										'Equateur'=>'Equateur',
										'Erythree'=>'Erythree',
										'Espagne'=>'Espagne',
										'Estonie'=>'Estonie',
										'Etats_Unis'=>'Etats_Unis',
										'Ethiopie'=>'Ethiopie',
										'Falkland'=>'Falkland',
										'Feroe'=>'Feroe',
										'Fidji'=>'Fidji',
										'Finlande'=>'Finlande',
										'France'=>'France',
										'Gabon'=>'Gabon',
										'Gambie'=>'Gambie',
										'Georgie'=>'Georgie',
										'Ghana'=>'Ghana',
										'Gibraltar'=>'Gibraltar',
										'Grece'=>'Grece',
										'Grenade'=>'Grenade',
										'Groenland'=>'Groenland',
										'Guadeloupe'=>'Guadeloupe',
										'Guam'=>'Guam',
										'Guatemala'=>'Guatemala',
										'Guernesey'=>'Guernesey',
										'Guinee'=>'Guinee',
										'Guinee_Bissau'=>'Guinee_Bissau',
										'Guinee equatoriale'=>'Guinee_Equatoriale',
										'Guyana'=>'Guyana',
										'Guyane_Francaise'=>'Guyane_Francaise',
										'Haiti'=>'Haiti',
										'Hawaii'=>'Hawaii',
										'Honduras'=>'Honduras',
										'Hong_Kong'=>'Hong_Kong',
										'Hongrie'=>'Hongrie',
										'Inde'=>'Inde',
										'Indonesie'=>'Indonesie',
										'Iran'=>'Iran',
										'Iraq'=>'Iraq',
										'Irlande'=>'Irlande',
										'Islande'=>'Islande',
										'Israel'=>'Israel',
										'Italie'=>'italie',
										'Jamaique'=>'Jamaique',
										'Jan Mayen'=>'Jan Mayen',
										'Japon'=>'Japon',
										'Jersey'=>'Jersey',
										'Jordanie'=>'Jordanie',
										'Kazakhstan'=>'Kazakhstan',
										'Kenya'=>'Kenya',
										'Kirghizstan'=>'Kirghizistan',
										'Kiribati'=>'Kiribati',
										'Koweit'=>'Koweit',
										'Laos'=>'Laos',
										'Lesotho'=>'Lesotho',
										'Lettonie'=>'Lettonie',
										'Liban'=>'Liban',
										'Liberia'=>'Liberia',
										'Liechtenstein'=>'Liechtenstein',
										'Lituanie'=>'Lituanie',
										'Luxembourg'=>'Luxembourg',
										'Lybie'=>'Lybie',
										'Macao'=>'Macao',
										'Macedoine'=>'Macedoine',
										'Madagascar'=>'Madagascar',
										'Madère'=>'Madère',
										'Malaisie'=>'Malaisie',
										'Malawi'=>'Malawi',
										'Maldives'=>'Maldives',
										'Mali'=>'Mali',
										'Malte'=>'Malte',
										'Man'=>'Man',
										'Mariannes du Nord'=>'Mariannes du Nord',
										'Maroc'=>'Maroc',
										'Marshall'=>'Marshall',
										'Martinique'=>'Martinique',
										'Maurice'=>'Maurice',
										'Mauritanie'=>'Mauritanie',
										'Mayotte'=>'Mayotte',
										'Mexique'=>'Mexique',
										'Micronesie'=>'Micronesie',
										'Midway'=>'Midway',
										'Moldavie'=>'Moldavie',
										'Monaco'=>'Monaco',
										'Mongolie'=>'Mongolie',
										'Montserrat'=>'Montserrat',
										'Mozambique'=>'Mozambique',
										'Namibie'=>'Namibie',
										'Nauru'=>'Nauru',
										'Nepal'=>'Nepal',
										'Nicaragua'=>'Nicaragua',
										'Niger'=>'Niger',
										'Nigeria'=>'Nigeria',
										'Niue'=>'Niue',
										'Norfolk'=>'Norfolk',
										'Norvege'=>'Norvege',
										'Nouvelle_Caledonie'=>'Nouvelle_Caledonie',
										'Nouvelle_Zelande'=>'Nouvelle_Zelande',
										'Oman'=>'Oman',
										'Ouganda'=>'Ouganda',
										'Ouzbekistan'=>'Ouzbekistan',
										'Pakistan'=>'Pakistan',
										'Palau'=>'Palau',
										'Palestine'=>'Palestine',
										'Panama'=>'Panama',
										'Papouasie_Nouvelle_Guinee'=>'Papouasie_Nouvelle_Guinee',
										'Paraguay'=>'Paraguay',
										'Pays_Bas'=>'Pays_Bas',
										'Perou'=>'Perou',
										'Philippines'=>'Philippines',
										'Pologne'=>'Pologne',
										'Polynesie'=>'Polynesie',
										'Porto_Rico'=>'Porto_Rico',
										'Portugal'=>'Portugal',
										'Qatar'=>'Qatar',
										'Republique_Dominicaine'=>'Republique_Dominicaine',
										'Republique_Tcheque'=>'Republique_Tcheque',
										'Reunion'=>'Reunion',
										'Roumanie'=>'Roumanie',
										'Royaume_Uni'=>'Royaume_Uni',
										'Russie'=>'Russie',
										'Rwanda'=>'Rwanda',
										'Sahara Occidental'=>'Sahara Occidental',
										'Sainte_Lucie'=>'Sainte_Lucie',
										'Saint_Marin'=>'Saint_Marin',
										'Salomon'=>'Salomon',
										'Salvador'=>'Salvador',
										'Samoa_Occidentales'=>'Samoa_Occidentales',
										'Samoa_Americaine'=>'Samoa_Americaine',
										'Sao_Tome_et_Principe'=>'Sao_Tome_et_Principe',
										'Senegal'=>'Senegal',
										'Seychelles'=>'Seychelles',
										'Sierra Leone'=>'Sierra Leone',
										'Singapour'=>'Singapour',
										'Slovaquie'=>'Slovaquie',
										'Slovenie'=>'Slovenie',
										'Somalie'=>'Somalie',
										'Soudan'=>'Soudan',
										'Sri_Lanka'=>'Sri_Lanka',
										'Suede'=>'Suede',
										'Suisse'=>'Suisse',
										'Surinam'=>'Surinam',
										'Swaziland'=>'Swaziland',
										'Syrie'=>'Syrie',
										'Tadjikistan'=>'Tadjikistan',
										'Taiwan'=>'Taiwan',
										'Tonga'=>'Tonga',
										'Tanzanie'=>'Tanzanie',
										'Tchad'=>'Tchad',
										'Thailande'=>'Thailande',
										'Tibet'=>'Tibet',
										'Timor_Oriental'=>'Timor_Oriental',
										'Togo'=>'Togo',
										'Trinite_et_Tobago'=>'Trinite_et_Tobago',
										'Tristan da cunha'=>'Tristan da cunha',
										'Tunisie'=>'Tunisie',
										'Turkmenistan'=>'Turmenistan',
										'Turquie'=>'Turquie',
										'Ukraine'=>'Ukraine',
										'Uruguay'=>'Uruguay',
										'Vanuatu'=>'Vanuatu',
										'Vatican'=>'Vatican',
										'Venezuela'=>'Venezuela',
										'Vierges_Americaines'=>'Vierges_Americaines',
										'Vierges_Britanniques'=>'Vierges_Britanniques',
										'Vietnam'=>'Vietnam',
										'Wake'=>'Wake',
										'Wallis et Futuma'=>'Wallis et Futuma',
										'Yemen'=>'Yemen',
										'Yougoslavie'=>'Yougoslavie',
										'Zambie'=>'Zambie',
										'Zimbabwe'=>'Zimbabwe',
									],
								Auth::user()->bio->usr_pays, ['class' => 'form-control']) !!}
							</div>
						</div>


						<div class="form-group{{ $errors->has('tel_mobile') ? ' has-error' : '' }}">
							<label for="ville" class="col-md-4 control-label">Téléphone mobile</label>

							<div class="col-md-6">
								<input id="tel_mobile" type="tel" class="form-control" name="tel_mobile" value="{{ Auth::user()->bio->usr_tel_mob }}" placeholder="Ex.: 0605040707">

								@if ($errors->has('tel_mobile'))
									<span class="help-block">
										<strong>{{ $errors->first('tel_mobile') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('tel_fixe') ? ' has-error' : '' }}">
							<label for="ville" class="col-md-4 control-label">Téléphone fixe</label>

							<div class="col-md-6">
								<input id="tel_fixe" type="tel" class="form-control" name="tel_fixe" value="{{ Auth::user()->bio->usr_tel_fixe }}" placeholder="Ex.: 0205040707">

								@if ($errors->has('tel_fixe'))
									<span class="help-block">
										<strong>{{ $errors->first('tel_fixe') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('annee_entree') ? ' has-error' : '' }}">
							<label class="col-md-4 col-sm-4 control-label">Année d'entrée</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('annee_entree', 
									[
										'' => 'Sélectionnez la date',
										'1980' => '1980',
										'1981' => '1981',
										'1982' => '1982',
										'1983' => '1983',
										'1984' => '1984',
										'1985' => '1985',
										'1986' => '1986',
										'1987' => '1987',
										'1988' => '1988',
										'1989' => '1989',
										'1990' => '1990',
										'1991' => '1991',
										'1992' => '1992',
										'1993' => '1993',
										'1994' => '1994',
										'1995' => '1995',
										'1996' => '1996',
										'1997' => '1997',
										'1998' => '1998',
										'1999' => '1999',
										'2000' => '2000',
										'2001' => '2001',
										'2002' => '2002',
										'2003' => '2003',
										'2004' => '2004',
										'2005' => '2005',
										'2006' => '2006',
										'2007' => '2007',
										'2008' => '2008',
										'2009' => '2009',
										'2010' => '2010',
										'2011' => '2011',
										'2012' => '2012',
										'2013' => '2013',
										'2014' => '2014',
										'2015' => '2015',
										'2016' => '2016',
										'2017' => '2017',
										'2018' => '2018',
										'2019' => '2019',
										'2020' => '2020',
										'2021' => '2021',
										'2022' => '2022',
										'2023' => '2023',
										'2024' => '2024',
										'2025' => '2025',
									],
								Auth::user()->bio->usr_annee_entree, ['class' => 'form-control']) !!}
								@if ($errors->has('annee_entree'))
									<span class="help-block">
										<strong>{{ $errors->first('annee_entree') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('annee_sortie') ? ' has-error' : '' }}">
							<label class="col-md-4 col-sm-4 control-label">Année de sortie</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('annee_sortie', 
									[
										'' => 'Sélectionnez la date',
										'1980' => '1980',
										'1981' => '1981',
										'1982' => '1982',
										'1983' => '1983',
										'1984' => '1984',
										'1985' => '1985',
										'1986' => '1986',
										'1987' => '1987',
										'1988' => '1988',
										'1989' => '1989',
										'1990' => '1990',
										'1991' => '1991',
										'1992' => '1992',
										'1993' => '1993',
										'1994' => '1994',
										'1995' => '1995',
										'1996' => '1996',
										'1997' => '1997',
										'1998' => '1998',
										'1999' => '1999',
										'2000' => '2000',
										'2001' => '2001',
										'2002' => '2002',
										'2003' => '2003',
										'2004' => '2004',
										'2005' => '2005',
										'2006' => '2006',
										'2007' => '2007',
										'2008' => '2008',
										'2009' => '2009',
										'2010' => '2010',
										'2011' => '2011',
										'2012' => '2012',
										'2013' => '2013',
										'2014' => '2014',
										'2015' => '2015',
										'2016' => '2016',
										'2017' => '2017',
										'2018' => '2018',
										'2019' => '2019',
										'2020' => '2020',
										'2021' => '2021',
										'2022' => '2022',
										'2023' => '2023',
										'2024' => '2024',
										'2025' => '2025',
									],
								Auth::user()->bio->usr_annee_sortie, ['class' => 'form-control']) !!}
								@if ($errors->has('annee_sortie'))
									<span class="help-block">
										<strong>{{ $errors->first('annee_sortie') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('diplome') ? ' has-error' : '' }}">
							<label for="ville" class="col-md-4 control-label">Diplôme</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('diplome',
									[
										'' => 'Sélectionnez votre diplôme',
										'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)' => 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)',
										'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)' => 'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)',
										'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)' => 'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)',
										'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)' => 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)',
										'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne (2002-2012)' => 'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne (2002-2012) ',
										'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)' => 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013) ',
										'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)' => 'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013) ',
									],
								Auth::user()->bio->usr_diplome, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group{{ $errors->has('fb') ? ' has-error' : '' }}">
							<label for="fb" class="col-md-4 control-label">
								Facebook
								<i class="material-icons fa fa-facebook-square fa-2x" style="margin-left: 5px;"></i>
							</label>

							<div class="col-md-6">
								<input id="fb" type="fb" class="form-control" name="fb" value="{{ Auth::user()->bio->usr_fb }}" placeholder="https://www.facebook.com/pseudo">

								@if ($errors->has('fb'))
									<span class="help-block">
										<strong>{{ $errors->first('fb') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Enregistrer
								</button>
								<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
									Retour
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Donnees Pro-->
	<div class="modal fade" id="modProData" tabindex="-1" role="dialog" aria-labelledby="Modify Professional Data">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><strong>Formulaire de saisie professionnel</strong></h4>
				</div>
				<div class="modal-body">

					<form method="POST" action="{{ route('update_pro') }}" class="form-horizontal"  enctype="multipart/form-data">
						{{ csrf_field() }}

						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<div class="form-group">
							<label  class="col-sm-5 control-label" for="activite">Êtes-vous en activité ?</label>
							<div class="col-sm-6">
								<label class="radio-inline">
									<input type="radio" name="activite" value="oui" 
										@if ( Auth::user()->bio->usr_activite == "oui")
											checked
										@endif
									>Oui
								</label>
								<label class="radio-inline">
									<input type="radio" name="activite" value="Sans emploi"
									@if ( Auth::user()->bio->usr_activite == "Sans emploi")
										checked
									@endif
									/>Sans emploi
								</label>
								<label class="radio-inline">
									<input type="radio" name="activite" value="Retraite"
									@if ( Auth::user()->bio->usr_activite == "Retraite")
										checked
									@endif
									/>Retraite
								</label>
							</div>
						</div>

						<div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="profession">Intitulé de poste</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="profession"  placeholder="Intitulé de poste" value="{{ Auth::user()->bio->usr_poste }}">
							</div>
						</div>

						<div class="form-group{{ $errors->has('type_de_contrat') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="type_de_contrat">Type de contrat</label>
							<div class="col-sm-6">
								{!! Form::select('type_de_contrat',
									[
										'' => 'Sélectionnez votre type de contrat',
										'CDI' => 'CDI',
										'CDD' => 'CDD',
										'CTT ou Interim' => 'CTT ou Interim',
										'CUI' => 'CUI',
										'Apprentissage' => 'Apprentissage',
										'Contrat de professionnalisation' => 'Contrat de professionnalisation',
										'Autre' => 'Autre',
									],

								Auth::user()->bio->usr_contrat, ['class' => 'form-control']) !!}
							</div>
						</div>

						<div class="form-group{{ $errors->has('anne_embauche') ? ' has-error' : '' }}">
							<label class="col-md-5 col-sm-5 control-label">Année d'embauche</label>

							<div class="col-md-6 col-sm-6">
								{!! Form::select('anne_embauche', 
									[
										'' => 'Sélectionnez la date',
										'1980' => '1980',
										'1981' => '1981',
										'1982' => '1982',
										'1983' => '1983',
										'1984' => '1984',
										'1985' => '1985',
										'1986' => '1986',
										'1987' => '1987',
										'1988' => '1988',
										'1989' => '1989',
										'1990' => '1990',
										'1991' => '1991',
										'1992' => '1992',
										'1993' => '1993',
										'1994' => '1994',
										'1995' => '1995',
										'1996' => '1996',
										'1997' => '1997',
										'1998' => '1998',
										'1999' => '1999',
										'2000' => '2000',
										'2001' => '2001',
										'2002' => '2002',
										'2003' => '2003',
										'2004' => '2004',
										'2005' => '2005',
										'2006' => '2006',
										'2007' => '2007',
										'2008' => '2008',
										'2009' => '2009',
										'2010' => '2010',
										'2011' => '2011',
										'2012' => '2012',
										'2013' => '2013',
										'2014' => '2014',
										'2015' => '2015',
										'2016' => '2016',
										'2017' => '2017',
										'2018' => '2018',
										'2019' => '2019',
										'2020' => '2020',
										'2021' => '2021',
										'2022' => '2022',
										'2023' => '2023',
										'2024' => '2024',
										'2025' => '2025',
									],
								Auth::user()->bio->usr_annee_emb, ['class' => 'form-control']) !!}
								@if ($errors->has('anne_embauche'))
									<span class="help-block">
										<strong>{{ $errors->first('anne_embauche') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('entreprise') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="entreprise">Nom de l'entreprise</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_nom_entreprise }}"type="text" class="form-control" name="entreprise"  placeholder="Nom de l'entreprise">
							</div>
						</div>

						<div class="form-group{{ $errors->has('adressepro') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="adressepro">Adresse de l'entreprise</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_adresse_entreprise }}"type="text" class="form-control" name="adressepro"  placeholder="Adresse de l'entreprise">
							</div>
						</div>

						<div class="form-group{{ $errors->has('code_postalpro') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="code_postalpro">Code postal de l'entreprise</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_cp_entreprise }}"type="text" class="form-control" name="code_postalpro"  placeholder="Code postal de l'entreprise">
							</div>
						</div>

						<div class="form-group{{ $errors->has('ville_pro') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="ville_pro">Ville de l'entreprise</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_ville_entreprise }}"type="text" class="form-control" name="ville_pro"  placeholder="Ville de l'entreprise">
							</div>
						</div>

						<!-- debut liste deroulante pays -->
						<div class="form-group{{ $errors->has('payspro') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="payspro">Quel est le lieu de votre travail ?</label>
							<div class="col-sm-6">

								{!! Form::select('payspro',
									[
										'' =>'Sélectionnez un pays',
										'France' =>'France',

										'Afghanistan'=>'Afghanistan',
										'Afrique_Centrale'=>'Afrique_Centrale',
										'Afrique_du_sud'=>'Afrique_du_Sud',
										'Albanie'=>'Albanie',
										'Algerie'=>'Algerie',
										'Allemagne'=>'Allemagne',
										'Andorre'=>'Andorre',
										'Angola'=>'Angola',
										'Anguilla'=>'Anguilla',
										'Arabie_Saoudite'=>'Arabie_Saoudite',
										'Argentine'=>'Argentine',
										'Armenie'=>'Armenie',
										'Australie'=>'Australie',
										'Autriche'=>'Autriche',
										'Azerbaidjan'=>'Azerbaidjan',
										'Bahamas'=>'Bahamas',
										'Bangladesh'=>'Bangladesh',
										'Barbade'=>'Barbade',
										'Bahrein'=>'Bahrein',
										'Belgique'=>'Belgique',
										'Belize'=>'Belize',
										'Benin'=>'Benin',
										'Bermudes'=>'Bermudes',
										'Bielorussie'=>'Bielorussie',
										'Bolivie'=>'Bolivie',
										'Botswana'=>'Botswana',
										'Bhoutan'=>'Bhoutan',
										'Boznie_Herzegovine'=>'Boznie_Herzegovine',
										'Bresil'=>'Bresil',
										'Brunei'=>'Brunei',
										'Bulgarie'=>'Bulgarie',
										'Burkina_Faso'=>'Burkina_Faso',
										'Burundi'=>'Burundi',
										'Caiman'=>'Caiman',
										'Cambodge'=>'Cambodge',
										'Cameroun'=>'Cameroun',
										'Canada'=>'Canada',
										'Canaries'=>'Canaries',
										'Cap_vert'=>'Cap_Vert',
										'Chili'=>'Chili',
										'Chine'=>'Chine',
										'Chypre'=>'Chypre',
										'Colombie'=>'Colombie',
										'Comores'=>'Colombie',
										'Congo'=>'Congo',
										'Congo_democratique'=>'Congo_democratique',
										'Cook'=>'Cook',
										'Coree_du_Nord'=>'Coree_du_Nord',
										'Coree_du_Sud'=>'Coree_du_Sud',
										'Costa_Rica'=>'Costa_Rica',
										'Cote_d_Ivoire'=>'Côte_d_Ivoire',
										'Croatie'=>'Croatie',
										'Cuba'=>'Cuba',
										'Danemark'=>'Danemark',
										'Djibouti'=>'Djibouti',
										'Dominique'=>'Dominique',
										'Egypte'=>'Egypte',
										'Emirats_Arabes_Unis'=>'Emirats_Arabes_Unis',
										'Equateur'=>'Equateur',
										'Erythree'=>'Erythree',
										'Espagne'=>'Espagne',
										'Estonie'=>'Estonie',
										'Etats_Unis'=>'Etats_Unis',
										'Ethiopie'=>'Ethiopie',
										'Falkland'=>'Falkland',
										'Feroe'=>'Feroe',
										'Fidji'=>'Fidji',
										'Finlande'=>'Finlande',
										'France'=>'France',
										'Gabon'=>'Gabon',
										'Gambie'=>'Gambie',
										'Georgie'=>'Georgie',
										'Ghana'=>'Ghana',
										'Gibraltar'=>'Gibraltar',
										'Grece'=>'Grece',
										'Grenade'=>'Grenade',
										'Groenland'=>'Groenland',
										'Guadeloupe'=>'Guadeloupe',
										'Guam'=>'Guam',
										'Guatemala'=>'Guatemala',
										'Guernesey'=>'Guernesey',
										'Guinee'=>'Guinee',
										'Guinee_Bissau'=>'Guinee_Bissau',
										'Guinee equatoriale'=>'Guinee_Equatoriale',
										'Guyana'=>'Guyana',
										'Guyane_Francaise'=>'Guyane_Francaise',
										'Haiti'=>'Haiti',
										'Hawaii'=>'Hawaii',
										'Honduras'=>'Honduras',
										'Hong_Kong'=>'Hong_Kong',
										'Hongrie'=>'Hongrie',
										'Inde'=>'Inde',
										'Indonesie'=>'Indonesie',
										'Iran'=>'Iran',
										'Iraq'=>'Iraq',
										'Irlande'=>'Irlande',
										'Islande'=>'Islande',
										'Israel'=>'Israel',
										'Italie'=>'italie',
										'Jamaique'=>'Jamaique',
										'Jan Mayen'=>'Jan Mayen',
										'Japon'=>'Japon',
										'Jersey'=>'Jersey',
										'Jordanie'=>'Jordanie',
										'Kazakhstan'=>'Kazakhstan',
										'Kenya'=>'Kenya',
										'Kirghizstan'=>'Kirghizistan',
										'Kiribati'=>'Kiribati',
										'Koweit'=>'Koweit',
										'Laos'=>'Laos',
										'Lesotho'=>'Lesotho',
										'Lettonie'=>'Lettonie',
										'Liban'=>'Liban',
										'Liberia'=>'Liberia',
										'Liechtenstein'=>'Liechtenstein',
										'Lituanie'=>'Lituanie',
										'Luxembourg'=>'Luxembourg',
										'Lybie'=>'Lybie',
										'Macao'=>'Macao',
										'Macedoine'=>'Macedoine',
										'Madagascar'=>'Madagascar',
										'Madère'=>'Madère',
										'Malaisie'=>'Malaisie',
										'Malawi'=>'Malawi',
										'Maldives'=>'Maldives',
										'Mali'=>'Mali',
										'Malte'=>'Malte',
										'Man'=>'Man',
										'Mariannes du Nord'=>'Mariannes du Nord',
										'Maroc'=>'Maroc',
										'Marshall'=>'Marshall',
										'Martinique'=>'Martinique',
										'Maurice'=>'Maurice',
										'Mauritanie'=>'Mauritanie',
										'Mayotte'=>'Mayotte',
										'Mexique'=>'Mexique',
										'Micronesie'=>'Micronesie',
										'Midway'=>'Midway',
										'Moldavie'=>'Moldavie',
										'Monaco'=>'Monaco',
										'Mongolie'=>'Mongolie',
										'Montserrat'=>'Montserrat',
										'Mozambique'=>'Mozambique',
										'Namibie'=>'Namibie',
										'Nauru'=>'Nauru',
										'Nepal'=>'Nepal',
										'Nicaragua'=>'Nicaragua',
										'Niger'=>'Niger',
										'Nigeria'=>'Nigeria',
										'Niue'=>'Niue',
										'Norfolk'=>'Norfolk',
										'Norvege'=>'Norvege',
										'Nouvelle_Caledonie'=>'Nouvelle_Caledonie',
										'Nouvelle_Zelande'=>'Nouvelle_Zelande',
										'Oman'=>'Oman',
										'Ouganda'=>'Ouganda',
										'Ouzbekistan'=>'Ouzbekistan',
										'Pakistan'=>'Pakistan',
										'Palau'=>'Palau',
										'Palestine'=>'Palestine',
										'Panama'=>'Panama',
										'Papouasie_Nouvelle_Guinee'=>'Papouasie_Nouvelle_Guinee',
										'Paraguay'=>'Paraguay',
										'Pays_Bas'=>'Pays_Bas',
										'Perou'=>'Perou',
										'Philippines'=>'Philippines',
										'Pologne'=>'Pologne',
										'Polynesie'=>'Polynesie',
										'Porto_Rico'=>'Porto_Rico',
										'Portugal'=>'Portugal',
										'Qatar'=>'Qatar',
										'Republique_Dominicaine'=>'Republique_Dominicaine',
										'Republique_Tcheque'=>'Republique_Tcheque',
										'Reunion'=>'Reunion',
										'Roumanie'=>'Roumanie',
										'Royaume_Uni'=>'Royaume_Uni',
										'Russie'=>'Russie',
										'Rwanda'=>'Rwanda',
										'Sahara Occidental'=>'Sahara Occidental',
										'Sainte_Lucie'=>'Sainte_Lucie',
										'Saint_Marin'=>'Saint_Marin',
										'Salomon'=>'Salomon',
										'Salvador'=>'Salvador',
										'Samoa_Occidentales'=>'Samoa_Occidentales',
										'Samoa_Americaine'=>'Samoa_Americaine',
										'Sao_Tome_et_Principe'=>'Sao_Tome_et_Principe',
										'Senegal'=>'Senegal',
										'Seychelles'=>'Seychelles',
										'Sierra Leone'=>'Sierra Leone',
										'Singapour'=>'Singapour',
										'Slovaquie'=>'Slovaquie',
										'Slovenie'=>'Slovenie',
										'Somalie'=>'Somalie',
										'Soudan'=>'Soudan',
										'Sri_Lanka'=>'Sri_Lanka',
										'Suede'=>'Suede',
										'Suisse'=>'Suisse',
										'Surinam'=>'Surinam',
										'Swaziland'=>'Swaziland',
										'Syrie'=>'Syrie',
										'Tadjikistan'=>'Tadjikistan',
										'Taiwan'=>'Taiwan',
										'Tonga'=>'Tonga',
										'Tanzanie'=>'Tanzanie',
										'Tchad'=>'Tchad',
										'Thailande'=>'Thailande',
										'Tibet'=>'Tibet',
										'Timor_Oriental'=>'Timor_Oriental',
										'Togo'=>'Togo',
										'Trinite_et_Tobago'=>'Trinite_et_Tobago',
										'Tristan da cunha'=>'Tristan da cunha',
										'Tunisie'=>'Tunisie',
										'Turkmenistan'=>'Turmenistan',
										'Turquie'=>'Turquie',
										'Ukraine'=>'Ukraine',
										'Uruguay'=>'Uruguay',
										'Vanuatu'=>'Vanuatu',
										'Vatican'=>'Vatican',
										'Venezuela'=>'Venezuela',
										'Vierges_Americaines'=>'Vierges_Americaines',
										'Vierges_Britanniques'=>'Vierges_Britanniques',
										'Vietnam'=>'Vietnam',
										'Wake'=>'Wake',
										'Wallis et Futuma'=>'Wallis et Futuma',
										'Yemen'=>'Yemen',
										'Yougoslavie'=>'Yougoslavie',
										'Zambie'=>'Zambie',
										'Zimbabwe'=>'Zimbabwe',
									],
								Auth::user()->bio->usr_pays_travail, ['class' => 'form-control']) !!}
							</div>
						</div>
						<!-- fin de la liste des pays -->

						<div class="form-group{{ $errors->has('telephone_fixe_pro') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="telephone_fixe_pro">Numéro de téléphone professionnel</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_tel_pro }}" type="tel" class="form-control" name="telephone_fixe_pro"  placeholder="Ex : 0203040607">
								@if ($errors->has('telephone_fixe_pro'))
									<span class="help-block">
										<strong>{{ $errors->first('telephone_fixe_pro') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('courriel_professionnel') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="courriel_professionnel">E-mail professionnel</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_email_pro }}" type="email" class="form-control" name="courriel_professionnel" placeholder="E-mail professionnel">
							</div>
						</div>

						<div class="form-group{{ $errors->has('secteur') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="secteur">Secteur d'activité</label>
							<div class="col-sm-6">
								{!! Form::select('secteur', 
									[
										'' => 'Sélectionnez votre secteur',
										'Industrie de la viande ' => 'Industrie de la viande ',
										'Industrie laitière' => 'Industrie laitière',
										'Industrie sucrière' => 'Industrie sucrière',
										'Industrie des conserves, produits surgelés, fruits et légumes' => 'Industrie des conserves, produits surgelés, fruits et légumes',
										'Industrie des produits à base de céréales ' => 'Industrie des produits à base de céréales ',
										'Industrie des produits alimentaires divers (confiseries, condiments...) ' => 'Industrie des produits alimentaires divers (confiseries, condiments...) ',
										'Industrie des boissons, alcools et agro-carburants ' => 'Industrie des boissons, alcools et agro-carburants ',
										'Cosmétique' => 'Cosmétique',
										'Restauration et hôtellerie' => 'Restauration et hôtellerie',
										'Etablissements de santé' => 'Etablissements de santé',
										'Formation et consulting' => 'Formation et consulting',
										'Enseignement' => 'Enseignement',
										'Recherche' => 'Recherche',
										'Autres secteurs ' => 'Autres secteurs ',
									],
								Auth::user()->bio->usr_secteur, ['class' => 'form-control']) !!}

							</div>
						</div>

						<div class="form-group{{ $errors->has('cv') ? ' has-error' : '' }}">
							<label class="col-sm-5 control-label" for="cv">Votre CV</label>

							<div class="col-sm-6" data-placement="top" title="Formats possibles: pdf, doc, docx, rtf, odt" data-tooltip="true">

								<input type="file" name="cv" id="upload_hidden_cv" class="upl_hidden" onchange="document.getElementById('helpBlock_cv').innerHTML=this.value.replace(/.*[\/\\]/, ''); $('#helpBlock_cv').removeClass('hidden')">

								<button class="btn btn-success" onclick="event.preventDefault(); document.getElementById('upload_hidden_cv').click();">Chercher sur le disque</button>
								<span id="helpBlock_cv" class="help-block hidden" style="color: #434343; margin-bottom: 0;"></span>

								@if ($errors->has('cv'))
									<span class="help-block">
										<strong>{{ $errors->first('cv') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('adresse_site_web') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="adresse_site_web">Site web de l’entreprise</label>
							<div class="col-sm-6">
								<input value="{{ Auth::user()->bio->usr_site_web }}" type="text" class="form-control" name="adresse_site_web"  placeholder="www">
							</div>
						</div>

						<div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
							<label for="linkedin" class="col-md-5 control-label">
								LinkedIn
								<i class="material-icons fa fa-linkedin-square fa-2x" style="margin-left: 5px;"></i>
							</label>

							<div class="col-md-6">
								<input id="linkedin" type="linkedin" class="form-control" name="linkedin" value="{{ Auth::user()->bio->usr_linkedin }}" placeholder="LinkedIn">

								@if ($errors->has('linkedin'))
									<span class="help-block">
										<strong>{{ $errors->first('linkedin') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('viadeo') ? ' has-error' : '' }}">
							<label for="viadeo" class="col-md-5 control-label">
								Viadeo
								<i class="material-icons fa fa-viadeo-square fa-2x" style="margin-left: 5px;"></i>
							</label>

							<div class="col-md-6">
								<input id="viadeo" type="viadeo" class="form-control" name="viadeo" value="{{ Auth::user()->bio->usr_viadeo }}" placeholder="Viadeo">

								@if ($errors->has('viadeo'))
									<span class="help-block">
										<strong>{{ $errors->first('viadeo') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('ok') ? ' has-error' : '' }}">
							<label  class="col-sm-5 control-label" for="ok">Acceptez-vous l'utilisation de ces données dans le cadre de  l'édition de l'annuaire et sur le site web ?</label>
							<div class="col-sm-6 col-sm-offset-1 col-xs-offset-1 col-xs-11">
								<label class="radio">
									@if ( Auth::user()->bio->usr_donnees == "oui")
										{!! Form::radio('ok', 'oui', true) !!} Oui
									@else
										{!! Form::radio('ok', 'oui') !!} Oui
									@endif
								</label>
								<label class="radio">
									@if ( Auth::user()->bio->usr_donnees == "pro")
										{!! Form::radio('ok', 'pro', true) !!} Données professionnelles uniquement
									@else
										{!! Form::radio('ok', 'pro') !!} Données professionnelles uniquement
									@endif
								</label>
								<label class="radio">
									@if ( Auth::user()->bio->usr_donnees == "non")
										{!! Form::radio('ok', 'non', true) !!} Non
									@else
										{!! Form::radio('ok', 'non') !!} Non
									@endif
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-5">
								<button type="submit" class="btn btn-primary">
									Enregistrer
								</button>
								<button class="btn btn-danger pull-right" data-dismiss="modal" aria-label="Close">
									Retour
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection