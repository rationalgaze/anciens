@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">{{ $usr->usr_prenom.' '.$usr->usr_nom}}</div>

				<div class="panel-body">
					<div class="panel-body">
						@if($usr->usr_donnees == 'oui')
							<section class="personal">
								<div class="row">
									<div class="col-xs-12 col-sm-5">
										<div class="avatar text-center">
											@if($usr->usr_img)
												<div class="halo">
													<img src="{{ asset('storage/avatars/'.$usr->usr_img) }}" class="img-circle">
												</div>
											@else
												<div class="halo">
														<img src="http://dummyimage.com/240/434343.jpg/e7e7e7&text= Avatar" class="img-circle">
												</div>
											@endif
										</div>
									</div>
									<div class="col-xs-12 col-sm-7 info">
										@if($usr->usr_prenom || $usr->usr_nom)
											<h2 class="ancien_title text-capitalize" style="color: #e7e7e7">
												<strong>
													{{ strtolower($usr->usr_prenom).' '. strtolower($usr->usr_nom) }}
													@if($usr->usr_nom_naiss)
														{{ ' ('. strtolower($usr->usr_nom_naiss).')' }}
													@endif
												</strong>
											</h2>
										@else
											<h2 class="ancien_title text-capitalize" style="color: #e7e7e7">
												<strong>
													Données personnels
												</strong>
											</h2>
										@endif

										@if($usr->usr_adresse_perso || $usr->usr_cp || $usr->usr_ville || $usr->usr_pays)
											<p class="adress text-capitalize">
												<i class="material-icons"><small>location_on</small></i>
												<small>
													{{ 
														($usr->usr_cp)?$usr->usr_adresse_perso.', '.$usr->usr_cp.' '.strtolower($usr->usr_ville).', '.$usr->usr_pays :
														$usr->usr_adresse_perso.' '.$usr->usr_cp.' '.$usr->usr_ville.' '.$usr->usr_pays
													}}
												</small>
											</p>
										@endif

										@if($usr->usr_date_naiss)
											<p><i class="material-icons">cake</i>{{ 
												($usr->usr_lieu_naiss)?Carbon\Carbon::parse($usr->usr_date_naiss)->format('d F Y').', '.$usr->usr_lieu_naiss :Carbon\Carbon::parse($usr->usr_date_naiss)->format('d F Y') }}</p>
										@endif
										
										@if($usr->usr_email)
											<p class="mail">
												<i class="material-icons">mail_outline</i>
												<a href="mailto:{{ $usr->usr_email }}">
													{{ $usr->usr_email }}
												</a>
											</p>
										@endif

										@if(($usr->usr_annee_entree || $usr->usr_annee_sortie || $usr->usr_diplome) && ($usr->usr_annee_entree != '?' || $usr->usr_annee_sortie != '?' || $usr->usr_diplome != ''))
											<p>
												<i class="material-icons">school</i>
												@if($usr->usr_annee_entree && $usr->usr_annee_sortie && $usr->usr_diplome)
													{{ $usr->usr_annee_entree.' - '.$usr->usr_annee_sortie.' - '.$usr->usr_diplome }}
												@elseif(!$usr->usr_diplome || $usr->usr_diplome == '')
													{{ $usr->usr_annee_entree.' - '.$usr->usr_annee_sortie }}
												@else
													{{ $usr->usr_annee_entree.' '.$usr->usr_annee_sortie.' '.$usr->usr_diplome }}
												@endif
											</p>
										@endif

										@if($usr->usr_tel_mob||$usr->usr_tel_fixe)
											<p>
												<i class="material-icons">stay_current_portrait</i>
												@if($usr->usr_tel_fixe) 
													{{ chunk_split($usr->usr_tel_mob, 2, " ").' ' }}<i class="material-icons" style="margin: 0 5px;">phone</i>{{ ' '.chunk_split($usr->usr_tel_fixe, 2, " ") }}
												@else
													{{ chunk_split($usr->usr_tel_mob,2, " ") }}
												@endif
											</p>
										@endif

										@if($usr->usr_fb)
											<p class="socials mail">
												<i class="material-icons fa fa-facebook-square"></i>
												<a href="{{ $usr->usr_fb }}" target="_blank">
													{{ preg_replace('/(https:\/\/www.facebook.com\/)/i', '', $usr->usr_fb) }}
												</a>
											</p>
										@endif
									</div>
								</div>
							</section>

							<section class="pro">
								<div class="row">
									<h1>Données professionnelles</h1>
									<hr>

									@if($usr->usr_activite == 'Sans emploi')
										<p>Sans emploi</p>
										@if($usr->usr_cv)
											<p>
												<i class="material-icons">content_paste</i>
												<a href="{{ asset('storage/cv_storage/'.$usr->usr_cv) }}" target="_blanc" alt="{{ $usr->usr_prenom.' '.$usr->usr_nom }}">voir CV</a>
											</p>
										@endif

										@if($usr->usr_linkedin)
											<p class="socials mail">
												<i class="material-icons fa fa-linkedin-square"></i>
												<a href="{{ $usr->usr_linkedin }}" target="_blank">
													LinkedIn
												</a>
											</p>
										@endif

										@if($usr->usr_viadeo)
											<p class="socials mail">
												<i class="material-icons fa fa-viadeo-square"></i>
												<a href="{{ $usr->usr_viadeo }}" target="_blank">
													Viadeo
												</a>
											</p>
										@endif

										@if($usr->usr_donnees == 'oui')
											<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
										@elseif($usr->usr_donnees == 'pro')
											<div>J’accepte l’utilisation de mes données professionnelles</div>
										@else
											<div>J'accepte pas l'utilisation de mes données</div>
										@endif
									@elseif($usr->usr_activite == 'Retraite')
										<p>En retraite</p>
										@if($usr->usr_cv)
											<p>
												<i class="material-icons">content_paste</i>
												<a href="{{ asset('storage/cv_storage/'.$usr->usr_cv) }}" target="_blanc" alt="{{ $usr->usr_prenom.' '.$usr->usr_nom }}">voir CV</a>
											</p>
										@endif

										@if($usr->usr_linkedin)
											<p class="socials mail">
												<i class="material-icons fa fa-linkedin-square"></i>
												<a href="{{ $usr->usr_linkedin }}" target="_blank">
													LinkedIn
												</a>
											</p>
										@endif

										@if($usr->usr_viadeo)
											<p class="socials mail">
												<i class="material-icons fa fa-viadeo-square"></i>
												<a href="{{ $usr->usr_viadeo }}" target="_blank">
													Viadeo
												</a>
											</p>
										@endif

										@if($usr->usr_donnees == 'oui')
											<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
										@elseif($usr->usr_donnees == 'pro')
											<div>J’accepte l’utilisation de mes données professionnelles</div>
										@else
											<div>J'accepte pas l'utilisation de mes données</div>
										@endif
									@else
										@if($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_secteur && $usr->usr_contrat)
											<p>
												<i class="material-icons">work</i>
												{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise.' ( '.$usr->usr_secteur.' ) - '.$usr->usr_contrat }}
											</p>
										@elseif($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_secteur)
											<p>
												<i class="material-icons">work</i>
												{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise.' ( '.$usr->usr_secteur.' )' }}
											</p>
										@elseif($usr->usr_poste && $usr->usr_nom_entreprise)
											<p>
												<i class="material-icons">work</i>
												{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise }}
											</p>
										@elseif($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_contrat)
											<p>
												<i class="material-icons">work</i>
												{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise .' - '.$usr->usr_contrat }}
											</p>
										@elseif($usr->usr_poste||$usr->usr_nom_entreprise ||$usr->usr_secteur||$usr->usr_contrat)
											<p>
												<i class="material-icons">work</i>
												{{ $usr->usr_poste.' '.$usr->usr_nom_entreprise .' '.$usr->usr_secteur.' '.$usr->usr_contrat }}
											</p>
										@endif

										@if($usr->usr_adresse_entreprise || $usr->usr_cp_entreprise || $usr->usr_ville_entreprise || $usr->usr_pays_travail)
											<p class="adress-pro">
												<i class="material-icons">location_on</i>
													{{ 
														($usr->usr_cp_entreprise && $usr->usr_pays_travail)
															?
																$usr->usr_adresse_entreprise.', '.$usr->usr_cp_entreprise.' '.$usr->usr_ville_entreprise.', '.$usr->usr_pays_travail
															:
																$usr->usr_adresse_entreprise.' '.$usr->usr_cp_entreprise.' '.$usr->usr_ville_entreprise.' '.$usr->usr_pays_travail
													}}
											</p>
										@endif

										@if($usr->usr_email_pro)
											<p class="mail">
												<a href="mailto:{{ $usr->usr_email_pro }}"><i class="material-icons">mail_outline</i>{{ $usr->usr_email_pro }}</a>
											</p>
										@endif

										@if($usr->usr_tel_pro)
											<p>
												<i class="material-icons">phone</i>
												{{ chunk_split($usr->usr_tel_pro, 2, " ") }}
											</p>
										@endif

										@if($usr->usr_site_web)
											<p>
												<i class="material-icons">public</i>
												<a href="http://www.{{ $usr->usr_site_web }}/" target="_blank">{{ $usr->usr_site_web }}</a>
											</p>
										@endif

										@if($usr->usr_cv)
											<p>
												<i class="material-icons">content_paste</i>
												<a href="{{ asset('storage/cv_storage/'.$usr->usr_cv) }}" target="_blanc" alt="{{ $usr->usr_prenom.' '.$usr->usr_nom }}">voir CV</a>
											</p>
										@endif

										@if($usr->usr_linkedin)
											<p class="socials mail">
												<i class="material-icons fa fa-linkedin-square"></i>
												<a href="{{ $usr->usr_linkedin }}" target="_blank">
													LinkedIn
												</a>
											</p>
										@endif

										@if($usr->usr_viadeo)
											<p class="socials mail">
												<i class="material-icons fa fa-viadeo-square"></i>
												<a href="{{ $usr->usr_viadeo }}" target="_blank">
													Viadeo
												</a>
											</p>
										@endif

										@if($usr->usr_donnees == 'oui')
											<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
										@elseif($usr->usr_donnees == 'pro')
											<div>J’accepte l’utilisation de mes données professionnelles</div>
										@else
											<div>J'accepte pas l'utilisation de mes données</div>
										@endif
									@endif
								</div>
							</section>
						@endif

						@if($usr->usr_donnees == 'pro')
							<div class="row">
								<div class="col-md-12">
									<table class="table table-striped task-table">
										<thead>
											<th colspan="2">Données professionneles</th>
										</thead>
										<tbody>

											<tr>
												<td class="table-text" width="300">
													<div>	En activité :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_activite }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Intitulé de poste :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_poste }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Type de contrat :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_contrat }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Année d'embauche :</div>
												</td>
												<td class="table-text date">
													<div>{{ $usr->usr_annee_emb }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Nom de l'entreprise :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_nom_entreprise }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Adresse de l'entreprise :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_adresse_entreprise }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Code postal de l'entreprise :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_cp_entreprise }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Ville de l'entreprise :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_ville_entreprise }}</div>
												</td>
											</tr>
											
											<tr>
												<td class="table-text">
													<div>Lieu de travail :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_pays_travail }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Téléphone professionnel :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_tel_pro }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>E-mail professionnel :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_email_pro }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Secteur d'activité :</div>
												</td>
												<td class="table-text">
													<div>{{ $usr->usr_secteur }}</div>
												</td>
											</tr>

											<tr>
												<td class="table-text">
													<div>Adresse site web :</div>
												</td>
												<td class="table-text">
													<div><a href="http://{{ $usr->usr_site_web }}" target="_blank">{{ $usr->usr_site_web }}</a></div>
												</td>
											</tr>
											
											@if($usr->usr_cv)
											<tr>
												<td class="table-text">
													<div>CV :</div>
												</td>
												<td class="table-text">
													<div>
														<a href="{{ asset('storage/cv_storage/'.$usr->usr_cv) }}" target="_blanc" alt="{{ $usr->usr_prenom.' '.$usr->usr_nom }}">voir CV</a>
													</div>
												</td>
											</tr>
											@endif

											@if($usr->usr_linkedin)
											<tr>
												<td>LinkedIn <i class="material-icons fa fa-linkedin-square"></i></td>
												<td>
													<a href="{{ $usr->usr_linkedin }}" target="_blank">
														LinkedIn
													</a>
												</td>
											</tr>
											@endif

											@if($usr->usr_viadeo)
											<tr>
												<td>
													Viadeo <i class="material-icons fa fa-viadeo-square"></i>
												</td>
												<td>
													<a href="{{ $usr->usr_viadeo }}" target="_blank">
														Viadeo
													</a>
												</p>
												</td>
											</tr>
											@endif

											<tr>
												<td class="table-text">
													<div>Acceptez-vous l'utilsation de ces données dans le cadre de la publication de l'annuaire des anciens de l'ESIAB et sur le site des Anciens ? :</div>
												</td>
												<td class="table-text" colspan="2">
													@if($usr->usr_donnees == 'oui')
														<div>J'accepte l'utilisation de mes données professionnelles et personnelles</div>
													@elseif($usr->usr_donnees == 'pro')
														<div>J’accepte l’utilisation de mes données professionnelles</div>
													@else
														<div>J'accepte pas l'utilisation de mes données</div>
													@endif
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						@endif

						<div class="row">
							<div class="col-sm-12">
								<a href="javascript:history.back()" class="btn btn-danger pull-right">
									Retour
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection