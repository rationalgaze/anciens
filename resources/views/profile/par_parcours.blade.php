@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li><a href="{{ route('profile') }}" role="tab" aria-controls="profile">Mon profil</a></li>
						<li class="active"><a href="#" role="tab" aria-controls="etudiants" data-toggle="tab">Liste des diplômés</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 main-section">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="students">
					<div class="panel panel-default">
						<div class="panel-heading">Liste des diplômé(e)s</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="{{ route('profile') }}" class="btn btn-default btn-xs">A - Z</a>
										<a href="{{ route('profile.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
										<a href="{{ route('profile.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
										<a href="#" class="btn btn-default btn-xs active disabled">Tri par parcours</a>
									</div>
								</div>
								<hr>
							</div>

							<div id="parparcours">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel viande">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
														Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l'Université de Brest" (1975-1992)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@php $i=1; @endphp
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbio">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
														Ingénieur Microbiologiste, diplômé(e) de l'Université de Brest (1985-1993)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbiologie-et-securite">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
														Ingénieur diplômé(e) de l'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbiologie-et-qualite">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
														Ingénieur diplômé(e) de l'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel agroalimentaire">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
														Ingénieur diplômé(e) de l'Université de Brest, spécialité "agroalimentaire" en partenariat avec l'IFRIA Bretagne (2002-2012)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne (2002-2012)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel procedes-indutriels">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
														Ingénieur diplômé(e) de l'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l'IFRIA Bretagne (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel innovation">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
														Master professionnel diplômé(e) de l'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20px">&#8470;</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>CP de l'entreprise</th>
																	<th>Pays (pro.)</th>
																</tr>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/profile/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																	<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_cp_entreprise }}</div>
																			</td>
																						<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-12 text-center">
												<div class="btn-group btn-group-sm nav"> 
													<a href="{{ route('profile') }}" class="btn btn-default btn-xs">A - Z</a>
													<a href="{{ route('profile.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
													<a href="{{ route('profile.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
													<a href="#" class="btn btn-default btn-xs active disabled">Tri par parcours</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
