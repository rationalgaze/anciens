@extends('layouts.app')

@section('content')
@if(Auth::check())
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li class="active"><a href="#students" role="tab" aria-controls="students" data-toggle="tab">Diplômés</a></li>
						<li><a href="#admins" role="tab" aria-controls="admins" data-toggle="tab">Administrateurs</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 main-section">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="students">
					<div class="panel panel-default">
						<div class="panel-heading"><h4><strong>Liste des diplômés de l'ESIAB</strong></h4></div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="{{ route('admin.dashboard') }}" class="btn btn-default btn-xs">A - Z</a>
										<a href="{{ route('admin.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
										<a href="#" class="btn btn-default btn-xs active disabled">Tri par secteur</a>
										<a href="{{ route('admin.par_parcours') }}" class="btn btn-default btn-xs">Tri par parcours</a>
									</div>
								</div>
								<hr>
							</div>

							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-9 col-xs-4">
											<a href="mailto:@foreach($bio_par_secteur as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="bottom" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>
											<button data-clipboard-text="@foreach($bio_par_secteur as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
										</div>
										<div class="col-md-3 col-xs-8">
											<button class="btn btn-success dropdown-toggle btn-ctrls" data-toggle="dropdown" data-placement="top" title="Exporter des données" data-tooltip="true">
												Export <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="{{ route('admin.to_word') }}">Word</a></li>
												<li><a href="{{ route('admin.to_excel') }}">Excel</a></li>
											</ul>

											<button class="btn btn-success btn-ctrls" type="button" data-placement="top" title="Importer des données" data-tooltip="true" data-toggle="modal" data-target="#modalImport" >Import</button>

											<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="top" title="Ajouter un diplômé" data-tooltip="true">
												<i class="material-icons">person_add</i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="parsecteur">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel viande">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="viande">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
														Industrie de la viande
													</a>
												</h4>
											</div>
										</div>

										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@php $i=1; @endphp
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie de la viande')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel lait">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="lait">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
														Industrie laitière
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie laitière')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel sucre">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
														Industrie sucrière
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie sucrière')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel fruits">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="fruits">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
														Industrie des conserves, produits surgelés, fruits et légumes
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie des conserves, produits surgelés, fruits et légumes')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel cereales">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="cereales">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
														Industrie des produits à base de céréales
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie des produits à base de céréales')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel divers">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="divers">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
														Industrie des produits alimentaires divers (confiseries, condiments...)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie des produits alimentaires divers (confiseries, condiments...)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel boissons">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="boissons">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
														Industrie des boissons, alcools et agro-carburants
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Industrie des boissons, alcools et agro-carburants')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel cosmetique">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="cosmetique">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
														Cosmétique
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Cosmétique')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel restauration">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="restauration">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
														Restauration et hôtellerie
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Restauration et hôtellerie')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel sante">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="sante">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
														Etablissements de santé
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Etablissements de santé')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel consulting">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="consulting">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
														Formation et consulting
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Formation et consulting')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel enseignement">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="enseignement">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
														Enseignement
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Enseignement')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel recherche">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="recherche">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
														Recherche
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Recherche')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel autres">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="autres">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
														Autres secteurs
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_secteur as $bio)
																	@if ($bio_par_secteur && $bio->usr_secteur == 'Autres secteurs')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-6">
												<a href="mailto:@foreach($bio_par_secteur as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="bottom" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>
												<button data-clipboard-text="@foreach($bio_par_secteur as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
											</div>

											<div class="col-xs-6">
												<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="bottom" title="Ajouter un diplômé" data-tooltip="true">
													<i class="material-icons">person_add</i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal Add_student -->
				@include('layouts.modals.add_ancien')

				<!-- Admins data -->
				@include('layouts.adm_data')

				<!-- Modal Add_Admin -->
				@include('layouts.modals.add_admin')

				<!-- Modal Del_Ancien -->
				@include('layouts.modals.del_ancien')

				<!-- Modal Del_Admin -->
				@include('layouts.modals.del_admin')

				<!-- Modal Import -->
				@include('layouts.modals.import')

			</div>
		</div>
	</div>
</div>
@endif
@endsection
