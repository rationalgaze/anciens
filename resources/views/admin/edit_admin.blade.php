@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">

		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="col-md-12">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">
					<div class="panel panel-default">
						<div class="panel-heading">Modifiez vos données</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<form role="form" method="POST" action="{{ route('admin.update_admin', $admin) }}">
								{{ csrf_field() }}

								<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
									<label for="name">Nom</label>

									<input id="name" type="text" class="form-control" name="name" value="{{ $admin->name }}" autofocus>

									@if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
								</div>

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<label for="email">E-mail</label>

									<input id="email" type="text" class="form-control" name="email" value="{{ $admin->email }}">

									@if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>
								
								<div class="form-group{{ $errors->has('mdp_act') ? ' has-error' : '' }}">
									<label for="mdp_act" class="control-label">Entrez votre mot de passe actuel</label>

									<input id="mdp_act" type="password" class="form-control" name="mdp_act" required>

									@if ($errors->has('mdp_act'))
										<span class="help-block">
											<strong>{{ $errors->first('mdp_act') }}</strong>
										</span>
									@endif
								</div>

								<div class="form-group{{ $errors->has('mdp_new') ? ' has-error' : '' }}">
									<label for="mdp_new	" class="control-label">Entrez votre nouveau mot de passe</label>

										<input id="mdp_new" type="password" class="form-control" name="mdp_new" required>

										@if ($errors->has('mdp_new'))
											<span class="help-block">
												<strong>{{ $errors->first('mdp_new') }}</strong>
											</span>
										@endif
								</div>

								<div class="form-group">
									<label for="mdp_new-confirm" class="control-label">Confirmez le mot de passe</label>

										<input id="mdp_new-confirm" type="password" class="form-control" name="mdp_new_confirmation" required>
								</div>

								<div class="form-group">
									<label for=""></label>
									<button type="submit" class="btn btn-primary btn-block">
										Enregistrer
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<a href="{{ url()->previous() }}" class="btn btn-default btn-md pull-right btn-float">
			<i class="material-icons">keyboard_backspace</i>
		</a>
	</div>
</div>
@endsection