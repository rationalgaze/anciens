@extends('layouts.app')

@section('content')
@if(Auth::check())

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li class="active"><a href="#students" role="tab" aria-controls="students" data-toggle="tab">Diplômés</a></li>
						<li><a href="#admins" role="tab" aria-controls="admins" data-toggle="tab">Administrateurs</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 main-section">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="students">
					<div class="panel panel-default">
						<div class="panel-heading"><h4><strong>Liste des diplômés de l'ESIAB</strong></h4></div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="{{ route('admin.dashboard') }}" class="btn btn-default btn-xs">A - Z</a>
										<a href="#" class="btn btn-default btn-xs active disabled">Tri par région</a>
										<a href="{{ route('admin.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
										<a href="{{ route('admin.par_parcours') }}" class="btn btn-default btn-xs">Tri par parcours</a>
									</div>
								</div>
								<hr>
							</div>

							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-9 col-xs-4">
											<a href="mailto:@foreach($bio_par_region as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="top" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>
											<button data-clipboard-text="@foreach($bio_par_region as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
										</div>
										<div class="col-md-3 col-xs-8">
											<button class="btn btn-success dropdown-toggle btn-ctrls" data-toggle="dropdown" data-placement="top" title="Exporter des données" data-tooltip="true">
												Export <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="{{ route('admin.to_word') }}">Word</a></li>
												<li><a href="{{ route('admin.to_excel') }}">Excel</a></li>
											</ul>

											<button class="btn btn-success btn-ctrls" type="button" data-placement="top" title="Importer des données" data-tooltip="true" data-toggle="modal" data-target="#modalImport" >Import</button>

											<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="top" title="Ajouter un diplômé" data-tooltip="true">
												<i class="material-icons">person_add</i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="parregion">
								@php 
									$i=1;
								@endphp

								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel auvergne-rhone-alpes">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="auvergne-rhone-alpes">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
														<strong>Auvergne-Rhône-Alpes</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '01' || strtolower($ville) == 'bourg-en-bresse'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Ain (01)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '01' || strtolower($ville) == 'bourg-en-bresse'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '03' || strtolower($ville) == 'moulins'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Allier (03)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '03' || strtolower($ville) == 'moulins'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '07' || strtolower($ville) == 'privas'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Ardèche (07)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '07' || strtolower($ville) == 'privas'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '15' || strtolower($ville) == 'aurillac'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Cantal (15)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '15' || strtolower($ville) == 'aurillac'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '26' || strtolower($ville) == 'valence'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Drôme (26)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '26' || strtolower($ville) == 'valence'))
																		
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '38' || strtolower($ville) == 'grenoble'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Isère (38)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '38' || strtolower($ville) == 'grenoble'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '42' || strtolower($ville) == 'saint-étienne'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Loire (42)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '42' || strtolower($ville) == 'saint-étienne'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '43' || strtolower($ville) == 'le puy-en-velay'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Loire (43)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '43' || strtolower($ville) == 'le puy-en-velay'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '63' || strtolower($ville) == 'clermont-ferrand'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Puy-de-Dôme (63)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '63' || strtolower($ville) == 'clermont-ferrand'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '69' || strtolower($ville) == 'lyon'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Rhône et Métropole de Lyon (69)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '69' || strtolower($ville) == 'lyon'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '73' || strtolower($ville) == 'chambéry'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Savoie (73)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '73' || strtolower($ville) == 'chambéry'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '74' || strtolower($ville) == 'annecy'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Savoie (74)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '74' || strtolower($ville) == 'annecy'))
																		</tr>
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel bourgogne-franche-comte">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="bourgogne-franche-comte">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
														<strong>Bourgogne-Franche-Comté</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '21' || strtolower($ville) == 'dijon'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Côte-d'Or (21)</div>
																			</td>
																		</tr>
																		@break;
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '21' || strtolower($ville) == 'dijon'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '25' || strtolower($ville) == 'besançon'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Doubs (25)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '25' || strtolower($ville) == 'besançon'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '39')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Jura (39)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '39')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '58')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Nièvre (58)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '58')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '70')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Saône (70)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '70')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '71')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Saône-et-Loire (71)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '71')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '89')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Yonne (89)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '89')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '90')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Territoire de Belfort (90)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '90')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel bretagne">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="bretagne">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
														<strong>Bretagne</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '22'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Côtes-d'Armor (22)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '22'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '29' || strtolower($ville) == 'brest'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Finistère (29)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '29' || strtolower($ville) == 'brest'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '35' || strtolower($ville) == 'rennes')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Ille-et-Vilaine (35)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '35' || strtolower($ville) == 'rennes')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '56')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Morbihan (56)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '56')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel centre-val-de-loire">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="centre-val-de-loire">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
														<strong>Centre-Val de Loire</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '18')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Cher (18)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '18')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '28')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Eure-et-Loir (28)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '28')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '36')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Indre (36)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '36')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '37')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Indre-et-Loire (37)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '37')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '41')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Loir-et-Cher (41)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '41')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '45')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Loiret (45)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '45')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel corse">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="corse">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
														<strong>Corse</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '20')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel grand-est">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="grand-est">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
														<strong>Grand Est</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '08')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Ardennes (08)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '08')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '10')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Aube (10)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '10')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '51')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Marne (51)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '51')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '52')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Marne (52)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '52')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '54')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Meurthe-et-Moselle (54)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '54')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '55')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Meuse (55)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '55')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '57')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Moselle (57)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '57')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '67')
																		<tr>
																			<td colspan="13" class="success">
																				<div><i>Bas-Rhin (67)</i></div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '67')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '68')
																		<tr>
																			<td colspan="13" class="success">
																				<div><i>Haut-Rhin (68)</i></div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '68')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '88')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Vosges (88)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '88')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel hauts-de-france">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="hauts-de-france">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
														<strong>Hauts-de-France</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '02')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Aisne (02)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '02')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '59')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Nord (59)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '59')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '60')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Oise (60)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '60')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '62')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Pas-de-Calais (62)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '62')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '80')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Somme (80)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '80')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel ile-de-france">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="ile-de-france">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
														<strong>Île-de-France</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && (substr($bio->usr_cp_entreprise,0,2) == '75' || strtolower($ville) == 'paris'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Paris (75)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && (substr($bio->usr_cp_entreprise,0,2) == '75' || strtolower($ville) == 'paris'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '77')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Seine-et-Marne (77)</div>
																			</td>
																		</tr>
																		@break
																@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '77')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '78')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Yvelines (78)</div>
																			</td>
																		</tr>
																		@break
																@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '78')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '91')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Essonne (91)</div>
																			</td>
																		</tr>
																		@break
																@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '91')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '92')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Hauts-de-Seine (92)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '92')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '93')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Seine-Saint-Denis (93)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '93')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '94')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Val-de-Marne (94)</div>
																			</td>
																		</tr>
																		@break
																@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '94')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '95')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Val-d'Oise (95)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '95')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel normandie">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="normandie">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
														<strong>Normandie</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '14')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Calvados (14)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '14')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '27')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Eure (27)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '27')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '50')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Manche (50)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '50')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '61')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Orne (61)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '61')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '76')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Seine-Maritime (76)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '76')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel nouvelle-aquitaine">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="nouvelle-aquitaine">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
														<strong>Nouvelle-Aquitaine</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '16')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Charente (16)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '16')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '17')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Charente-Maritime (17)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '17')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '19')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Corrèze (19)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '19')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '23')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Creuse (23)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '23')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '24')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Dordogne (24)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '24')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '33')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Gironde (33)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '33')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '40')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Landes (40)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '40')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '47')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Lot-et-Garonne (47)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '47')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '64')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Pyrénées-Atlantiques (64)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '64')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '79')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Deux-Sèvres (79)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '79')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '86')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Vienne (86)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '86')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '87')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Vienne (87)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '87')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel occitanie">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="occitanie">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
														<strong>Occitanie</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '09')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Ariège (09)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '09')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '11')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Aude (11)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '11')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '12')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Aveyron (12)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '12')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '30')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Gard (30)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '30')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '31')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Haute-Garonne (31)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '31')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '32')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Gers (32)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '32')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '34')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Hérault (34)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '34')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '46')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Lot (46)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '46')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '48')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Lozère (48)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '48')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '65')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Hautes-Pyrénées (65)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '65')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '66')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Pyrénées-Orientales (66)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '66')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '81')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Tarn (81)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '81')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '82')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Tarn-et-Garonne (82)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '82')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel pays-de-la-loire">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="pays-de-la-loire">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
														<strong>Pays de la Loire</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '44')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Loire-Atlantique (44)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '44')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '49')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Maine-et-Loire (49)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '49')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '53')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Mayenne (53)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '53')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '72')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Sarthe (72)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '72')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '85')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Vendée (85)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '85')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel provence-alpes-cote-dAzur">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="provence-alpes-cote-dAzur">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
														<strong>Provence-Alpes-Côte d'Azur</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '04')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Alpes-de-Haute-Provence (04)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '04')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '05')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Hautes-Alpes (05)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '05')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '06')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Alpes-Maritimes (06)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '06')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '13' || strtolower($ville) == 'marseille'))
																		<tr>
																			<td colspan="13" class="success">
																				<div>Bouches-du-Rhône (13)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && ($region < 971 && substr($bio->usr_cp_entreprise,0,2) == '13' || strtolower($ville) == 'marseille'))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '83')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Var (83)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '83')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '84')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Vaucluse (84)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php 
																		$region = intval(substr($bio->usr_cp_entreprise,0,3 ));
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region < 971 && substr($bio->usr_cp_entreprise,0,2) == '84')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel dom-tom">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="dom-tom)">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse22" aria-expanded="false" aria-controls="collapse22">
														<strong>France métropolitaine (Dom-Tom)</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse22" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading22">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '971')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Guadeloupe (971)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '971')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '972')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Martinique (972)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '972')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '973')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Guyane (973)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '973')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '974')
																		<tr>
																			<td colspan="13" class="success">
																				<div>La Réunion (974)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '974')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '975')
																		<tr>
																			<td colspan="13" class="success">
																				<div>St Pierre et Miquelon (975)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '975')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '976')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Mayotte (976)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '976')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '986')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Wallis et Futuna (986)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '986')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '987')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Polynésie Française (987)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '987')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach

																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '988')
																		<tr>
																			<td colspan="13" class="success">
																				<div>Nouvelle Calédonie (988)</div>
																			</td>
																		</tr>
																		@break
																	@endif
																@endforeach
																@foreach($bio_par_region as $bio)
																	@php $region = substr($bio->usr_cp_entreprise,0,3 ); $ville = $bio->usr_ville_entreprise; @endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == 'France' && $region == '988')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
											</div>
									</div>

									<div class="panel etrangere">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="etrangere">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
														<strong>Hors France</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php
																		$ville = $bio->usr_ville_entreprise;
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail != 'France' && $bio->usr_pays_travail != '')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel sans-region">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="sans-region">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse24" aria-expanded="false" aria-controls="collapse24">
														<strong>Région non précisée</strong>
													</a>
												</h4>
											</div>
										</div>
										<div id="collapse24" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading24">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_region as $bio)
																	@php 
																		$ville = $bio->usr_ville_entreprise;
																		$villes = array('paris', 'moulins', 'dijon', 'bourg-en-bresse', 'annecy', 'chambéry', 'lyon', 'privas', 'aurillac', 'valence','grenoble','saint-étienne', 'le puy-en-velay','clermont-ferrand', 'brest', 'rennes','marseille');
																	@endphp
																	@if ($bio_par_region && $bio->usr_pays_travail == '' && $bio->usr_cp_entreprise == '' && !in_array(strtolower($ville), $villes))
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-6">
												<a href="mailto:@foreach($bio_par_region as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="bottom" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>
												<button data-clipboard-text="@foreach($bio_par_region as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
											</div>

											<div class="col-xs-6">
												<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="bottom" title="Ajouter un diplômé" data-tooltip="true">
													<i class="material-icons">person_add</i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal Add_student -->
				@include('layouts.modals.add_ancien')

				<!-- Admins data -->
				@include('layouts.adm_data')

				<!-- Modal Add_Admin -->
				@include('layouts.modals.add_admin')

				<!-- Modal Del_Ancien -->
				@include('layouts.modals.del_ancien')

				<!-- Modal Del_Admin -->
				@include('layouts.modals.del_admin')

				<!-- Modal Import -->
				@include('layouts.modals.import')

			</div>
		</div>
	</div>
</div>
@endif

@endsection
