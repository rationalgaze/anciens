@extends('layouts.app')

@section('content')
<div class="container">
	<section class="personal">
		<div class="row">
			<div class="col-xs-12 col-sm-5">
				<div class="avatar text-center">
					@if($usr->usr_img)
						<div class="halo">
							<img src="{{ asset('storage/avatars/'.$usr->usr_img) }}" class="img-circle">
						</div>
					@else
						<div class="halo">
							<img src="http://dummyimage.com/240/434343.jpg/e7e7e7&text= Avatar" class="img-circle">
						</div>
					@endif
				</div>
			</div>
			<div class="col-xs-12 col-sm-7 info">
				<h2 class="ancien_title text-capitalize" style="color: #e7e7e7">
					<strong>
						{{ strtolower($usr->usr_prenom).' '. strtolower($usr->usr_nom) }}
						@if($usr->usr_nom_naiss)
							{{ ' ('. strtolower($usr->usr_nom_naiss).')' }}
						@endif
					</strong>
				</h2>

				@if($usr->usr_adresse_perso || $usr->usr_cp || $usr->usr_ville || $usr->usr_pays)
					<p class="adress text-capitalize">
						<i class="material-icons"><small>location_on</small></i>
						<small>
							{{ 
								($usr->usr_cp)?$usr->usr_adresse_perso.', '.$usr->usr_cp.' '.strtolower($usr->usr_ville).', '.$usr->usr_pays :
								$usr->usr_adresse_perso.' '.$usr->usr_cp.' '.$usr->usr_ville.' '.$usr->usr_pays
							}}
						</small>
					</p>
				@endif

				@if($usr->usr_date_naiss)
					<p><i class="material-icons">cake</i>{{ 
						($usr->usr_lieu_naiss)?Carbon\Carbon::parse($usr->usr_date_naiss)->format('d F Y').', '.$usr->usr_lieu_naiss :Carbon\Carbon::parse($usr->usr_date_naiss)->format('d F Y') }}</p>
				@endif
				
				@if($usr->usr_email)
					<p class="mail">
						<a href="mailto:{{ $usr->usr_email }}"><i class="material-icons">mail_outline</i>{{ $usr->usr_email }}</a>
					</p>
				@endif

				@if(($usr->usr_annee_entree || $usr->usr_annee_sortie || $usr->usr_diplome) && ($usr->usr_annee_entree != '?' || $usr->usr_annee_sortie != '?' || $usr->usr_diplome != ''))
					<p>
						<i class="material-icons">school</i>
						@if($usr->usr_annee_entree && $usr->usr_annee_sortie && $usr->usr_diplome)
							{{ $usr->usr_annee_entree.' - '.$usr->usr_annee_sortie.' - '.$usr->usr_diplome }}
						@elseif(!$usr->usr_diplome || $usr->usr_diplome == '')
							{{ $usr->usr_annee_entree.' - '.$usr->usr_annee_sortie }}
						@else
							{{ $usr->usr_annee_entree.' '.$usr->usr_annee_sortie.' '.$usr->usr_diplome }}
						@endif
					</p>
				@endif

				@if($usr->usr_tel_mob||$usr->usr_tel_fixe)
					<p>
						<i class="material-icons">stay_current_portrait</i>
						@if($usr->usr_tel_fixe) 
							{{ chunk_split("$usr->usr_tel_mob", 2, " ").' ' }}<i class="material-icons" style="margin: 0 5px;">phone</i>{{ ' '.chunk_split("$usr->usr_tel_fixe", 2, " ") }}
						@else
							{{ chunk_split("$usr->usr_tel_mob",2, " ") }}
						@endif
					</p>
				@endif

				@if($usr->usr_fb)
					<p class="socials mail">
						<i class="material-icons fa fa-facebook-square"></i>
						<a href="{{ $usr->usr_fb }}" target="_blank">
							{{ $usr->usr_nom." ".$usr->usr_prenom }}
						</a>
					</p>
				@endif

			</div>
		</div>
	</section>

	<section class="pro">
		<div class="row">
			<div class="col-md-12">
				<h1>Données professionnelles</h1>
				<hr>
				@if($usr->usr_activite == 'Sans emploi')
					<p>Sans emploi</p>
				@elseif($usr->usr_activite == 'Retraite')
					<p>En retraite</p>
				@endif

				@if($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_secteur && $usr->usr_contrat)
					<p>
						<i class="material-icons">work</i>
						{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise.' ( '.$usr->usr_secteur.' ) - '.$usr->usr_contrat }}
					</p>
				@elseif($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_secteur)
					<p>
						<i class="material-icons">work</i>
						{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise.' ( '.$usr->usr_secteur.' )' }}
					</p>
				@elseif($usr->usr_poste && $usr->usr_nom_entreprise)
					<p>
						<i class="material-icons">work</i>
						{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise }}
					</p>
				@elseif($usr->usr_poste && $usr->usr_nom_entreprise && $usr->usr_contrat)
					<p>
						<i class="material-icons">work</i>
						{{ $usr->usr_poste.' - '.$usr->usr_nom_entreprise .' - '.$usr->usr_contrat }}
					</p>
				@elseif($usr->usr_poste||$usr->usr_nom_entreprise ||$usr->usr_secteur||$usr->usr_contrat)
					<p>
						<i class="material-icons">work</i>
						{{ $usr->usr_poste.' '.$usr->usr_nom_entreprise .' '.$usr->usr_secteur.' '.$usr->usr_contrat }}
					</p>
				@endif

				@if($usr->usr_adresse_entreprise || $usr->usr_cp_entreprise || $usr->usr_ville_entreprise || $usr->usr_pays_travail)
					<p class="adress-pro">
						<i class="material-icons">location_on</i>
							{{ 
								($usr->usr_cp_entreprise && $usr->usr_pays_travail)
									?
										$usr->usr_adresse_entreprise.', '.$usr->usr_cp_entreprise.' '.$usr->usr_ville_entreprise.', '.$usr->usr_pays_travail
									:
										$usr->usr_adresse_entreprise.' '.$usr->usr_cp_entreprise.' '.$usr->usr_ville_entreprise.' '.$usr->usr_pays_travail
							}}
					</p>
				@endif

				@if($usr->usr_email_pro)
					<p class="mail">
						<a href="mailto:{{ $usr->usr_email_pro }}"><i class="material-icons">mail_outline</i>{{ $usr->usr_email_pro }}</a>
					</p>
				@endif

				@if($usr->usr_tel_pro)
					<p>
						<i class="material-icons">phone</i>
						{{ chunk_split("$usr->usr_tel_pro", 2, " ") }}
					</p>
				@endif

				@if($usr->usr_site_web)
					<p>
						<i class="material-icons">public</i>
						<a href="http://www.{{ $usr->usr_site_web }}/" target="_blank">{{ $usr->usr_site_web }}</a>
					</p>
				@endif

				@if($usr->usr_cv)
					<p>
						<i class="material-icons">content_paste</i>
						<a href="{{ asset('storage/cv_storage/'.$usr->usr_cv) }}" alt="{{ $usr->usr_prenom.' '.$usr->usr_nom }}" target="_blank">voir CV</a>
					</p>
				@endif

				@if($usr->usr_linkedin)
					<p class="socials mail">
						<i class="material-icons fa fa-linkedin-square"></i>
						<a href="{{ $usr->usr_linkedin }}" target="_blank">
							LinkedIn
						</a>
					</p>
				@endif

				@if($usr->usr_viadeo)
					<p class="socials mail">
						<i class="material-icons fa fa-viadeo-square"></i>
						<a href="{{ $usr->usr_viadeo }}" target="_blank">
							Viadeo
						</a>
					</p>
				@endif

				@if($usr->usr_donnees == 'oui')
					<div>J’accepte l’utilisation de mes données professionnelles et personnelles</div>
				@elseif($usr->usr_donnees == 'pro')
					<div>J’accepte l’utilisation de mes données professionnelles</div>
				@else
					<div>J'accepte pas l'utilisation de mes données</div>
				@endif
			</div>
		</div>
	</section>
	<div class="row">
		<a href="{{ url()->previous() }}" class="btn btn-default btn-md pull-right btn-float">
			<i class="material-icons">keyboard_backspace</i>
		</a>
	</div>
</div>
@endsection