@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li class="active"><a href="#students" role="tab" aria-controls="students" data-toggle="tab">Diplômés</a></li>
						<li><a href="#admins" role="tab" aria-controls="admins" data-toggle="tab">Administrateurs</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 main-section">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="students">
					<div class="panel panel-default">
						<div class="panel-heading"><h4><strong>Liste des diplômé(e)s de l'ESIAB</strong></h4></div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="{{ route('admin.dashboard') }}" class="btn btn-default btn-xs">A - Z</a>
										<a href="{{ route('admin.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
										<a href="{{ route('admin.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
										<a href="#" class="btn btn-default btn-xs active disabled">Tri par parcours</a>
									</div>
								</div>
								<hr>
							</div>

							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-9 col-xs-4">
											<a href="mailto:@foreach($bio_par_parcours as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="bottom" title="Envoyer un message à tous les diplômé(e)s" data-tooltip="true"><i class="material-icons">email</i></a>
											<button data-clipboard-text="@foreach($bio_par_parcours as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômé(e)s" data-tooltip="true"><i class="material-icons">content_copy</i></button>
										</div>
										<div class="col-md-3 col-xs-8">
											<button class="btn btn-success dropdown-toggle btn-ctrls" data-toggle="dropdown" data-placement="top" title="Exporter des données" data-tooltip="true">
												Export <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="{{ route('admin.to_word') }}">Word</a></li>
												<li><a href="{{ route('admin.to_excel') }}">Excel</a></li>
											</ul>

											<button class="btn btn-success btn-ctrls" type="button" data-placement="top" title="Importer des données" data-tooltip="true" data-toggle="modal" data-target="#modalImport" >Import</button>

											<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="top" title="Ajouter un diplômé(e)" data-tooltip="true">
												<i class="material-icons">person_add</i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="parparcours">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel viande">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
														Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l'Université de Brest" (1975-1992)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@php $i=1; @endphp
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbio">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
														Ingénieur Microbiologiste, diplômé(e) de l'Université de Brest (1985-1993)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbiologie-et-securite">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
														Ingénieur diplômé(e) de l'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel microbiologie-et-qualite">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
														Ingénieur diplômé(e) de l'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel agroalimentaire">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
														Ingénieur diplômé(e) de l'Université de Brest, spécialité "agroalimentaire" en partenariat avec l'IFRIA Bretagne (2002-2012)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne (2002-2012)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel procedes-indutriels">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
														Ingénieur diplômé(e) de l'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l'IFRIA Bretagne (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel innovation">
										<div class="panel panel-default">
											<div class="panel-heading" role="tab">
												<h4 class="panel-title">
													<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
														Master professionnel diplômé(e) de l'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)
													</a>
												</h4>
											</div>
										</div>

										<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
											<div class="panel-body">
												<div class="row">
													<div class="table-responsive">
														<table class="table table-striped">
															<thead>
																<tr>
																	<th width="20">&#8470;</th>
																	<th width="20">Civilité</th>
																	<th>Nom</th>
																	<th>Prénom</th>
																	<th>E-mail</th>
																	<th>Année de sortie</th>
																	<th>Poste</th>
																	<th>Entreprise</th>
																	<th>Ville (pro.)</th>
																	<th>Pays (pro.)</th>
																	<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
																	<th class="text-center" width="20"><i class="material-icons">edit</i></th>
																	<th class="text-center" width="20"><i class="material-icons">delete</i></th>
																</tr>
															</thead>
															<tbody>
																@foreach($bio_par_parcours as $bio)
																	@if ($bio_par_parcours && $bio->usr_diplome == 'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013)')
																		<tr>
																			<td class="table-text">
																				<div>{{ $i++ }}</div>
																			</td>
																			<td class="table-text">
																				{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_nom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div>
																					<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																						{{ $bio->usr_prenom }}
																					</a>
																				</div>
																			</td>
																			<td class="table-text">
																				<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																			</td>
																			<td class="table-text">
																				<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																			</td>
																			<td class="table-text">
																				<div>{{ $bio->usr_poste }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_nom_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_ville_entreprise }}</div>
																			</td>
																			<td>
																				<div>{{ $bio->usr_pays_travail }}</div>
																			</td>
																			<td class="table-text">
																				<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																					<i class="material-icons">perm_identity</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																					<i class="material-icons">edit</i>
																				</a>
																			</td>
																			<td class="table-text">
																				<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i</button>
																			</td>
																		</tr>
																	@endif
																@endforeach
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-xs-6">
												<a href="mailto:@foreach($bio_par_parcours as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="bottom" title="Envoyer un message à tous les diplômé(e)s" data-tooltip="true"><i class="material-icons">email</i></a>
												<button data-clipboard-text="@foreach($bio_par_parcours as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômé(e)s" data-tooltip="true"><i class="material-icons">content_copy</i></button>
											</div>

											<div class="col-xs-6">
												<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="bottom" title="Ajouter un diplômé(e)" data-tooltip="true">
													<i class="material-icons">person_add</i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal Add_student -->
				@include('layouts.modals.add_ancien')

				<!-- Admins data -->
				@include('layouts.adm_data')

				<!-- Modal Add_Admin -->
				@include('layouts.modals.add_admin')

				<!-- Modal Del_Ancien -->
				@include('layouts.modals.del_ancien')

				<!-- Modal Del_Admin -->
				@include('layouts.modals.del_admin')

				<!-- Modal Import -->
				@include('layouts.modals.import')

			</div>
		</div>
	</div>
</div>
@endsection
