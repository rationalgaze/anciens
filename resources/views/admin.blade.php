@extends('layouts.app')

@section('content')
<div class="container">
	@if(Auth::check())
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<ul class="nav nav-pills">
						<li class="active"><a href="#students" role="tab" aria-controls="students" data-toggle="tab">Diplômés</a></li>
						<li><a href="#admins" role="tab" aria-controls="admins" data-toggle="tab">Administrateurs</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 main-section">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="students">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4><strong>Liste des diplômés de l'ESIAB</strong>
							<span class="badge pull-right">{{ $users_all->count() }}</span></h4>
						</div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							@if(Session::has('message'))
								<div class="alert alert-dismissible alert-success">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									{{ Session::get('message') }}
								</div>
							@elseif(Session::has('error'))
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									{{ Session::get('error') }}
								</div>
							@endif

							<div class="row">
								<div class="col-xs-12 text-center">
									<div class="btn-group btn-group-sm nav"> 
										<a href="#paraz" class="btn btn-default btn-xs active disabled">A - Z</a>
										<a href="{{ route('admin.par_region') }}" class="btn btn-default btn-xs">Tri par région</a>
										<a href="{{ route('admin.par_secteur') }}" class="btn btn-default btn-xs">Tri par secteur</a>
										<a href="{{ route('admin.par_parcours') }}" class="btn btn-default btn-xs">Tri par parcours</a>
										<a href="{{ route('par_annee') }}" class="btn btn-default btn-xs">Tri par année</a>
									</div>
								</div>
								<hr>
								<div class="col-xs-12 text-center az_pages">
									<a href="{{route('a')}}">A</a>
									<a href="{{route('b')}}">B</a>
									<a href="{{route('c')}}">C</a>
									<a href="{{route('d')}}">D</a>
									<a href="{{route('e')}}">E</a>
									<a href="{{route('f')}}">F</a>
									<a href="{{route('g')}}">G</a>
									<a href="{{route('h')}}">H</a>
									<a href="{{route('i')}}">I</a>
									<a href="{{route('j')}}">J</a>
									<a href="{{route('k')}}">K</a>
									<a href="{{route('l')}}">L</a>
									<a href="{{route('m')}}">M</a>
									<a href="{{route('n')}}">N</a>
									<a href="{{route('o')}}">O</a>
									<a href="{{route('p')}}">P</a>
									<a href="{{route('q')}}">Q</a>
									<a href="{{route('r')}}">R</a>
									<a href="{{route('s')}}">S</a>
									<a href="{{route('t')}}">T</a>
									<a href="{{route('u')}}">U</a>
									<a href="{{route('v')}}">V</a>
									<a href="{{route('w')}}">W</a>
									<a href="{{route('x')}}">X</a>
									<a href="{{route('y')}}">Y</a>
									<a href="{{route('z')}}">Z</a>
									<a href="{{route('all')}}">Tous</a>
									<a href="{{route('new')}}">Derniers</a>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-9 col-xs-12">
											<a href="mailto:@foreach($users_all as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm btn-ctrls" data-placement="top" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>

											<button data-clipboard-text="@foreach($users_all as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd btn-ctrls" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
										</div>

										<div class="col-md-3 col-xs-12">
											<button class="btn btn-success dropdown-toggle btn-ctrls" data-toggle="dropdown" data-placement="top" title="Exporter des données" data-tooltip="true">
												Export <span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												<li><a href="{{ route('admin.to_word') }}">Word</a></li>
												<li><a href="{{ route('admin.to_excel') }}">Excel</a></li>
											</ul>

											<button class="btn btn-success btn-ctrls" type="button" data-placement="top" title="Importer des données" data-tooltip="true" data-toggle="modal" data-target="#modalImport" >Import</button>

											<a href="#" class="btn btn-success btn-sm pull-right btn-ctrls" data-toggle="modal" data-target="#modalAdd" data-placement="top" title="Ajouter un diplômé" data-tooltip="true">
												<i class="material-icons">person_add</i>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel2" class="tab-pane active" id="paraz">
								@php $i=$users_bio->firstItem(); @endphp
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row">
											<div class="col-md-12 col-xs-12 text-center">
													{{ $users_bio->links() }}
											</div>
										</div>
										<div class="row">
											<div class="table-responsive">
												<table class="table table-striped task-table">
													<thead>
														<tr>
															<th width="20">&#8470;</th>
															<th width="20">Civilité</th>
															<th>Nom</th>
															<th>Prénom</th>
															<th>E-mail</th>
															<th>Année de sortie</th>
															<th class="text-center" width="20"><i class="material-icons">perm_identity</i></th>
															<th class="text-center" width="20"><i class="material-icons">edit</i></th>
															<th class="text-center" width="20"><i class="material-icons">delete</i></th>
														</tr>
													</thead>
													<tbody>
														@foreach($users_bio as $bio)
															<tr>
																<td class="table-text">
																	<div>{{ $i++ }}</div>
																</td>
																@if ($users_bio)
																	<td class="table-text">
																		{{ ($bio->usr_sex != '')? (($bio->usr_sex == 'H') ? 'M' : 'MME') : null }}
																	</td>
																	<td class="table-text">
																		<div>
																			<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																				{{ $bio->usr_nom }}
																			</a>
																		</div>
																	</td>
																	<td class="table-text">
																		<div>
																			<a href="/admin/show/{{ $bio->user_id }}" class="" data-placement="top" title="Voir les données" data-tooltip="true">
																				{{ $bio->usr_prenom }}
																			</a>
																		</div>
																	</td>
																	<td class="table-text">
																		<div><a href="mailto:{{ $bio->usr_email }}">{{ $bio->usr_email }}</a></div>
																	</td>
																	<td class="table-text">
																		<div>@if ($bio->usr_annee_sortie){{ $bio->usr_annee_sortie }} @endif</div>
																	</td>
																	<td class="table-text">
																		<a href="/admin/show/{{ $bio->user_id }}" class="btn btn-info btn-xs btn-block" data-placement="bottom" title="Voir des données" data-tooltip="true">
																			<i class="material-icons">perm_identity</i>
																		</a>
																	</td>
																	<td class="table-text">
																		<a href="/admin/edit/{{ $bio->user_id }}" class="btn btn-primary btn-xs btn-block" data-placement="bottom" title="Modifier des données" data-tooltip="true">
																			<i class="material-icons">edit</i>
																		</a>
																	</td>
																	<td class="table-text">
																		<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete" data-delete="{{ $bio->user_id }}"><i class="material-icons">delete</i></button>
																	</td>
																@endif
															</tr>
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-md-12 col-xs-12 text-center">
													{{ $users_bio->links() }}
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<hr>
									<div class="col-xs-12">
										<div class="row">
											<div class="col-md-9 col-xs-12">
												<a href="mailto:@foreach($users_all as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm" data-placement="top" title="Envoyer un message à tous les diplômés" data-tooltip="true"><i class="material-icons">email</i></a>
												<button data-clipboard-text="@foreach($users_all as $bio)@if(!preg_match('/@temp\.fr/i',$bio->usr_email)){{ $bio->usr_email }},@endif @endforeach" class="btn btn-success btn-sm cpy_cbd" data-placement="top" title="Copier les emails de tous les diplômés" data-tooltip="true"><i class="material-icons">content_copy</i></button>
											</div>

											<div class="col-md-3 col-xs-12">
												<button class="btn btn-success dropdown-toggle" data-toggle="dropdown" data-placement="top" title="Exporter des données" data-tooltip="true">
													Export <span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
													<li><a href="{{ route('admin.to_word') }}">Word</a></li>
													<li><a href="{{ route('admin.to_excel') }}">Excel</a></li>
												</ul>
												<button class="btn btn-success" type="button" data-placement="top" title="Importer des données" data-tooltip="true" data-toggle="modal" data-target="#modalImport" >Import</button>

												<a href="#" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modalAdd" data-placement="top" title="Ajouter un diplômé" data-tooltip="true">
													<i class="material-icons">person_add</i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Modal Add_student -->
				@include('layouts.modals.add_ancien')

				<!-- Admins data -->
				@include('layouts.adm_data')

				<!-- Modal Add_Admin -->
				@include('layouts.modals.add_admin')

				<!-- Modal Del_Ancien -->
				@include('layouts.modals.del_ancien')

				<!-- Modal Del_Admin -->
				@include('layouts.modals.del_admin')

				<!-- Modal Import -->
				@include('layouts.modals.import')
			</div>
		</div>
	</div>
	</div>
	@endif
</div>
@endsection
