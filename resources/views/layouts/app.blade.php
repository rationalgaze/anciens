<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="{{ asset('images/favicon.ico') }}" type="image/x-icon" rel="shortcut icon">
	<meta name="description" content="Annuaire électronique des diplômés de l'ESIAB - UBO - Brest">
	<meta name="keywords" content="Annuaire électronique des diplômés de l'ESIAB - UBO - Brest">
	<meta name="robots" content="noindex, nofollow">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Anciens | Annuaire électronique des diplômés de l'ESIAB</title>

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css"/>

	<!-- Scripts -->
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>

	<!-- Schema.org markup (Google) -->
	<meta itemprop="name" content="Anciens de l'ESIAB">
	<meta itemprop="description" content="Annuaire électronique des diplômés de l'ESIAB">
	<meta itemprop="image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}">

	<!-- Twitter Card markup-->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@annuaireESIAB">
	<meta name="twitter:title" content="Annuaire électronique des diplômés de l'ESIAB">
	<meta name="twitter:description" content="Annuaire électronique des diplômés de l'ESIAB">
	<meta name="twitter:creator" content="@NikolaiVorotnikov">
	<meta name="twitter:image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}">
	<meta name="twitter:image:alt" content="Annuaire électronique des diplômés de l'ESIAB">

	<!-- Open Graph markup (Facebook, Pinterest) -->
	<meta property="og:title" content="Anciens de l'ESIAB" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="https://222xesiab12.univ-brest.fr" />
	<meta property="og:image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}" />
	<meta property="og:description" content="Annuaire électronique des diplômés de l'ESIAB" />
	<meta property="og:site_name" content="Annuaire électronique des diplômés de l'ESIAB" />
</head>
<body>
	<div id="app">
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">

					<!-- Collapsed Hamburger -->
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
						<span class="sr-only">Toggle Navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<!-- Branding Image -->
					<a class="navbar-brand" href="{{ url('/') }}">
						Annuaire électronique des diplômés de l'ESIAB
					</a>
				</div>

				<div class="collapse navbar-collapse" id="app-navbar-collapse">
					<!-- Left Side Of Navbar -->
					<ul class="nav navbar-nav">
						&nbsp;
					</ul>

					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav navbar-right">
						<!-- Authentication Links -->
						@if (Auth::guest())
							<li>
								<a href="#" type="button" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Se connecter</a>
								<ul class="dropdown-menu">
									<li><a href="{{ url('/login') }}" class="text-capitalize">Diplômés</a></li>
									<li><a href="{{ url('/admin') }}" class="text-capitalize">Administrateurs</a></li>
								</ul>
							</li>
						@else
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									{{ Auth::user()->name }} <span class="caret"></span>
								</a>

								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="{{ route('logout') }}"
											onclick="event.preventDefault();
													 document.getElementById('logout-form').submit();">
											<i class="material-icons">keyboard_backspace</i>
											<span class="logout">Se déconnecter</span>
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
									</li>
								</ul>
							</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>

		@yield('content')
	</div>

	<footer>
		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
	</footer>
</body>
</html>
