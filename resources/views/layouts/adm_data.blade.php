				<div role="tabpanel" class="tab-pane" id="admins">
					<div class="panel panel-default">
						<div class="panel-heading"><h4><strong>Administrateurs</strong></h4></div>
						<div class="panel-body">
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<div class="panel panel-default">
								<div class="panel-body">
									<div class="row">
										<div class="table-responsive">
											<table class="table table-striped task-table">
												<thead>
													<tr>
														<th width="20px">&#8470;</th>
														<th>Nom</th>
														<th>E-mail</th>
														<th>Date de création</th>
														<th class="text-center" width="20px"><i class="material-icons">edit</i></th>
														<th class="text-center" width="20px"><i class="material-icons">delete</i></th>
													</tr>
												</thead>
												<tbody><?php $j=1; ?>
													@foreach($admins as $admin)
														<tr>
															<td class="table-text">
																<div><?php echo $j++; ?></div>
															</td>
															<td>
																<div>{{ $admin->name }}</div>
															</td>
															<td>
																<div>{{ $admin->email }}</div>
															</td>
															<td>
																<div>{{ $admin->created_at->format('d-m-Y') }}</div>
															</td>
															<td class="table-text">
																<a href="/admin/edit_admin/{{ $admin->id }}" class="btn btn-primary btn-xs btn-block">
																	<i class="material-icons">edit</i>
																</a>
															</td>
														@if($admin->id === 1)
															<td class="table-text">
																<button type="submit" class="btn btn-danger btn-xs btn-block disabled"><i class="material-icons">delete</i></button>
															</td>
														@else
															<td class="table-text">
																<button type="button" class="btn btn-danger btn-xs btn-block" data-toggle="modal" data-target="#delete_adm" data-delete_adm="{{ $admin->id }}"><i class="material-icons">delete</i</button>
															</td>
														@endif
														</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<hr>
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-6">
											<a href="mailto:@foreach($admins as $admin){{ $admin->email }},@endforeach" class="btn btn-success btn-sm" data-placement="bottom" title="Envoyer un message à tous les administrateurs" data-tooltip="true"><i class="material-icons">email</i></a>
											<button data-clipboard-text="@foreach($admins as $admin){{ $admin->email }},@endforeach" class="btn btn-success btn-sm cpy_cbd" data-placement="top" title="Copier les emails de tous les administrateurs" data-tooltip="true"><i class="material-icons">content_copy</i></button>
										</div>
										<div class="col-xs-6">
											<a href="#" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modalAddAdmin" data-placement="bottom" title="Ajouter un administrateur" data-tooltip="true">
												<i class="material-icons">person_add</i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>