<!-- Modal Import -->
<div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="Import">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Importer des données</strong></h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<p>Choisissez le fichier au formats: <span class="text-primary">xls, xlsx, ods</span>. Les noms des colonnes doivent correspondre exactement à ceux indiqués ci-dessous en respectant la casse (<span style="text-decoration: underline;">tout en minuscule</span>) :</p>
						<div class="table-responsive" style="margin-bottom: 10px;">
							<table class="table">
									<tr style="font-size: 10px;">
										<th>nom</th>
										<th>prenom</th>
										<th>mail</th>
										<th>date_de_naissance</th>
										<th>adresse_1</th>
										<th>adresse_2</th>
										<th>adresse_3</th>
										<th>adresse_etranger</th>
										<th>cp</th>
										<th>adresse_commune</th>
										<th>telephone</th>
										<th></th>
									</tr>
							</table>
						</div>
						<p> Evitez des doublons dans vos tables ! Hormis les données correspondant aux colonnes ci-dessus, aucune autre donnée (formules, graphiques, statistiques) ne doit apparaitre dans le fichier d'import. L'import d'un fichier comportant plusieurs feuilles est possible.</p>
						<form class="form-horizontal text-center" role="form" method="POST" action="{{ route('admin.importExcel') }}" enctype="multipart/form-data">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							<input type="file" name="import_file" id="upl_ex_hidden" onchange="document.getElementById('f-name').innerHTML='fichier : /'+this.value.replace(/.*[\/\\]/, '')" class="upl_hidden">

							<button class="btn btn-success" type="button" onclick="event.preventDefault(); document.getElementById('upl_ex_hidden').click();">
								Choisir un fichier
							</button>
							<button class="btn btn-danger" type="submit">Importer</button>
						</form>
						<div class="text-center" id="f-name" style="margin-top: 10px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
