<!-- Modal Add_student -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="Add">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel"><strong>Ajouter des données d'un diplômé</strong></h4>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<form class="form-horizontal" role="form" method="POST" action="{{ route('admin.add_usr') }}"  enctype="multipart/form-data">
							{{ csrf_field() }}

							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif

							{{-- Personal data --}}
							<fieldset class="col-md-6">
								<h4 class="modal-title" id="myModalLabel"><strong>Données personnelles</strong></h4>
								<hr>
								<div class="form-group">
									<label for="name" class="col-md-5 control-label">Êtes vous un/une</label>

									<div class="col-md-6">
										<label class="radio-inline">
											<input type="radio" name="sex" id="sex_h" value="H">Homme
										</label>
										<label class="radio-inline">
											<input type="radio" name="sex" id="sex_f" value="F">Femme
										</label>
									</div>
								</div>

								<div class="form-group{{ $errors->has('nom') ? ' has-error' : '' }}">
									<label for="nom" class="col-md-5 control-label">Nom</label>

									<div class="col-md-6">
										<input id="nom" type="text" class="form-control" name="nom" placeholder="Nom" autofocus>

										@if ($errors->has('nom'))
											<span class="help-block">
												<strong>{{ $errors->first('nom') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('nom_naiss') ? ' has-error' : '' }}">
									<label for="nom_naiss" class="col-md-5 control-label">Nom de naissance</label>

									<div class="col-md-6">
										<input id="nom_naiss" type="text" class="form-control" name="nom_naiss" placeholder="Nom de naissance">

										@if ($errors->has('nom_naiss'))
											<span class="help-block">
												<strong>{{ $errors->first('nom_naiss') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('prenom') ? ' has-error' : '' }} required">
									<label for="prenom" class="col-md-5 control-label">Prenom</label>

									<div class="col-md-6">
										<input id="prenom" type="text" class="form-control" name="prenom" placeholder="Prenom" required>

										@if ($errors->has('prenom'))
											<span class="help-block">
												<strong>{{ $errors->first('prenom') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} required">
									<label for="email" class="col-md-5 control-label">E-mail</label>

									<div class="col-md-6">
										<input id="email" type="text" class="form-control" name="email" placeholder="E-mail" required>

										@if ($errors->has('email'))
											<span class="help-block">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('date_naiss') ? ' has-error' : '' }}">
									<label for="date_naiss" class="col-md-5 control-label">Date de naissance</label>

									<div class="col-md-6">
										<input id="date_naiss" type="date" class="form-control" name="date_naiss" placeholder="Format : aaaa-mm-jj">

										@if ($errors->has('date_naiss'))
											<span class="help-block">
												<strong>{{ $errors->first('date_naiss') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('lieu_naiss') ? ' has-error' : '' }}">
									<label for="lieu_naiss" class="col-md-5 control-label">Lieu de naissance</label>

									<div class="col-md-6">
										<input id="lieu_naiss" type="text" class="form-control" name="lieu_naiss" placeholder="Votre Lieu de Naissance">

										@if ($errors->has('lieu_naiss'))
											<span class="help-block">
												<strong>{{ $errors->first('lieu_naiss') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('adresse_perso') ? ' has-error' : '' }}">
									<label for="adresse_perso" class="col-md-5 control-label">Adresse personnelle</label>

									<div class="col-md-6">
										<input id="adresse_perso" type="text" class="form-control" name="adresse_perso" placeholder="Ex.: 2 rue de Brest">

										@if ($errors->has('adresse_perso'))
											<span class="help-block">
												<strong>{{ $errors->first('adresse_perso') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('cp') ? ' has-error' : '' }}">
									<label for="cp" class="col-md-5 control-label">Code postale</label>

									<div class="col-md-6">
										<input id="cp" type="text" class="form-control" name="cp" placeholder="Ex.: 29200">

										@if ($errors->has('cp'))
											<span class="help-block">
												<strong>{{ $errors->first('cp') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('ville') ? ' has-error' : '' }}">
									<label for="ville" class="col-md-5 control-label">Ville</label>

									<div class="col-md-6">
										<input id="ville" type="text" class="form-control" name="ville" placeholder="Ex.: Brest">

										@if ($errors->has('ville'))
											<span class="help-block">
												<strong>{{ $errors->first('ville') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('pays') ? ' has-error' : '' }}">
									<label for="ville" class="col-md-5 control-label">Pays</label>

									<div class="col-md-6">
										{!! Form::select('pays',
											[
												'' =>'Sélectionnez un pays',
												'France' =>'France',

												'Afghanistan'=>'Afghanistan',
												'Afrique_Centrale'=>'Afrique_Centrale',
												'Afrique_du_sud'=>'Afrique_du_Sud',
												'Albanie'=>'Albanie',
												'Algerie'=>'Algerie',
												'Allemagne'=>'Allemagne',
												'Andorre'=>'Andorre',
												'Angola'=>'Angola',
												'Anguilla'=>'Anguilla',
												'Arabie_Saoudite'=>'Arabie_Saoudite',
												'Argentine'=>'Argentine',
												'Armenie'=>'Armenie',
												'Australie'=>'Australie',
												'Autriche'=>'Autriche',
												'Azerbaidjan'=>'Azerbaidjan',
												'Bahamas'=>'Bahamas',
												'Bangladesh'=>'Bangladesh',
												'Barbade'=>'Barbade',
												'Bahrein'=>'Bahrein',
												'Belgique'=>'Belgique',
												'Belize'=>'Belize',
												'Benin'=>'Benin',
												'Bermudes'=>'Bermudes',
												'Bielorussie'=>'Bielorussie',
												'Bolivie'=>'Bolivie',
												'Botswana'=>'Botswana',
												'Bhoutan'=>'Bhoutan',
												'Boznie_Herzegovine'=>'Boznie_Herzegovine',
												'Bresil'=>'Bresil',
												'Brunei'=>'Brunei',
												'Bulgarie'=>'Bulgarie',
												'Burkina_Faso'=>'Burkina_Faso',
												'Burundi'=>'Burundi',
												'Caiman'=>'Caiman',
												'Cambodge'=>'Cambodge',
												'Cameroun'=>'Cameroun',
												'Canada'=>'Canada',
												'Canaries'=>'Canaries',
												'Cap_vert'=>'Cap_Vert',
												'Chili'=>'Chili',
												'Chine'=>'Chine',
												'Chypre'=>'Chypre',
												'Colombie'=>'Colombie',
												'Comores'=>'Colombie',
												'Congo'=>'Congo',
												'Congo_democratique'=>'Congo_democratique',
												'Cook'=>'Cook',
												'Coree_du_Nord'=>'Coree_du_Nord',
												'Coree_du_Sud'=>'Coree_du_Sud',
												'Costa_Rica'=>'Costa_Rica',
												'Cote_d_Ivoire'=>'Côte_d_Ivoire',
												'Croatie'=>'Croatie',
												'Cuba'=>'Cuba',
												'Danemark'=>'Danemark',
												'Djibouti'=>'Djibouti',
												'Dominique'=>'Dominique',
												'Egypte'=>'Egypte',
												'Emirats_Arabes_Unis'=>'Emirats_Arabes_Unis',
												'Equateur'=>'Equateur',
												'Erythree'=>'Erythree',
												'Espagne'=>'Espagne',
												'Estonie'=>'Estonie',
												'Etats_Unis'=>'Etats_Unis',
												'Ethiopie'=>'Ethiopie',
												'Falkland'=>'Falkland',
												'Feroe'=>'Feroe',
												'Fidji'=>'Fidji',
												'Finlande'=>'Finlande',
												'France'=>'France',
												'Gabon'=>'Gabon',
												'Gambie'=>'Gambie',
												'Georgie'=>'Georgie',
												'Ghana'=>'Ghana',
												'Gibraltar'=>'Gibraltar',
												'Grece'=>'Grece',
												'Grenade'=>'Grenade',
												'Groenland'=>'Groenland',
												'Guadeloupe'=>'Guadeloupe',
												'Guam'=>'Guam',
												'Guatemala'=>'Guatemala',
												'Guernesey'=>'Guernesey',
												'Guinee'=>'Guinee',
												'Guinee_Bissau'=>'Guinee_Bissau',
												'Guinee equatoriale'=>'Guinee_Equatoriale',
												'Guyana'=>'Guyana',
												'Guyane_Francaise'=>'Guyane_Francaise',
												'Haiti'=>'Haiti',
												'Hawaii'=>'Hawaii',
												'Honduras'=>'Honduras',
												'Hong_Kong'=>'Hong_Kong',
												'Hongrie'=>'Hongrie',
												'Inde'=>'Inde',
												'Indonesie'=>'Indonesie',
												'Iran'=>'Iran',
												'Iraq'=>'Iraq',
												'Irlande'=>'Irlande',
												'Islande'=>'Islande',
												'Israel'=>'Israel',
												'Italie'=>'italie',
												'Jamaique'=>'Jamaique',
												'Jan Mayen'=>'Jan Mayen',
												'Japon'=>'Japon',
												'Jersey'=>'Jersey',
												'Jordanie'=>'Jordanie',
												'Kazakhstan'=>'Kazakhstan',
												'Kenya'=>'Kenya',
												'Kirghizstan'=>'Kirghizistan',
												'Kiribati'=>'Kiribati',
												'Koweit'=>'Koweit',
												'Laos'=>'Laos',
												'Lesotho'=>'Lesotho',
												'Lettonie'=>'Lettonie',
												'Liban'=>'Liban',
												'Liberia'=>'Liberia',
												'Liechtenstein'=>'Liechtenstein',
												'Lituanie'=>'Lituanie',
												'Luxembourg'=>'Luxembourg',
												'Lybie'=>'Lybie',
												'Macao'=>'Macao',
												'Macedoine'=>'Macedoine',
												'Madagascar'=>'Madagascar',
												'Madère'=>'Madère',
												'Malaisie'=>'Malaisie',
												'Malawi'=>'Malawi',
												'Maldives'=>'Maldives',
												'Mali'=>'Mali',
												'Malte'=>'Malte',
												'Man'=>'Man',
												'Mariannes du Nord'=>'Mariannes du Nord',
												'Maroc'=>'Maroc',
												'Marshall'=>'Marshall',
												'Martinique'=>'Martinique',
												'Maurice'=>'Maurice',
												'Mauritanie'=>'Mauritanie',
												'Mayotte'=>'Mayotte',
												'Mexique'=>'Mexique',
												'Micronesie'=>'Micronesie',
												'Midway'=>'Midway',
												'Moldavie'=>'Moldavie',
												'Monaco'=>'Monaco',
												'Mongolie'=>'Mongolie',
												'Montserrat'=>'Montserrat',
												'Mozambique'=>'Mozambique',
												'Namibie'=>'Namibie',
												'Nauru'=>'Nauru',
												'Nepal'=>'Nepal',
												'Nicaragua'=>'Nicaragua',
												'Niger'=>'Niger',
												'Nigeria'=>'Nigeria',
												'Niue'=>'Niue',
												'Norfolk'=>'Norfolk',
												'Norvege'=>'Norvege',
												'Nouvelle_Caledonie'=>'Nouvelle_Caledonie',
												'Nouvelle_Zelande'=>'Nouvelle_Zelande',
												'Oman'=>'Oman',
												'Ouganda'=>'Ouganda',
												'Ouzbekistan'=>'Ouzbekistan',
												'Pakistan'=>'Pakistan',
												'Palau'=>'Palau',
												'Palestine'=>'Palestine',
												'Panama'=>'Panama',
												'Papouasie_Nouvelle_Guinee'=>'Papouasie_Nouvelle_Guinee',
												'Paraguay'=>'Paraguay',
												'Pays_Bas'=>'Pays_Bas',
												'Perou'=>'Perou',
												'Philippines'=>'Philippines',
												'Pologne'=>'Pologne',
												'Polynesie'=>'Polynesie',
												'Porto_Rico'=>'Porto_Rico',
												'Portugal'=>'Portugal',
												'Qatar'=>'Qatar',
												'Republique_Dominicaine'=>'Republique_Dominicaine',
												'Republique_Tcheque'=>'Republique_Tcheque',
												'Reunion'=>'Reunion',
												'Roumanie'=>'Roumanie',
												'Royaume_Uni'=>'Royaume_Uni',
												'Russie'=>'Russie',
												'Rwanda'=>'Rwanda',
												'Sahara Occidental'=>'Sahara Occidental',
												'Sainte_Lucie'=>'Sainte_Lucie',
												'Saint_Marin'=>'Saint_Marin',
												'Salomon'=>'Salomon',
												'Salvador'=>'Salvador',
												'Samoa_Occidentales'=>'Samoa_Occidentales',
												'Samoa_Americaine'=>'Samoa_Americaine',
												'Sao_Tome_et_Principe'=>'Sao_Tome_et_Principe',
												'Senegal'=>'Senegal',
												'Seychelles'=>'Seychelles',
												'Sierra Leone'=>'Sierra Leone',
												'Singapour'=>'Singapour',
												'Slovaquie'=>'Slovaquie',
												'Slovenie'=>'Slovenie',
												'Somalie'=>'Somalie',
												'Soudan'=>'Soudan',
												'Sri_Lanka'=>'Sri_Lanka',
												'Suede'=>'Suede',
												'Suisse'=>'Suisse',
												'Surinam'=>'Surinam',
												'Swaziland'=>'Swaziland',
												'Syrie'=>'Syrie',
												'Tadjikistan'=>'Tadjikistan',
												'Taiwan'=>'Taiwan',
												'Tonga'=>'Tonga',
												'Tanzanie'=>'Tanzanie',
												'Tchad'=>'Tchad',
												'Thailande'=>'Thailande',
												'Tibet'=>'Tibet',
												'Timor_Oriental'=>'Timor_Oriental',
												'Togo'=>'Togo',
												'Trinite_et_Tobago'=>'Trinite_et_Tobago',
												'Tristan da cunha'=>'Tristan da cunha',
												'Tunisie'=>'Tunisie',
												'Turkmenistan'=>'Turmenistan',
												'Turquie'=>'Turquie',
												'Ukraine'=>'Ukraine',
												'Uruguay'=>'Uruguay',
												'Vanuatu'=>'Vanuatu',
												'Vatican'=>'Vatican',
												'Venezuela'=>'Venezuela',
												'Vierges_Americaines'=>'Vierges_Americaines',
												'Vierges_Britanniques'=>'Vierges_Britanniques',
												'Vietnam'=>'Vietnam',
												'Wake'=>'Wake',
												'Wallis et Futuma'=>'Wallis et Futuma',
												'Yemen'=>'Yemen',
												'Yougoslavie'=>'Yougoslavie',
												'Zambie'=>'Zambie',
												'Zimbabwe'=>'Zimbabwe',
											],
											null, ['class' => 'form-control']) !!}
										@if ($errors->has('pays'))
											<span class="help-block">
												<strong>{{ $errors->first('pays') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('tel_mobile') ? ' has-error' : '' }}">
									<label for="ville" class="col-md-5 control-label">Téléphone mobile</label>

									<div class="col-md-6">
										<input id="tel_mobile" type="tel" class="form-control" name="tel_mobile" placeholder="Ex.: 0605040707">

										@if ($errors->has('tel_mobile'))
											<span class="help-block">
												<strong>{{ $errors->first('tel_mobile') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('tel_fixe') ? ' has-error' : '' }}">
									<label for="ville" class="col-md-5 control-label">Téléphone fixe</label>

									<div class="col-md-6">
										<input id="tel_fixe" type="tel" class="form-control" name="tel_fixe" placeholder="Ex.: 0605040707">

										@if ($errors->has('tel_fixe'))
											<span class="help-block">
												<strong>{{ $errors->first('tel_fixe') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('annee_entree') ? ' has-error' : '' }}">
									<label class="col-md-5 control-label">Année d'entrée</label>

									<div class="col-md-6 col-sm-6">
										{!! Form::select('annee_entree', 
											[
												'' => 'Sélectionnez la date',
												'1980' => '1980',
												'1981' => '1981',
												'1982' => '1982',
												'1983' => '1983',
												'1984' => '1984',
												'1985' => '1985',
												'1986' => '1986',
												'1987' => '1987',
												'1988' => '1988',
												'1989' => '1989',
												'1990' => '1990',
												'1991' => '1991',
												'1992' => '1992',
												'1993' => '1993',
												'1994' => '1994',
												'1995' => '1995',
												'1996' => '1996',
												'1997' => '1997',
												'1998' => '1998',
												'1999' => '1999',
												'2000' => '2000',
												'2001' => '2001',
												'2002' => '2002',
												'2003' => '2003',
												'2004' => '2004',
												'2005' => '2005',
												'2006' => '2006',
												'2007' => '2007',
												'2008' => '2008',
												'2009' => '2009',
												'2010' => '2010',
												'2011' => '2011',
												'2012' => '2012',
												'2013' => '2013',
												'2014' => '2014',
												'2015' => '2015',
												'2016' => '2016',
												'2017' => '2017',
												'2018' => '2018',
												'2019' => '2019',
												'2020' => '2020',
												'2021' => '2021',
												'2022' => '2022',
												'2023' => '2023',
												'2024' => '2024',
												'2025' => '2025',
											],
										null, ['class' => 'form-control']) !!}
										@if ($errors->has('annee_entree'))
											<span class="help-block">
												<strong>{{ $errors->first('annee_entree') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('annee_sortie') ? ' has-error' : '' }}">
									<label class="col-md-5 control-label">Année de sortie</label>

									<div class="col-md-6 col-sm-6">
										{!! Form::select('annee_sortie', 
											[
												'' => 'Sélectionnez la date',
												'1980' => '1980',
												'1981' => '1981',
												'1982' => '1982',
												'1983' => '1983',
												'1984' => '1984',
												'1985' => '1985',
												'1986' => '1986',
												'1987' => '1987',
												'1988' => '1988',
												'1989' => '1989',
												'1990' => '1990',
												'1991' => '1991',
												'1992' => '1992',
												'1993' => '1993',
												'1994' => '1994',
												'1995' => '1995',
												'1996' => '1996',
												'1997' => '1997',
												'1998' => '1998',
												'1999' => '1999',
												'2000' => '2000',
												'2001' => '2001',
												'2002' => '2002',
												'2003' => '2003',
												'2004' => '2004',
												'2005' => '2005',
												'2006' => '2006',
												'2007' => '2007',
												'2008' => '2008',
												'2009' => '2009',
												'2010' => '2010',
												'2011' => '2011',
												'2012' => '2012',
												'2013' => '2013',
												'2014' => '2014',
												'2015' => '2015',
												'2016' => '2016',
												'2017' => '2017',
												'2018' => '2018',
												'2019' => '2019',
												'2020' => '2020',
												'2021' => '2021',
												'2022' => '2022',
												'2023' => '2023',
												'2024' => '2024',
												'2025' => '2025',
											],
										null, ['class' => 'form-control']) !!}
										@if ($errors->has('annee_sortie'))
											<span class="help-block">
												<strong>{{ $errors->first('annee_sortie') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('diplome') ? ' has-error' : '' }}">
									<label for="ville" class="col-md-5 control-label">Diplôme</label>

									<div class="col-md-6">
										{!! Form::select('diplome', 
																	
											[
												'' => 'Sélectionnez votre diplôme',
												'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)' => 'Maître en sciences et techniques, mention "Biologie appliquée aux industries agro-alimentaires, diplômé(e) de l\'Université de Brest" (1975-1992)',
												'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)' => 'Ingénieur Microbiologiste, diplômé(e) de l\'Université de Brest (1985-1993)',
												'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012) ' => 'Ingénieur diplômé(e) de l\'ESMISAB, Université de Brest, spécialité "Microbiologie et sécurité alimentaire" (1994-2012)',
												'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013) ' => 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Microbiologie et Qualité" (Depuis 2013) ',
												'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne(2002-2012) ' => 'Ingénieur diplômé(e) de l\'Université de Brest, spécialité "agroalimentaire" en partenariat avec l\'IFRIA Bretagne(2002-2012) ',
												'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013) ' => 'Ingénieur diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Procédés indutriels" en partenariat avec l\'IFRIA Bretagne (Depuis 2013) ',
												'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013) ' => 'Master professionnel diplômé(e) de l\'ESIAB, Université de Brest, spécialité "Innovation en Industries Alimentaires" (Depuis 2013) ',
											], null, ['class' => 'form-control']) !!}

									</div>
								</div>

								<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
									<label class="col-md-5 control-label" for="photo">Votre photo</label>

									<div class="col-md-6" data-placement="top" title="Formats possibles: jpg,jpeg,png,gif" data-tooltip="true">

										<input type="file" name="photo" id="photo_hidden" class="upl_hidden" onchange="document.getElementById('helpBlock_img').innerHTML=this.value.replace(/.*[\/\\]/, ''); $('#helpBlock_img').removeClass('hidden')">

										<button class="btn btn-success" onclick="event.preventDefault(); document.getElementById('photo_hidden').click();">Chercher sur le disque</button>
										<span id="helpBlock_img" class="help-block hidden" style="color: #434343; margin-bottom: 0;"></span>

										@if ($errors->has('photo'))
											<span class="help-block">
												<strong>{{ $errors->first('photo') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('fb') ? ' has-error' : '' }}">
									<label for="fb" class="col-sm-5 control-label">
										Facebook
										<i class="material-icons fa fa-facebook-square fa-2x" style="margin-left: 5px;"></i>
									</label>

									<div class="col-sm-6">
										<input id="fb" type="fb" class="form-control" name="fb" placeholder="https://www.facebook.com/pseudo">

										@if ($errors->has('fb'))
											<span class="help-block">
												<strong>{{ $errors->first('fb') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</fieldset>

							{{-- Professional data --}}
							<fieldset class="col-md-6">
								<h4 class="modal-title" id="myModalLabel"><strong>Données professionnelles</strong></h4>
								<hr>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="activite">Êtes vous en activité ?</label>
									<div class="col-sm-8">
										<label class="radio-inline">
											<input type="radio" name="activite" value="oui">Oui
										</label>
										<label class="radio-inline">
											<input type="radio" name="activite" value="Sans emploi"/>Sans emploi
										</label>
										<label class="radio-inline">
											<input type="radio" name="activite" value="Retraite">Retraite
										</label>
									</div>
								</div>

								<div class="form-group{{ $errors->has('profession') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="profession">Intitulé de poste</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="profession" placeholder="Intitulé de poste">
									</div>
								</div>

								<div class="form-group{{ $errors->has('type_de_contrat') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="type_de_contrat">Type de contrat</label>
									<div class="col-sm-6">
										{!! Form::select('type_de_contrat', 
											[
												'' => 'Sélectionnez votre contrat',
												'CDI' => 'CDI',
												'CDD' => 'CDD',
												'CTT ou Interim' => 'CTT ou Interim',
												'CUI' => 'CUI',
												'Apprentissage' => 'Apprentissage',
												'Contrat de professionnalisation' => 'Contrat de professionnalisation',
												'Autre' => 'Autre',
											],null, ['class' => 'form-control']) !!}
									</div>
								</div>

								<div class="form-group{{ $errors->has('anne_embauche') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="anne_embauche">Année d'embauche</label>
									<div class="col-sm-6">
										<input type="date" class="form-control" name="anne_embauche" placeholder="Année d'embauche">

										@if ($errors->has('anne_embauche'))
											<span class="help-block">
												<strong>{{ $errors->first('anne_embauche') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('entreprise') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="entreprise">Nom de l'entreprise</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="entreprise" placeholder="Nom de l'entreprise">
									</div>
								</div>

								<div class="form-group{{ $errors->has('adressepro') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="adressepro">Adresse de l'entreprise</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="adressepro" placeholder="Adresse de l'entreprise">
									</div>
								</div>

								<div class="form-group{{ $errors->has('code_postalpro') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="code_postalpro">Code postal de l'entreprise</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="code_postalpro" placeholder="Code postal de l'entreprise">
									</div>
								</div>

								<div class="form-group{{ $errors->has('ville_pro') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="ville_pro">Ville de l'entreprise</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="ville_pro" placeholder="Ville de l'entreprise">
									</div>
								</div>

								<!-- debut liste deroulante pays -->
								<div class="form-group{{ $errors->has('payspro') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="payspro">Quel est le lieu de votre travail ?</label>
									<div class="col-sm-6">

										{!! Form::select('payspro', 
											[
												'' =>'Pays',
												'France' =>'France',
												'Afghanistan'=>'Afghanistan',
												'Afrique_Centrale'=>'Afrique_Centrale',
												'Afrique_du_sud'=>'Afrique_du_Sud',
												'Albanie'=>'Albanie',
												'Algerie'=>'Algerie',
												'Allemagne'=>'Allemagne',
												'Andorre'=>'Andorre',
												'Angola'=>'Angola',
												'Anguilla'=>'Anguilla',
												'Arabie_Saoudite'=>'Arabie_Saoudite',
												'Argentine'=>'Argentine',
												'Armenie'=>'Armenie',
												'Australie'=>'Australie',
												'Autriche'=>'Autriche',
												'Azerbaidjan'=>'Azerbaidjan',
												'Bahamas'=>'Bahamas',
												'Bangladesh'=>'Bangladesh',
												'Barbade'=>'Barbade',
												'Bahrein'=>'Bahrein',
												'Belgique'=>'Belgique',
												'Belize'=>'Belize',
												'Benin'=>'Benin',
												'Bermudes'=>'Bermudes',
												'Bielorussie'=>'Bielorussie',
												'Bolivie'=>'Bolivie',
												'Botswana'=>'Botswana',
												'Bhoutan'=>'Bhoutan',
												'Boznie_Herzegovine'=>'Boznie_Herzegovine',
												'Bresil'=>'Bresil',
												'Brunei'=>'Brunei',
												'Bulgarie'=>'Bulgarie',
												'Burkina_Faso'=>'Burkina_Faso',
												'Burundi'=>'Burundi',
												'Caiman'=>'Caiman',
												'Cambodge'=>'Cambodge',
												'Cameroun'=>'Cameroun',
												'Canada'=>'Canada',
												'Canaries'=>'Canaries',
												'Cap_vert'=>'Cap_Vert',
												'Chili'=>'Chili',
												'Chine'=>'Chine',
												'Chypre'=>'Chypre',
												'Colombie'=>'Colombie',
												'Comores'=>'Colombie',
												'Congo'=>'Congo',
												'Congo_democratique'=>'Congo_democratique',
												'Cook'=>'Cook',
												'Coree_du_Nord'=>'Coree_du_Nord',
												'Coree_du_Sud'=>'Coree_du_Sud',
												'Costa_Rica'=>'Costa_Rica',
												'Cote_d_Ivoire'=>'Côte_d_Ivoire',
												'Croatie'=>'Croatie',
												'Cuba'=>'Cuba',
												'Danemark'=>'Danemark',
												'Djibouti'=>'Djibouti',
												'Dominique'=>'Dominique',
												'Egypte'=>'Egypte',
												'Emirats_Arabes_Unis'=>'Emirats_Arabes_Unis',
												'Equateur'=>'Equateur',
												'Erythree'=>'Erythree',
												'Espagne'=>'Espagne',
												'Estonie'=>'Estonie',
												'Etats_Unis'=>'Etats_Unis',
												'Ethiopie'=>'Ethiopie',
												'Falkland'=>'Falkland',
												'Feroe'=>'Feroe',
												'Fidji'=>'Fidji',
												'Finlande'=>'Finlande',
												'France'=>'France',
												'Gabon'=>'Gabon',
												'Gambie'=>'Gambie',
												'Georgie'=>'Georgie',
												'Ghana'=>'Ghana',
												'Gibraltar'=>'Gibraltar',
												'Grece'=>'Grece',
												'Grenade'=>'Grenade',
												'Groenland'=>'Groenland',
												'Guadeloupe'=>'Guadeloupe',
												'Guam'=>'Guam',
												'Guatemala'=>'Guatemala',
												'Guernesey'=>'Guernesey',
												'Guinee'=>'Guinee',
												'Guinee_Bissau'=>'Guinee_Bissau',
												'Guinee equatoriale'=>'Guinee_Equatoriale',
												'Guyana'=>'Guyana',
												'Guyane_Francaise'=>'Guyane_Francaise',
												'Haiti'=>'Haiti',
												'Hawaii'=>'Hawaii',
												'Honduras'=>'Honduras',
												'Hong_Kong'=>'Hong_Kong',
												'Hongrie'=>'Hongrie',
												'Inde'=>'Inde',
												'Indonesie'=>'Indonesie',
												'Iran'=>'Iran',
												'Iraq'=>'Iraq',
												'Irlande'=>'Irlande',
												'Islande'=>'Islande',
												'Israel'=>'Israel',
												'Italie'=>'italie',
												'Jamaique'=>'Jamaique',
												'Jan Mayen'=>'Jan Mayen',
												'Japon'=>'Japon',
												'Jersey'=>'Jersey',
												'Jordanie'=>'Jordanie',
												'Kazakhstan'=>'Kazakhstan',
												'Kenya'=>'Kenya',
												'Kirghizstan'=>'Kirghizistan',
												'Kiribati'=>'Kiribati',
												'Koweit'=>'Koweit',
												'Laos'=>'Laos',
												'Lesotho'=>'Lesotho',
												'Lettonie'=>'Lettonie',
												'Liban'=>'Liban',
												'Liberia'=>'Liberia',
												'Liechtenstein'=>'Liechtenstein',
												'Lituanie'=>'Lituanie',
												'Luxembourg'=>'Luxembourg',
												'Lybie'=>'Lybie',
												'Macao'=>'Macao',
												'Macedoine'=>'Macedoine',
												'Madagascar'=>'Madagascar',
												'Madère'=>'Madère',
												'Malaisie'=>'Malaisie',
												'Malawi'=>'Malawi',
												'Maldives'=>'Maldives',
												'Mali'=>'Mali',
												'Malte'=>'Malte',
												'Man'=>'Man',
												'Mariannes du Nord'=>'Mariannes du Nord',
												'Maroc'=>'Maroc',
												'Marshall'=>'Marshall',
												'Martinique'=>'Martinique',
												'Maurice'=>'Maurice',
												'Mauritanie'=>'Mauritanie',
												'Mayotte'=>'Mayotte',
												'Mexique'=>'Mexique',
												'Micronesie'=>'Micronesie',
												'Midway'=>'Midway',
												'Moldavie'=>'Moldavie',
												'Monaco'=>'Monaco',
												'Mongolie'=>'Mongolie',
												'Montserrat'=>'Montserrat',
												'Mozambique'=>'Mozambique',
												'Namibie'=>'Namibie',
												'Nauru'=>'Nauru',
												'Nepal'=>'Nepal',
												'Nicaragua'=>'Nicaragua',
												'Niger'=>'Niger',
												'Nigeria'=>'Nigeria',
												'Niue'=>'Niue',
												'Norfolk'=>'Norfolk',
												'Norvege'=>'Norvege',
												'Nouvelle_Caledonie'=>'Nouvelle_Caledonie',
												'Nouvelle_Zelande'=>'Nouvelle_Zelande',
												'Oman'=>'Oman',
												'Ouganda'=>'Ouganda',
												'Ouzbekistan'=>'Ouzbekistan',
												'Pakistan'=>'Pakistan',
												'Palau'=>'Palau',
												'Palestine'=>'Palestine',
												'Panama'=>'Panama',
												'Papouasie_Nouvelle_Guinee'=>'Papouasie_Nouvelle_Guinee',
												'Paraguay'=>'Paraguay',
												'Pays_Bas'=>'Pays_Bas',
												'Perou'=>'Perou',
												'Philippines'=>'Philippines',
												'Pologne'=>'Pologne',
												'Polynesie'=>'Polynesie',
												'Porto_Rico'=>'Porto_Rico',
												'Portugal'=>'Portugal',
												'Qatar'=>'Qatar',
												'Republique_Dominicaine'=>'Republique_Dominicaine',
												'Republique_Tcheque'=>'Republique_Tcheque',
												'Reunion'=>'Reunion',
												'Roumanie'=>'Roumanie',
												'Royaume_Uni'=>'Royaume_Uni',
												'Russie'=>'Russie',
												'Rwanda'=>'Rwanda',
												'Sahara Occidental'=>'Sahara Occidental',
												'Sainte_Lucie'=>'Sainte_Lucie',
												'Saint_Marin'=>'Saint_Marin',
												'Salomon'=>'Salomon',
												'Salvador'=>'Salvador',
												'Samoa_Occidentales'=>'Samoa_Occidentales',
												'Samoa_Americaine'=>'Samoa_Americaine',
												'Sao_Tome_et_Principe'=>'Sao_Tome_et_Principe',
												'Senegal'=>'Senegal',
												'Seychelles'=>'Seychelles',
												'Sierra Leone'=>'Sierra Leone',
												'Singapour'=>'Singapour',
												'Slovaquie'=>'Slovaquie',
												'Slovenie'=>'Slovenie',
												'Somalie'=>'Somalie',
												'Soudan'=>'Soudan',
												'Sri_Lanka'=>'Sri_Lanka',
												'Suede'=>'Suede',
												'Suisse'=>'Suisse',
												'Surinam'=>'Surinam',
												'Swaziland'=>'Swaziland',
												'Syrie'=>'Syrie',
												'Tadjikistan'=>'Tadjikistan',
												'Taiwan'=>'Taiwan',
												'Tonga'=>'Tonga',
												'Tanzanie'=>'Tanzanie',
												'Tchad'=>'Tchad',
												'Thailande'=>'Thailande',
												'Tibet'=>'Tibet',
												'Timor_Oriental'=>'Timor_Oriental',
												'Togo'=>'Togo',
												'Trinite_et_Tobago'=>'Trinite_et_Tobago',
												'Tristan da cunha'=>'Tristan da cunha',
												'Tunisie'=>'Tunisie',
												'Turkmenistan'=>'Turmenistan',
												'Turquie'=>'Turquie',
												'Ukraine'=>'Ukraine',
												'Uruguay'=>'Uruguay',
												'Vanuatu'=>'Vanuatu',
												'Vatican'=>'Vatican',
												'Venezuela'=>'Venezuela',
												'Vierges_Americaines'=>'Vierges_Americaines',
												'Vierges_Britanniques'=>'Vierges_Britanniques',
												'Vietnam'=>'Vietnam',
												'Wake'=>'Wake',
												'Wallis et Futuma'=>'Wallis et Futuma',
												'Yemen'=>'Yemen',
												'Yougoslavie'=>'Yougoslavie',
												'Zambie'=>'Zambie',
												'Zimbabwe'=>'Zimbabwe',
											], null, ['class' => 'form-control']) !!}
									</div>
								</div>
								<!-- fin de la liste des pays -->

								<div class="form-group{{ $errors->has('telephone_fixe_pro') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="telephone_fixe_pro">Numéro de téléphone professionnel</label>
									<div class="col-sm-6">
										<input type="tel" class="form-control" name="telephone_fixe_pro" placeholder="Téléphone professionnel">
										@if ($errors->has('telephone_fixe_pro'))
											<span class="help-block">
												<strong>{{ $errors->first('telephone_fixe_pro') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('courriel_professionnel') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="courriel_professionnel">E-mail professionnel</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="courriel_professionnel" placeholder="Courriel professionnel">
									</div>
								</div>

								<div class="form-group{{ $errors->has('secteur') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="secteur">Secteur d'activité</label>
									<div class="col-sm-6">
										{!! Form::select('secteur', 
											[
												'' => 'Sélectionnez votre secteur',
												'Industrie de la viande ' => 'Industrie de la viande ',
												'Industrie laitière' => 'Industrie laitière',
												'Industrie sucrière' => 'Industrie sucrière',
												'Industrie des conserves, produits surgelés, fruits et légumes' => 'Industrie des conserves, produits surgelés, fruits et légumes',
												'Industrie des produits à base de céréales ' => 'Industrie des produits à base de céréales ',
												'Industrie des produits alimentaires divers (confiseries, condiments...) ' => 'Industrie des produits alimentaires divers (confiseries, condiments...) ',
												'Industrie des boissons, alcools et agro-carburants ' => 'Industrie des boissons, alcools et agro-carburants ',
												'Cosmétique' => 'Cosmétique',
												'Restauration et hôtellerie' => 'Restauration et hôtellerie',
												'Etablissements de santé' => 'Etablissements de santé',
												'Formation et consulting' => 'Formation et consulting',
												'Enseignement' => 'Enseignement',
												'Recherche' => 'Recherche',
												'Autres secteurs ' => 'Autres secteurs ',
											], null, ['class' => 'form-control']) !!}
									</div>
								</div>

								<div class="form-group{{ $errors->has('adresse_site_web') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="adresse_site_web">Site web de l’entreprise</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="adresse_site_web" placeholder="www">
									</div>
								</div>

								<div class="form-group{{ $errors->has('cv') ? ' has-error' : '' }}">
									<label class="col-sm-5 control-label" for="cv">Votre CV</label>

									<div class="col-sm-6" data-placement="top" title="Formats possibles: pdf, doc, docx, rtf, odt" data-tooltip="true">

										<input type="file" name="cv" id="upload_hidden_cv" class="upl_hidden" onchange="document.getElementById('helpBlock_cv').innerHTML=this.value.replace(/.*[\/\\]/, ''); $('#helpBlock_cv').removeClass('hidden')">

										<button class="btn btn-success" onclick="event.preventDefault(); document.getElementById('upload_hidden_cv').click();">Chercher sur le disque</button>
										<span id="helpBlock_cv" class="help-block hidden" style="color: #434343; margin-bottom: 0;"></span>

										@if ($errors->has('cv'))
											<span class="help-block">
												<strong>{{ $errors->first('cv') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
									<label for="linkedin" class="col-sm-5 control-label">
										LinkedIn
										<i class="material-icons fa fa-linkedin-square fa-2x" style="margin-left: 5px;"></i>
									</label>

									<div class="col-sm-6">

										<input id="linkedin" type="linkedin" class="form-control" name="linkedin" placeholder="LinkedIn">
										@if ($errors->has('linkedin'))
											<span class="help-block">
												<strong>{{ $errors->first('linkedin') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('viadeo') ? ' has-error' : '' }}">
									<label for="viadeo" class="col-sm-5 control-label">
										Viadeo
										<i class="material-icons fa fa-viadeo-square fa-2x" style="margin-left: 5px;"></i>
									</label>

									<div class="col-sm-6">

										<input id="viadeo" type="viadeo" class="form-control" name="viadeo" placeholder="Viadeo">
										@if ($errors->has('viadeo'))
											<span class="help-block">
												<strong>{{ $errors->first('viadeo') }}</strong>
											</span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} required">
									<label class="col-sm-5 control-label" for="adresse_site_web">Mot de passe</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="pass" name="password" placeholder="Min 6 caracters" required>

										<button class="btn btn-xs btn-primary gen">Générer</button>

										@if ($errors->has('password'))
											<span class="help-block">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
										@endif
									</div>
								</div>
							</fieldset>

							{{-- Agreement --}}
							<fieldset class="col-md-12">
								<div class="form-group{{ $errors->has('ok') ? ' has-error' : '' }} required">
									<label class="col-sm-6 control-label" for="ok">Acceptez-vous l'utilisation de ces données dans le cadre de  l'édition de l'annuaire et sur le site web ?</label>
									<div class="col-sm-6">
										<label class="radio-inline">
												{!! Form::radio('ok', 'oui') !!} Oui
										</label>
										<label class="radio-inline">
												{!! Form::radio('ok', 'pro') !!} Que mes données pro
										</label>
										<label class="radio-inline">
												{!! Form::radio('ok', 'non') !!} Non
										</label>
									</div>
								</div>

								<div class="form-group text-center">
									<div class="col-md-6 col-md-offset-3">
										<button type="submit" class="btn btn-primary">Enregistrer</button>
										<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Annuler</button>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
