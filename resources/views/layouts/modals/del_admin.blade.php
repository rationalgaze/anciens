				<div class="modal fade" id="delete_adm" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
					<div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Confirmez-vous la suppression ?</h4>
							</div>
							<div class="modal-body text-center">
								<form method="POST">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<button type="submit" class="btn btn-danger">Oui</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Non</button>
								</form>
							</div>
						</div>
					</div>
				</div>