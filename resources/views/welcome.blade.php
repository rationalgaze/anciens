<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('images/favicon.ico') }}" type="image/x-icon" rel="shortcut icon">
        <meta name="description" content="Annuaire électronique des diplômés de l'ESIAB - UBO - Brest">
        <meta name="keywords" content="Annuaire électronique des diplômés de l'ESIAB - UBO - Brest">
        <meta name="robots" content="noindex, nofollow">
        
        <title>Anciens | Annuaire électronique des diplômés de l'ESIAB</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        <!-- Schema.org markup (Google) -->
        <meta itemprop="name" content="Anciens de l'ESIAB">
        <meta itemprop="description" content="Annuaire électronique des diplômés de l'ESIAB">
        <meta itemprop="image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}">

        <!-- Twitter Card markup-->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@annuaireESIAB">
        <meta name="twitter:title" content="Annuaire électronique des diplômés de l'ESIAB">
        <meta name="twitter:description" content="Annuaire électronique des diplômés de l'ESIAB">
        <meta name="twitter:creator" content="@NikolaiVorotnikov">
        <meta name="twitter:image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}">
        <meta name="twitter:image:alt" content="Annuaire électronique des diplômés de l'ESIAB">

        <!-- Open Graph markup (Facebook, Pinterest) -->
        <meta property="og:title" content="Anciens de l'ESIAB" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="https://222xesiab12.univ-brest.fr" />
        <meta property="og:image" content="{{ asset('images/1-logo-ESIAB-blanc.png') }}" />
        <meta property="og:description" content="Annuaire électronique des diplômés de l'ESIAB" />
        <meta property="og:site_name" content="Annuaire électronique des diplômés de l'ESIAB" />
    </head>
    <body>
        <div class="position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/profile') }}">Home</a>
                    @else
                        <a type="button" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Se connecter</a>
                        <ul class="dropdown-menu">
                          <li><a href="{{ url('/login') }}">Diplômés</a></li>
                          <li><a href="{{ url('/admin') }}">Administrateurs</a></li>
                        </ul>
                    @endif
                </div>
            @endif

            <div class="container container-welcome">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="logo">
                            <img src="{{ asset('images/1-logo-ESIAB-blanc.png') }}" alt="ESIAB Logo">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                        <div class="title text-center">
                            <span class="text-uppercase">Espace des diplômés</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
