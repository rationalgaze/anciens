@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Se connecter</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">E-mail</label>

							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail">

								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Mot de passe</label>

							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" placeholder="Mot de passe" required>

								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Se connecter
								</button>

								<a class="btn btn-link" href="{{ route('password.request') }}">
									Mot de passe oublié ?
								</a>
							</div>
						</div>
					</form>
					<div class="col-md-8 col-md-offset-4">
						<a href="#" id="btn_msg_toadm" class="text-warning">Contacter l'administrateur</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row errors">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
				<div class="alert alert-dismissible alert-danger" id="error">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			@if(Session::has('message'))
				<div class="alert alert-dismissible alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ Session::get('message') }}
				</div>
			@endif
		</div>
	</div>

	<div id="msg_toadm" class="row" style="display: none">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Envoyer un message à l'administrateur</div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="{{ route('msg_to_admin') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('nom_msg') ? ' has-error' : '' }}">
							<label for="nom_msg" class="col-md-4 control-label">Votre Nom</label>

							<div class="col-md-6">
								<input id="nom_msg" type="nom_msg" class="form-control" name="nom_msg" placeholder="Votre Nom">

								@if ($errors->has('nom_msg'))
									<span class="help-block">
										<strong>{{ $errors->first('nom_msg') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('usr_email') ? ' has-error' : '' }}">
							<label for="usr_email" class="col-md-4 control-label">Votre E-mail</label>

							<div class="col-md-6">
								<input id="usr_email" type="usr_email" class="form-control" name="usr_email" required placeholder="Votre E-mail">

								@if ($errors->has('usr_email'))
									<span class="help-block">
										<strong>{{ $errors->first('usr_email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('texte') ? ' has-error' : '' }}">
							<label for="texte" class="col-md-4 control-label">Message</label>

							<div class="col-md-6">
								<textarea id="texte" type="texte" class="form-control" name="texte" maxlength="1000" placeholder="Message maximum 1000 caractères">
								</textarea>

								@if ($errors->has('texte'))
									<span class="help-block">
										<strong>{{ $errors->first('texte') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Envoyer
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
