@component('mail::message')
# Bonjour,

Un nouvel utilisateur <strong>{{ $user->name }}</strong> vient de s'inregistrer sur le site des diplômés de l'ESIAB
avec le login <span> {{ $user->email }}</span>.

<!-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent -->

Cordialement,<br>
{{ config('app.name') }}
@endcomponent
