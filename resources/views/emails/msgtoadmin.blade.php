@component('mail::message')
# Bonjour,

<strong>{{ $msg->nom_msg }}</strong> vient de vous envoyer le message suivant :

@component('mail::panel')
	<span> {{ $msg->texte }}</span>.
@endcomponent

Pour lui répondre envoyez un message sur : <span> {{ $msg->usr_email }} </span>
@endcomponent
