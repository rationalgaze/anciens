@component('mail::message')
# Bonjour {{ $user->name }},

Vous êtes bien enregistré sur le site des anciens étudiants de l'ESIAB.
Nous vous remercions pour votre visite.

<!-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent -->

Cordialement,<br>
{{ config('app.name') }}
@endcomponent
