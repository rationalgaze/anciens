<?php

use App\Users_bio;

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::prefix('admin')->group(function(){

	// students
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/show/{usr}', 'AdminController@show')->name('admin.show');
	Route::get('/edit/{usr}', 'AdminController@edit')->name('admin.edit');
	Route::post('/edit/update_personal/{usr}', 'AdminController@update_personal')->name('admin.update_personal');
	Route::post('/edit/update_pro/{usr}', 'AdminController@update_pro')->name('admin.update_pro');
	Route::post('/change_pass/{usr}', 'AdminController@change_pass')->name('admin.change_pass');
	Route::delete('/delete/{usr}', 'AdminController@delete')->name('admin.delete');
	Route::post('/add_usr', 'AdminController@add_usr')->name('admin.add_usr');

	Route::get('/par_region', 'AdminController@par_region')->name('admin.par_region');
	Route::get('/par_secteur', 'AdminController@par_secteur')->name('admin.par_secteur');
	Route::get('/par_parcours', 'AdminController@par_parcours')->name('admin.par_parcours');
	Route::get('/generate', 'AdminController@generate')->name('admin.generate');

	// admins
	Route::delete('/delete_admin/{admin}', 'AdminController@delete_admin')->name('admin.delete_admin');
	Route::post('/add_admin', 'AdminController@add_admin')->name('admin.add_admin');
	Route::post('/update_admin/{admin}', 'AdminController@update_admin')->name('admin.update_admin');
	Route::get('/edit_admin/{admin}', 'AdminController@edit_admin')->name('admin.edit_admin');

	// Export to Word/Excel/pdf
	Route::get('/to_word', 'AdminController@to_word')->name('admin.to_word');
	Route::get('/to_excel', 'AdminController@to_excel')->name('admin.to_excel');

	// Import from Excel
	Route::post('/importExcel', 'AdminController@importExcel')->name('admin.importExcel');

	// A-Z pagination
	Route::get('/a', 'Azpagination@a')->name('a');
	Route::get('/b', 'Azpagination@b')->name('b');
	Route::get('/c', 'Azpagination@c')->name('c');
	Route::get('/d', 'Azpagination@d')->name('d');
	Route::get('/e', 'Azpagination@e')->name('e');
	Route::get('/f', 'Azpagination@f')->name('f');
	Route::get('/g', 'Azpagination@g')->name('g');
	Route::get('/h', 'Azpagination@h')->name('h');
	Route::get('/i', 'Azpagination@i')->name('i');
	Route::get('/j', 'Azpagination@j')->name('j');
	Route::get('/k', 'Azpagination@k')->name('k');
	Route::get('/l', 'Azpagination@l')->name('l');
	Route::get('/m', 'Azpagination@m')->name('m');
	Route::get('/n', 'Azpagination@n')->name('n');
	Route::get('/o', 'Azpagination@o')->name('o');
	Route::get('/p', 'Azpagination@p')->name('p');
	Route::get('/q', 'Azpagination@q')->name('q');
	Route::get('/r', 'Azpagination@r')->name('r');
	Route::get('/s', 'Azpagination@s')->name('s');
	Route::get('/t', 'Azpagination@t')->name('t');
	Route::get('/u', 'Azpagination@u')->name('u');
	Route::get('/v', 'Azpagination@v')->name('v');
	Route::get('/w', 'Azpagination@w')->name('w');
	Route::get('/x', 'Azpagination@x')->name('x');
	Route::get('/y', 'Azpagination@y')->name('y');
	Route::get('/z', 'Azpagination@z')->name('z');
	Route::get('/all', 'Azpagination@all')->name('all');
	Route::get('/new', 'Azpagination@new')->name('new');
	Route::get('/par_annee', 'Azpagination@par_annee')->name('par_annee');
});
	
Route::post('/msg_to_admin', 'Message@msg_to_admin')->name('msg_to_admin');

Route::prefix('profile')->group(function(){

	Route::get('/', 'ProfileController@index')->name('profile');
	Route::get('/show/{usr}', 'ProfileController@show')->name('show');
	Route::post('/update', 'ProfileController@update')->name('update');
	Route::post('/change_email', 'ProfileController@change_email')->name('change_email');
	Route::post('/change_avatar', 'ProfileController@change_avatar')->name('change_avatar');
	Route::post('/change_pass', 'ProfileController@change_pass')->name('change_pass');
	Route::post('/update_pro', 'ProfileController@update_pro')->name('update_pro');

	Route::get('/par_region', 'ProfileController@par_region')->name('profile.par_region');
	Route::get('/par_secteur', 'ProfileController@par_secteur')->name('profile.par_secteur');
	Route::get('/par_parcours', 'ProfileController@par_parcours')->name('profile.par_parcours');

	// A-Z pagination
	Route::get('/a', 'ProfileController@a')->name('profile.a');
	Route::get('/b', 'ProfileController@b')->name('profile.b');
	Route::get('/c', 'ProfileController@c')->name('profile.c');
	Route::get('/d', 'ProfileController@d')->name('profile.d');
	Route::get('/e', 'ProfileController@e')->name('profile.e');
	Route::get('/f', 'ProfileController@f')->name('profile.f');
	Route::get('/g', 'ProfileController@g')->name('profile.g');
	Route::get('/h', 'ProfileController@h')->name('profile.h');
	Route::get('/i', 'ProfileController@i')->name('profile.i');
	Route::get('/j', 'ProfileController@j')->name('profile.j');
	Route::get('/k', 'ProfileController@k')->name('profile.k');
	Route::get('/l', 'ProfileController@l')->name('profile.l');
	Route::get('/m', 'ProfileController@m')->name('profile.m');
	Route::get('/n', 'ProfileController@n')->name('profile.n');
	Route::get('/o', 'ProfileController@o')->name('profile.o');
	Route::get('/p', 'ProfileController@p')->name('profile.p');
	Route::get('/q', 'ProfileController@q')->name('profile.q');
	Route::get('/r', 'ProfileController@r')->name('profile.r');
	Route::get('/s', 'ProfileController@s')->name('profile.s');
	Route::get('/t', 'ProfileController@t')->name('profile.t');
	Route::get('/u', 'ProfileController@u')->name('profile.u');
	Route::get('/v', 'ProfileController@v')->name('profile.v');
	Route::get('/w', 'ProfileController@w')->name('profile.w');
	Route::get('/x', 'ProfileController@x')->name('profile.x');
	Route::get('/y', 'ProfileController@y')->name('profile.y');
	Route::get('/z', 'ProfileController@z')->name('profile.z');
	Route::get('/all', 'ProfileController@all')->name('profile.all');
	Route::get('/par_annee', 'ProfileController@par_annee')->name('profile.par_annee');
});

