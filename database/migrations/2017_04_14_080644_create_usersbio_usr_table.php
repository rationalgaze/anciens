<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersbioUsrTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usersbio_usr', function (Blueprint $table) {
			$table->increments('user_id')->unique();
			$table->string('usr_sex', 1)->nullable();
			$table->string('usr_nom', 50)->nullable();
			$table->string('usr_prenom', 50)->nullable();
			$table->string('usr_nom_naiss', 50)->nullable();
			$table->string('usr_lieu_naiss', 100)->nullable();
			$table->date('usr_date_naiss')->nullable();
			$table->string('usr_email', 190)->unique()->index();
			$table->string('usr_adresse_perso', 100)->nullable();
			$table->string('usr_cp', 10)->nullable();
			$table->string('usr_ville', 50)->nullable();
			$table->string('usr_pays', 50)->nullable();
			$table->string('usr_tel_mob', 16)->nullable();
			$table->string('usr_tel_fixe', 16)->nullable();
			$table->string('usr_annee_entree',4)->nullable();
			$table->string('usr_annee_sortie',4)->nullable();
			$table->string('usr_diplome', 150)->nullable();
			$table->string('usr_fb')->nullable(); 

			//données pro
			$table->string('usr_activite', 11)->nullable();
			$table->string('usr_poste', 100)->nullable();
			$table->string('usr_contrat', 50)->nullable();
			$table->string('usr_annee_emb',4)->nullable();
			$table->string('usr_nom_entreprise', 100)->nullable();
			$table->string('usr_adresse_entreprise', 100)->nullable();
			$table->string('usr_cp_entreprise',6)->nullable();
			$table->string('usr_ville_entreprise', 50)->nullable();
			$table->string('usr_pays_travail', 50)->nullable();
			$table->string('usr_tel_pro', 16)->nullable();
			$table->string('usr_email_pro', 50)->nullable();
			$table->string('usr_secteur', 150)->nullable();
			$table->string('usr_site_web', 50)->nullable();
			$table->string('usr_cv',255)->nullable();
			$table->string('usr_img',255)->nullable();
			$table->string('usr_linkedin')->nullable();
			$table->string('usr_viadeo')->nullable();
			$table->string('usr_donnees',3)->default('oui');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
			Schema::dropIfExists('usersbio_usr');
	}
}
